<div id="sidebar">
    <div id="sidebar-wrapper" >


      <div id="profile-links"> <br />
        <a href="/index.php" target="_BLANK" style="color:#000;" title="网站前台首页">网站前台首页</a> | <a href="<?php echo Yii::app()->createUrl("site/logout"); ?>" style="color:#000;" >注销</a> </div>
      <ul id="main-nav">
        <li> <a href="#" class="nav-top-item <?php if($this->isType==1)echo 'current'; ?>">系统管理</a>
          <ul>
            <li><a <?php if($this->isSmallType==1 && $this->isType==1)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("Webroot/index",array('isSmallType'=>1)); ?>">站点设置</a></li>
            
          </ul>
        </li>
        <li> <a href="#" class="nav-top-item <?php if($this->isType==9)echo 'current'; ?>">管理员</a>
          <ul>
            <li><a <?php if($this->isSmallType==1 && $this->isType==9)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("jurisdiction/index",array('isSmallType'=>1)); ?>">账户管理</a></li>
          </ul>
        </li>
        
        <li> <a href="#" class="nav-top-item <?php if($this->isType==3)echo 'current'; ?>">广告管理</a>
          <ul>
            <li><a <?php if($this->isType==3 && $this->isSmallType==1)echo 'class="current"'; ?> href="<?php echo CHtml::normalizeUrl(array("Advert/index",'isSmallType'=>1)); ?>">广告管理</a></li>
          </ul>
        </li>
        <li> <a href="#" class="nav-top-item <?php if($this->isType==4)echo 'current'; ?>">栏目管理</a>
          <ul>
            <li><a <?php if($this->isType==4 && $this->isSmallType==1)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("channel/index",array('isSmallType'=>1)); ?>">栏目管理</a></li>
          </ul>
        </li>
        <li>
            <a href="#" class="nav-top-item <?php if($this->isType==5)echo 'current'; ?>">快捷内容管理</a>
          <ul>
              <?php $topChannel = adminSys::_getQuickChannel(); 
                   if($topChannel):
                       foreach($topChannel as $value):
              ?>
            <li><a href="<?php echo Yii::app()->createUrl('listart/index',array('artTopId'=>$value['id'],'isSmallType'=>$value['id'])); ?>"  <?php if($this->isType==5 && $this->isSmallType==$value->id)echo 'class="current"'; ?> ><?php echo $value->typename; ?></a></li>
            <?php endforeach;endif; ?>
          </ul>
        </li>
        <li>
            <a href="#" class="nav-top-item <?php if($this->isType==6)echo 'current'; ?>">数据库管理</a>
            <ul>
            <li><a <?php if($this->isType==6 && $this->isSmallType==1)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("backup/default/index"); ?>">数据库备份</a></li>
            <li><a <?php if($this->isType==6 && $this->isSmallType==1)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("backup/default/Create"); ?>">建立备份</a></li>
            <li><a <?php if($this->isType==6 && $this->isSmallType==1)echo 'class="current"'; ?> href="<?php echo Yii::app()->createUrl("backup/default/Upload"); ?>">上传数据库备份</a></li>
          </ul>
        </li>

      
          </ul>
        
    </div>
  </div>