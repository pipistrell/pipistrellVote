<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class WidgetController extends CController {
   public $isType = 0;
   public $isSmallType = 0;
   public $row = 10;
   public $page = 1;
   public $totalPage = 0;
   public $total = 0;
   public $layout='application.admin.views.layouts.main';
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter'
            )
        );
    }

    public function actionIndex() {
        $this->page = isset($_GET['page'])&&!empty($_GET['page'])?$_GET['page']:1;
        $this->isType = 2;
        $this->isSmallType = $_GET['isSmallType'];
        $this->render('widgetShow',array(
            'data'=>$this->_getData()
        ));
    }
    private function _getData(){
        $limit = ' limit '.(($this->page-1)*$this->row).','.$this->row;
        $where = adminSys::_whereWork($_POST);
        $where = empty($where)?' where isSys="0" ':' and isSys = "0" ';
        $sql = 'select * from {{widget}} '.$where.$limit;
        $sql1 = 'select count(*) as count_num from {{widget}} '.$where;
        $data = array();
        $data[0] = Yii::app()->db->createCommand($sql)->queryAll();
        $data[1] = Yii::app()->db->createCommand($sql1)->queryAll();
        $this->totalPage = ceil($data[1][0]['count_num']/$this->row);
        $this->total = $data[1][0]['count_num'];
        return $data[0];
    }
    public function actionEdit(){
        if(empty($_GET['wid']))$this->redirect (array('site/error','msg'=>'ID不能为空！'));
        $data = Widget::model()->findByPk($_GET['wid']);
        $this->render('widgetEdit',array(
            'data'=>$data,
            'title'=>'编辑挂件'
        ));
    }
    public function actionPutedit(){
        if(IS_POST){
            if(empty($_POST['wid']))$this->redirect (array('site/error','msg'=>'ID不能为空！'));
            $widget = Widget::model()->findByPk($_POST['wid']);
            if($widget->widgetNameCn != $_POST['widgetNameCn'] && !empty($_POST['widgetNameCn']))$widget->widgetNameCn = $_POST['widgetNameCn'];
            if($widget->widgetFile != $_POST['widgetFile'] && !empty($_POST['widgetFile']))$widget->widgetFile = $_POST['widgetFile'];
            if($widget->ismobile != $_POST['ismobile'] && !empty($_POST['ismobile']))$widget->ismobile = $_POST['ismobile'];
            if($widget->enable != $_POST['enable'] && !empty($_POST['enable']))$widget->enable = $_POST['enable'];
            if($widget->description != $_POST['description'] && !empty($_POST['description']))$widget->description = $_POST['description'];
            $widget->update();
            echo 1;
        }
    }
    public function actionAdd(){
        if(!isset($_GET['step']) || empty($_GET['step']))$_GET['step'] = 1;
        $step = $_GET['step'];

        if($step != 1){
            $step = isset(Yii::app()->session['newWidgetStep'])?$step:1;
            if(!isset(Yii::app()->session['uploadModel']))Yii::app()->session['uploadModel'] = '$.widgetAdd.isUploadC()';
        }
        
        $this->render('add_step_'.$step,array(
            'step'=>$step
        ));
    }
    public function actionClear(){
        unset(Yii::app()->session['uploadModel']);
        unset(Yii::app()->session['newWidgetStep']);
    }
    public function actionPutInDb(){
        if(!IS_POST)return;
        $widget = Widget::model()->find(' widgetName = :_widgetName ',array(':_widgetName'=>$_POST['widgetName']));
        if($widget){echo 2;return;}
        $widget = New Widget();
        if(!empty($_POST['widgetName']))$widget->widgetName = $_POST['widgetName'];
        if(!empty($_POST['widgetNameCn']))$widget->widgetNameCn = $_POST['widgetNameCn'];
        if(!empty($_POST['widgetFile']))$widget->widgetFile = $_POST['widgetFile'];
        if(!empty($_POST['description']))$widget->description = $_POST['description'];
        if(!empty($_POST['ismobile']))$widget->ismobile = $_POST['ismobile'];
        if(!empty($_POST['enable']))$widget->enable = $_POST['enable'];
        $bak = $widget->save();
        if(!$bak)return '';
        Yii::app()->session['newWidgetStep'] = $widget->widgetName;
        echo $bak;
    }
    public function actionSqldetail(){
        if(empty($_GET['wid']))return;
        $widget = Widget::model()->findByPk($_GET['wid']);
        $addfileds = require_once 'addfileds_'.$widget->tableName.'.php';  
       $addCN = require_once 'addfiledsCn_'.$widget->tableName.'.php';
        $this->render('sqldetail',array(
            'widgetData'=>  $widget,
            'addfileds'=>$addfileds,
            'addCN'=>$addCN
        ));
    }
    public function actionDeletField(){
        if(empty($_POST['wid']))return;
         
        $widget = Widget::model()->findByPk($_POST['wid']);
        $arr = $_POST['sqlcheck'];
        eval('$addfileds = array('.$_POST['addfileds'].');');
        eval('$addCN = array('.$_POST['addCN'].');');
        $str = '';
        $arrCN = '';
        $arrFileds = '';
        if(!empty($arr)){
        foreach($arr as $v){
            if(!empty($str))$str .= ',';
            $str .= $v;
            $arrFileds .= '"'.$v.'"=>"'.$addfileds[$v].'",';
            $arrCN .= '"'.$v.'"=>"'.$addCN[$v].'",';
         }
         $arrFileds = '<?php return array('.$arrFileds.');';
         $arrCN = '<?php return array('.$arrCN.');';
        }else{
            $arrFileds = '<?php return array();';
            $arrCN = '<?php return array();';
        }
         $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR.'addfileds'.DIRECTORY_SEPARATOR;
         file_put_contents($path.'addfileds_'.$widget->tableName.'.php', $arrFileds);
         file_put_contents($path.'addfiledsCn_'.$widget->tableName.'.php', $arrCN);
         $widget->Attribute = $str;
         $bak = $widget->update();
         echo '<script>parent.backMsg("'.$bak.'");</script>';
    }
    public function actionAddFildes(){
        if(empty($_GET['wid']))$this->redirect (array('site/index'));
        $this->render('addFileds',array(
            'wid'=>$_GET['wid']
        ));
    }
    public function actionAddSqlFiled(){
        if(empty($_POST['wid']))return;
        $widget = Widget::model()->findByPk($_POST['wid']);
        $Attr = $this->_filedAttr($_POST['Attr']);
         
        $addfileds = require_once 'addfileds_'.$widget->tableName.'.php';  
       $addCN = require_once 'addfiledsCn_'.$widget->tableName.'.php';
       $addfileds[] = 1;
      
       $str = var_export($addfileds,true);
       echo $str;return;
        $sql = 'alter table {add'.$widget->tableName.'} add '.$_POST['fName'].' '.$Attr.' ';
    }
    private function _filedAttr($name){
        switch($name){
            case 'radio':
                $a = 'tinyint(4)';
                break;
            case 'date':
                $a = 'int(11)';
                break;
            case 'htmltext':
                $a = 'mediumtext(0)';
                break;
            case 'upload':
                $a = 'varchar(255)';
                break;
            case 'number':
                $a = 'float(0)';
                break;
            case 'text':
                $a = 'varchar(50)';
                break;
        }
        return $a;
    }
}

?>