<?php
// author:pipistrell
// email:289404562@qq.com
$backend = dirname(dirname(__FILE__));
$frontend = dirname($backend);
Yii::setPathOfAlias('backend', $backend);
$frontendArray = require_once($frontend . '/config/main.php');

$backendArray = array(
    'name' => WEBNAME . '后台管理系统',
    'basePath' => $frontend,
    'defaultController' => 'site',
    'viewPath' => $backend . '/views',
    'controllerPath' => $backend . '/controllers',
    'runtimePath' => $backend . '/runtime',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.phpmailer.*',
        'backend.models.*',
        'backend.components.*',
        'backend.addfileds.*',
        'application.vendors.*',
    ),'components'=>array(
         'cache' => array(
            'class' => 'system.caching.CFileCache',
            'directoryLevel' => '2', //缓存文件的目录深度  
        ),'urlManager'=>array(
            'urlFormat'=>'get',
            'showScriptName' => true,
             'urlSuffix'=>false, //加上.html 
            ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        ),'params'=>array(  
        'uploadPath'=>'/upload/content/',  //添加这句，upload为存放文件文件夹的名字，自己定义，这里是放在根目录的upload文件夹  
            ),'modules'=>array('backup','member')
);
if (isset($frontendArray['components']['user']))
    unset($frontendArray['components']['user']);
return CMap::mergeArray($frontendArray, $backendArray);


