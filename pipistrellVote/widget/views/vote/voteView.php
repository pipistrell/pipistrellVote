<script type="text/javascript" src="<?php echo ASSETS; ?>vote/addvote.js"></script>
<style>
/*======中间部分内======*/
a{ text-decoration:none;}
/*分页*/
.endPageNum{ clear:both; font-size:12px; text-align:center;}
.endPageNum table{ margin:auto;}
.endPageNum .s1{width:52px;}
.endPageNum .s2{background:#1f3a87; border:1px solid #ccc; color:#fff; font-weight:bold;}
.endPageNum a.s2:visited {color:#fff;}
.endPageNum a{padding:2px 5px;margin:5px 4px 0 0; color:#1F3A87;background:#fff; display:inline-table; border:1px solid #ccc; float:left;}
.endPageNum a:visited{color:#1f3a87;} 
.endPageNum a:hover{color:#fff; background:#1f3a87; border:1px solid #1f3a87;float:left; text-decoration:underline;}
.endPageNum .s3{cursor:default;padding:2px 5px;margin:5px 4px 0 0; color:#ccc;background:#fff; display:inline-table; border:1px solid #ccc; float:left;}
.clear{ clear:both;}
.editor{ float:left; font-size:12px; margin:11px 0; width:545px; text-align:right;}
#newscontent{ width:95%; margin:0 auto; padding:10px;}
#newsconttitle a:hover{ color:#04d;}
#newsconttitle span{ float:left;}
#newsconttitle h1{ font-size:14px; font-weight:bold; color:#666; padding:0; margin:0;}
#newsconttitle p{ width:100%; height:20px; line-height:20px; float:left; padding:15px 0; margin:0; color:#666; border-bottom:1px #ddd solid; text-align:left;}
#newsconttitle p a{ width:410px;height:20px; overflow:hidden; display:block; color:#1e6bc5;word-wrap: break-word;word-break: normal; float:left;}	

#newsummary{margin: 10px 0px 0px;padding: 12px 5px 6px;width:98%; background:#FFF;border: 1px solid #DCDDDD; }
#newsummary h2{text-indent: 2em;font-size: 14px;line-height: 20px;color:#666; font-weight:500;}
#newcontent{ line-height:22px; color:#666; margin:10px 5px 0 5px; float:left; display:inline;}
#newcontent p{ text-indent:24px; padding:0; margin:0;}
#newcontent p a:hover{ color:#04d;}
#newsleft .ad{ margin:10px 0; float:left;}
#newsright{ float:left; width:350px; margin-left:10px; display:inline;}
#articeBottom {font-size: 14px; margin: 6px 0 10px; padding-top: 10px; text-align: right; width: 97%;}
#articeBottom span{ float:left;}
#articeBottom span a{ font-size:12px;}
#articeBottom span a:hover{ color:#0099FF;}
#articleHeader { margin:5px 0; padding:10px;background:#F5F5F5; height:60px;}
#articleHeader h4{font-size:12px; color:#333; height:20px;}
#articleHeader h4 a{ font-size:12px; color:#333}
.details h2{ background:#F8F8FF; border: 1px solid #DDD; line-height: 25px; margin-bottom: 15px;  padding: 10px; font-size:14px; text-indent:28px;}

#newlist{ width:98%; margin-top:10px;}
#newlist ul { width:100%; height:auto;}
#newlist li{ width:170px; border-bottom:#ccc 1px dashed; list-style:none; padding:10px; margin-left:20px; display:inline;border: 1px #eee solid; text-align: center; float:left;}

#newlist li  img { width:170px; height:120px;}
#newlist li  span { width:100%; height15px; line-height:15px; display:block; color:#666;}
#newlist li:hover h3 {color:red; }
/************ Button ************/
.button {
				font-family: Verdana, Arial, sans-serif;
                display: inline-block;
                background: #459300 url('<?php echo ASSETS; ?>admin/resources/images/bg-button-green.gif') top left repeat-x !important;
                border: 1px solid #459300 !important;
                padding: 4px 7px 4px 7px !important;
                color: #fff !important;
                font-size: 11px !important;
                cursor: pointer;
                }
                
.button:hover {
                text-decoration: underline;
                }
                
.button:active {
                padding: 5px 7px 3px 7px !important;
                }
.voteBox { width:100%; height:auto; margin-top:20px; display:block; }

.rleft { float:left; display:inline; width:25%; margin-left:5%;}
.rRight { float:right; display:inline; width:55%; text-align:right;}
</style>
<?php $has = Yii::app()->cache->get($id.$_SERVER['REMOTE_ADDR']); 
       $now = time()-(8*3600);
       $t = $now>=$startime&&$now<=$endtime;
?>
<div id="newscontent">
	<div id="newsconttitle">
		<h1 style="text-align:center;"><?php echo $title; ?></h1>
		<p><span >总票数：<?php echo $countVote; ?></span></p>
	</div>
    <div class="clear"></div>
    <input type="hidden" id="aid" value="<?php echo $id; ?>" />
    <?php echo $description; ?>
	<div id="newlist">
		<ul >
            <?php
            $data = Addvote_infos::model()->findAll(' aid=:aid ',array(':aid'=>$id));
	if( !empty( $data ) )
	{
		foreach($data as $k =>$value)
		{

			?>
            <li>
                
                    <img src="<?php echo empty($value['litpic'])?ASSETS.'base/NOIMG.jpg':UPFILE.$filetemp.'/'.$value['litpic']; ?>" />
                    <h3><?php echo $value['votename']; ?></h3>
                    <span>票数：<?php echo $value['count']; ?></span>
                    <?php if($t): ?>
                    <?php $a = '';if(!$has):
                        $a = 'rRight';
                        ?>
                    <a class="button rleft" href="javascript:;" onclick="$.avote.putVote('<?php echo CHtml::normalizeUrl(array('request/putVote')); ?>','<?php echo $value['vid']; ?>');" >投票</a>
                    <?php endif; ?>
                    <div class="bdsharebuttonbox <?php echo $a; ?>"><a href="#" class="bds_more" data-cmd="more">为他拉票：</a></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"share":{"bdSize":16}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
                    <?php else: ?>
                     <span>投票已结束</span>
                    <?php endif; ?>
                    </li>
		<?php			
		}
	}
	else
	{
		echo '暂无信息。';
	}
?>
			

				</ul>
	</div>
    <div class="clear"></div> 
    

        <?php if($has||!$t): ?>
        <div class="voteBox">
        <link rel="stylesheet" href="<?php echo ASSETS; ?>vote/css/style.css" media="screen" type="text/css" />
        <h3>投票情况</h3>
        <?php $colorArr = array(
            array(
                'tc'=>'#d35400','bc'=>'#e67e22'
            ),
            array(
                'tc'=>'#124e8c','bc'=>'#4288d0'
            ),
            array(
                'tc'=>'#2c3e50','bc'=>'#2c3e50'
            ),
            array(
                'tc'=>'#46465e','bc'=>'#5a68a5'
            ),
            array(
                'tc'=>'#333333','bc'=>'#525252'
            ),
            array(
                'tc'=>'#27ae60','bc'=>'#2ecc71'
            ),
            array(
                'tc'=>'#124e8c','bc'=>'#4288d0'
            )
            
        ); ?>
        <?php if(!empty($data)):
            foreach($data as $v){
              if($countVote==0||$v['count']==0){
               $bf = 0;   
              }else{
               $bf = floor(($v['count']/$countVote)*100);
              }
               $num = rand(0,6);
            ?>
        <div class="skillbar clearfix " data-percent="<?php echo $bf; ?>%">
	<div class="skillbar-title" style="background: <?php echo $colorArr[$num]['tc']; ?>;"><span><?php echo $v['votename']; ?></span></div>
	<div class="skillbar-bar" style="background: <?php echo $colorArr[$num]['bc']; ?>;"></div>
	<div class="skill-bar-percent"><?php echo $bf; ?>%</div>
        </div>  
        <?php }endif; ?>
        <script type="text/javascript">
            $(function(){
                $('.skillbar').each(function(){
		$(this).find('.skillbar-bar').animate({
			width:$(this).attr('data-percent')
		},6000);
	});  
            });
           
       </script>
       </div>
        <?php endif; ?>
    
</div>
<div style="border:1px dashed #999999"></div>
<div id="articleHeader">
<?php 
	/*
		以下程序添加于2014-12-11
		author:pipistrell
	*/
	if($paging['after'])
	{
	?>
        <h4>下一篇：<a href="<?php Bjtcms::Syshref($paging['after']['id'],'info',$paging['after']['channelId'])?>"><?php echo $paging['after']['title']; ?></a></h4>
	<?php 
	}	
	if($paging['before'])
	{
	?>
	<h4>上一篇：<a href="<?php Bjtcms::Syshref($paging['before']['id'],'info',$paging['before']['channelId'])?>"><?php echo $paging['before']['title']; ?></a></h4>
	<?php 	
	}	
?>
</div>
