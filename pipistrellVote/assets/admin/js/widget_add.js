var lock = 0;
$.extend({
    widgetAdd:{
        infoNextStep:function(){
            if(lock){
                $.showLoading('<div class="notification success png_bg"><div>请求已提交，请耐心等待！</div></div>');return;
            }
            var widgetName = $('#widgetName').val();
            var widgetNameCn = $('#widgetNameCn').val();
            var widgetFile = $('#widgetFile').val();
            var description = $('#description').val();
            var ismobile = $(":radio[name='ismobile']:checked").val();
            var enable = $(":radio[name='enable']:checked").val();
            if(widgetName == '' || widgetNameCn == '' || widgetFile == '' || description == '' || ismobile == undefined || ismobile == '' || enable == undefined || enable == ''){
                $.showMsg('<div class="notification attention png_bg"><div>您所提交的信息不完整，请完整填写后再次提交！</div></div>');return;
            }
            $.showLoading('<div class="notification success png_bg"><div>请求已提交，请耐心等待！</div></div>');
            lock = 1;
            $.ajax({
                type:'post',
                data:'widgetName='+widgetName+'&widgetNameCn='+widgetNameCn+'&widgetFile='+widgetFile+'&description='+description+'&ismobile='+ismobile+'&enable='+enable,
                url:WEB_IN+'?r=widget/putInDb',
                success:function(data){
                    lock = 0;
                    if(data == 2){
                        $.showMsg('<div class="notification attention png_bg"><div>挂件名称重复，请更换</div></div>');return;
                    }
                    window.location.href = WEB_IN+'?r=widget/add&step=2';
                }
            });
        },
        isUploadC:function(){
            $('#isUpload').val(1);
        },
        nextStep:function(){
            var is_upload = $('#isUpload').val();
            if(!is_upload){$.showMsg('<div class="notification attention png_bg"><div>您未上传任何文件！</div></div>');return;}
            window.location.href = WEB_IN+'?r=widget/add&step=3';
        },
        finish:function(){
            var is_upload = $('#isUpload').val();
            if(!is_upload){$.showMsg('<div class="notification attention png_bg"><div>您未上传任何文件！</div></div>');return;}
            $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">3</i>秒后返回挂件管理</div></div>');
            $.post(WEB_IN+'?r=widget/clear');
            var i = 3;
            window.setInterval(function(){
                if(i > 1){
                    i--;
                    $('#scerdShow').html(i);return;
                }
                window.location.href = WEB_IN+'?r=widget';
            },1000); 
            return;
        }
    }
});