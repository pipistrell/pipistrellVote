<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>images/style.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>banner/css/style.css" />
        <script type="text/javascript" src='<?php echo ASSETS; ?>vote/js/jquery.js'></script>
    </head>
    <body >
        <div id="cjrl_top">
            <?php
            $this->beginContent('//layouts/head');
            echo $content;
            $this->endContent();
            ?>
            <div class="clear"></div>

            <?php echo $content; ?>

            <?php
            $this->beginContent('//layouts/foot');
            echo $content;
            $this->endContent();
            ?>
    </body>
</html>
