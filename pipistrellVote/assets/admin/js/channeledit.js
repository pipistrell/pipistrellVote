
$.extend({
    channeledit:{
        changeWidget:function(th,pkid){
            $('div .success').attr('class','notification information');
            $(th).attr('class','notification success');
            $('#wid').val(pkid);
            if(pkid == 0){
            var abs = $(th).attr('abs');
            $('#temp').val(abs);
            $('#comtemp').val(abs+'View');
            $('#tempName').val(abs);
            }else{
            $('#temp').val(widget[$(th).attr('id')].widgetFile);
            $('#comtemp').val(widget[$(th).attr('id')].widgetFile+'View');
            $('#tempName').val(widget[$(th).attr('id')].widgetName);
            }
        },
        putDataDb:function(){
            var wid = $('#wid').val();
            var ordering = $('#ordering').val();
            var typename = $('#typename').val();
            var typenameEN = $('#typenameEN').val();
            var keywords = $('#keywords').val();
            var description = $('#description').val();
            var ismobile = $(':radio[name="ismobile"]:checked').val();
            var display = $(':radio[name="display"]:checked').val();
            var submission = $(':radio[name="submission"]:checked').val();
            var topId = $('#topId').val();
            var temp = $('#temp').val();
            var comtemp = $('#comtemp').val();
            var coverin = $('#coverin').val();
            var smallcoverin = $('#smallcoverin').val();
            var article_content = editor_article_content.html();
            var channelId = $('#channelId').val();
            var filetemp = '';
            var tempName = $('#tempName').val();
            var url = WEB_IN+'?r=channel/editdata';
            var commonTemp = $('#commonTemp').val();
            if(channelId == 0){
                filetemp = $('#filetemp').val();
                url = WEB_IN+'?r=channel/adddata';
            }
            if(channelId == '' || channelId == undefined){
                $.showMsg('<div class="notification error png_bg"><div>参数错误，请联系管理员！</div></div>');
                return;
            }
            if(typename == '' || typenameEN == ''){
                $.showMsg('<div class="notification attention png_bg"><div>栏目中英文名称不能为空！</div></div>');
                return;
            }

            if(comtemp == '无')comtemp = '';
            if(ordering == ''|| ordering == undefined)ordering=0;
            if(topId == '' || topId == undefined)topId=0;
            $.post(url,{
                'wid':wid,
                'ordering':ordering,
                'typename':typename,
                'typenameEN':typenameEN,
                'keywords':keywords,
                'description':description,
                'ismobile':ismobile,
                'display':display,
                'topId':topId,
                'temp':temp,
                'comtemp':comtemp,
                'coverin':coverin,
                'smallcoverin':smallcoverin,
                'article_content':article_content,
                'channelId':channelId,
                'filetemp':filetemp,
                'tempName':tempName,
                'commonTemp':commonTemp,
                'submission':submission
            },
            function(data){
                if(data == ''){
                    $.showMsg('<div class="notification attention png_bg"><div>没有数据修改！</div></div>');
                    return;
                }else if(data == 1){
                    $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">3</i>秒后返回栏目管理</div></div>');
                    var i = 3;
                    window.setInterval(function(){
                        if(i > 1){
                            i--;
                            $('#scerdShow').html(i);
                            return;
                        }
                        window.location.href = WEB_IN+'?r=channel';
                    },1000); 
                    return;
                }else{
                    $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
                    return;
                }
            });
        }
    }
});