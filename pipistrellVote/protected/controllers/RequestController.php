<?php
Class RequestController extends Controller{
    public function actionPutVote(){
        if(!isset($_POST['aid'])&&empty($_POST['aid'])){
            echo '参数错误！';return;
        }
        $has = Yii::app()->cache->get($_POST['aid'].$_SERVER['REMOTE_ADDR']);
        if($has){
            echo '您已参与过投票了！';return;
        }
        Yii::app()->cache->set($_POST['aid'].$_SERVER['REMOTE_ADDR'],'true');
        $obj = Addvote_infos::model()->findByPk($_POST['vote']);
        if(empty($obj)){
            echo '参数错误！';return;
        }
        $a = Addvote::model()->findByPk($_POST['aid']);
        $obj->count = $obj->count+1;
        $a->countVote = $a->countVote+1;
        $obj->update();
        $a->update();
        echo '1';
    }
    public function actionAjaxPaging(){
        
        if( Bjtcms::_checkSqlStr($_POST['id']) || Bjtcms::_checkSqlStr($_POST['row']) || Bjtcms::_checkSqlStr($_POST['count']) || Bjtcms::_checkSqlStr($_POST['page'])){
            echo 2;return;
        }
        $has = '';
        if(CACHEABLE)$has = Yii::app()->cache->get('pagingImg'.$_POST['id'].$_POST['page'].$_POST['row']);
        if(!$has){
            $limit = ($_POST['page']-1) * $_POST['row'];
            $limit = ' limit '.$limit.','.$_POST['row'];
            $order = ' order by ordering asc ';
            $sql = 'select filetemp,id,title,keywords,description,click,Slitpic,litpic,source,Awrite,senddate,msgCount from {{article}} where arcrank = 1 and (channelId = "'.$_POST['id'].'" or find_in_set("'.$_POST['id'].'",channelIdAll)) '.$order.$limit;
            $arr = Yii::app()->db->createCommand($sql)->queryAll();
            
            if(is_array($arr)){
                $str1 = '';
                foreach($arr as $v){
                      $str1 .= '<div class="sjlist" ><a href="'.CHtml::normalizeUrl(array('ins/info','id'=>$v['id'],'pid'=>$_POST['id'])).'">'.$v['title'].'</a> ';
                      $str1 .= '<p>'.date('Y-m-d',$v['senddate']+(8*3600)).'</p></div>';
                }
            }
            if(CACHEABLE)Yii::app()->cache->set('pagingImg'.$_POST['id'].$_POST['page'].$_POST['row'],$str1);
        }else{
            $str1 = $has;
        }
            echo $str1;
    }
    public function actionAjaxOnebyone(){
        
        if( Bjtcms::_checkSqlStr($_POST['id']) || Bjtcms::_checkSqlStr($_POST['row']) || Bjtcms::_checkSqlStr($_POST['count']) || Bjtcms::_checkSqlStr($_POST['page'])){
            echo 2;return;
        }
        $has = '';
        if(CACHEABLE)$has = Yii::app()->cache->get('pagingOne'.$_POST['id'].$_POST['page'].$_POST['row']);
        if(!$has){
            $limit = ($_POST['page']-1) * $_POST['row'];
            $limit = ' limit '.$limit.','.$_POST['row'];
            $order = ' order by ordering asc ';
            $sql = 'select filetemp,id,title,keywords,description,click,Slitpic,litpic,source,Awrite,senddate,msgCount from {{article}} where arcrank = 1 and (channelId = "'.$_POST['id'].'" or find_in_set("'.$_POST['id'].'",channelIdAll)) '.$order.$limit;
            $arr = Yii::app()->db->createCommand($sql)->queryAll();
            
            if(is_array($arr)){
                $str1 = '';
                foreach($arr as $v){
                      $str1 .= '<dl><span><a href="'.CHtml::normalizeUrl(array('ins/info','id'=>$v['id'],'pid'=>$_POST['id'])).'">&nbsp;'.$v['title'].'</a></span> ';
                      $str1 .= '<dt><a href="'.CHtml::normalizeUrl(array('ins/info','id'=>$v['id'],'pid'=>$_POST['id'])).'"><img src="'.UPFILE.$v['filetemp'].'/'.$v['litpic'].'"></a></dt>';
                      $str1 .= '<dd>'. mb_substr($value['description'],0,10,'utf-8' ).'...<a class="onea" href="'.CHtml::normalizeUrl(array('ins/info','id'=>$v['id'],'pid'=>$_POST['id'])).'">阅读原文 ></a></dd></dl>';
                }
            }
            if(CACHEABLE)Yii::app()->cache->set('pagingOne'.$_POST['id'].$_POST['page'].$_POST['row'],$str1);
        }else{
            $str1 = $has;
        }
            echo $str1;
    }
    public function actionAjaxPagingList(){
        if( Bjtcms::_checkSqlStr($_POST['id']) || Bjtcms::_checkSqlStr($_POST['row']) || Bjtcms::_checkSqlStr($_POST['count']) || Bjtcms::_checkSqlStr($_POST['page'])){
            echo 2;return;
        }
        $has = '';
        if(CACHEABLE)$has = Yii::app()->cache->get('pagingList'.$_POST['id'].$_POST['page'].$_POST['row']);
        if(!$has){
            $limit = ($_POST['page']-1) * $_POST['row'];
            $limit = ' limit '.$limit.','.$_POST['row'];
            $order = ' order by ordering asc ';
            $sql = 'select filetemp,id,title,keywords,description,click,Slitpic,litpic,source,Awrite,senddate,msgCount from {{article}} where arcrank = 1 and (channelId = "'.$_POST['id'].'" or find_in_set("'.$_POST['id'].'",channelIdAll)) '.$order.$limit;
            $arr = Yii::app()->db->createCommand($sql)->queryAll();
            
            if(is_array($arr)){
                $str1 = '';
                foreach($arr as $v){
                      $str1 .= '<a href="'.CHtml::normalizeUrl(array('ins/info','id'=>$v['id'],'pid'=>$_POST['id'])).'"><dt class="main_dt"><img src="'.UPFILE.$v['filetemp'].'/'.$v['litpic'].'"></dt>';
                      $str1 .= '<dd class="main_dd"><h3 class="main_h3">'.mb_strcut($v['title'],0,15,'utf-8').'</h3><h4 class="main_h4">'.mb_strcut($v['description'],0,15,'utf-8').'...</h4>';
                      $str1 .= '<p class="main_p"><span class="main_sp1">'.date('Y-m-d',$v['senddate']+(8*3600)).'</span><span class="main_sp2">'.$v['msgCount'].'</span></p></dd></dl></a>';

                }
            }
            if(CACHEABLE)Yii::app()->cache->set('pagingList'.$_POST['id'].$_POST['page'].$_POST['row'],$str1);
        }else{
            $str1 = $has;
        }
            echo $str1;
    }
}
?>