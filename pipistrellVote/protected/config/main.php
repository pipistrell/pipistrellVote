<?php

// author:pipistrell
// email:289404562@qq.com

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => WEBNAME,
    'defaultController' => 'bjt',
    'preload' => array('log'),
    'theme' => 'defalut',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.admin.addfileds.*',
    ),
    'modules' => array('member',
    ),
    'components' => array(
         
        'cache' => array(
            'class' => 'system.caching.CFileCache',
            'directoryLevel' => '1', 
           
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;port=3306;dbname=voteBase',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'tablePrefix' => 'pipi_',
        ),
        'errorHandler' => array(
            'errorAction' => 'bjt/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName' => false, //去除index.php  
            'urlSuffix'=>'.html', //加上.html  
            'rules'=>array(
                'page<id:\d+>'=>'ins/page',
                'info<id:\d+>.<pid:\d+>'=>'ins/info',
                'member<mobile:\d+>'=>'member/default/index'
            ),
            ),
    ),
    
);