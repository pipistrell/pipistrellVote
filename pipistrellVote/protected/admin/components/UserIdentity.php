<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $user_model = Sysadmin::model()->find('user=:name',array(':name'=>$this->username));
            $return = 4;
            $back = array();
            $non = true;
            if($user_model === null || !isset($user_model->id)){
                $this -> errorCode = self::ERROR_USERNAME_INVALID;
                $return = 1;
                $non = false;
                $back = array('return'=>$return);
            } else if ($user_model->password !== md5('bjt'.$this -> password.'cms')){
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
                $return = 2;
                $non = false;
                $back = array('return'=>$return);
            } else {
                $this->errorCode=self::ERROR_NONE;
                $return = 9;
         
                $back = array('group'=>$user_model->groupid,'id'=>$user_model->id,'uname'=>$user_model->uname,'loginip'=>$user_model->loginip,'return'=>$return,'email'=>$user_model->email);
            }
            if($non){
             if($user_model->staus == 0){
                $return = 100;
                $back = array('return'=>$return);
             }
            }
            return $back;
	}
}