<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
Class Bjtcms {
    public static $string = '';
    public static $allTid= 0;
    public static $lock = false;
    public static function _channelAll($id=0,$cacheName='',$Container=array('tag'=>'ul','class'=>'','model'=>1),$Style=array('star'=>'<li><a href="~typelink~">~typename~</a>','end'=>'</li>')){
        
        $has = '';
        if(CACHEABLE && !empty($cacheName))$has = Yii::app()->cache->get($cacheName);
        if(!$has){
            $channel = Channel::model()->findAllByAttributes(array('pid'=>$id,'display'=>1));
            if($Container['model']==2){
                $class = empty($Container['class'])?'':' class="'.$Container['class'].'" ';
                self::$string .= '<'.$Container['tag'].$class.'>';
            }
            foreach($channel as $value){
                $StyleNow = '';
                $href = Yii::app()->createUrl('ins/page',array('id'=>$value->id));
                $exists = Channel::model()->exists('pid = :_pid and display = 1 ',array(':_pid'=>$value->id));
                $StyleNow = $Style['star'];
                $StyleNow = preg_replace("#[~]typename+[~]#", $value->typename, $StyleNow);
                $StyleNow = preg_replace("#[~]typelink+[~]#", $href, $StyleNow);
                self::$string .= $StyleNow;
                if($exists){
                    $sContainer = $Container;
                    $sContainer['model'] = 2;
                    self::_channelAll ($value->id,$cacheName,$sContainer,$Style);
                    }
                self::$string .= $Style['end'];    
            }
            if($Container['model']==2)self::$string .= '</'.$Container['tag'].'>';
            if(CACHEABLE && !empty($cacheName))Yii::app()->cache->set($cacheName,self::$string);
        }else{
            self::$string = $has;
        }
        return self::$string;
    }
    public static function timeChange($time,$model='Y-m-d'){
        $time = $time+(8*3600);
        echo date($model,$time);     
    }
    public static function Position($id,$typename,$pid=0,$index='/'){
        $has = '';
        $position = '';
        if(CACHEABLE)$has = Yii::app()->cache->get('position'.$id);
        if($has){
            $position = $has;
        }else{
            $position = '<a href="'.$index.'">首页&nbsp;&nbsp;</a>&gt;&gt;&nbsp;&nbsp;';
            if($pid){
                $position = self::_getPosition($pid,$position);
            }
            $position .= '<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$id)).'">'.$typename.'</a>';
            if(CACHEABLE)Yii::app()->cache->set('position'.$id,$position);
        }
        echo $position;
    }
    private function _getPosition($pid,$position){
        if($pid){
            $Pobj = Channel::model()->findByPk($pid);
            if($Pobj->pid)$position = self::_getPosition($Pobj->pid,$position);
            $position .= !empty($Pobj)?'<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$pid)).'">'.$Pobj->typename.'&nbsp;&nbsp;</a>&gt;&gt;&nbsp;&nbsp;':'';
           
        }
            return $position;
        
    }
    public static function Syshref($id,$model='page',$pid=''){
        if(!empty($pid)) 
            echo CHtml::normalizeUrl(array('ins/'.$model,'id'=>$id,'pid'=>$pid));
        else 
            echo CHtml::normalizeUrl(array('ins/'.$model,'id'=>$id));
    }
    public static function artList($channelId,$Darr=array(),$model='m'){

        $cacheName = isset($Darr['cache'])?$Darr['cache']:'';
        $lock = self::_checkCache($cacheName);
  
        if(!$lock){
            $order = isset($Darr['orderby'])?'order by '.$Darr['orderby'].' ':'order by ordering ';
            $order = isset($Darr['order'])?$order.$Darr['order']:$order.' asc';
            $row =  isset($Darr['row'])?' limit ' .$Darr['row']:' limit 10 ';
            $arr = array(
                'artTopId'=>$channelId,
                'isMobile'=>isset($Darr['mobile'])?$Darr['mobile']:0,
                'artFlag'=>isset($Darr['flag'])?$Darr['flag']:'',
                'arcrank'=>1
            );
           
            $content = self::_getArtList($arr,$order,$row,$model);
            if(CACHEABLE)Yii::app()->cache->set($cacheName,$content);
        }else{
            $content = $lock;
        }
        return $content;
    }
    private function _getArtList($arr,$order,$limit,$model){
        $where = self::_whereWork($arr);
        $where = !empty($where)?' where '.$where.$order.$limit:'';
        $search = ($model=='m')?' filetemp,channelId,title,id,litpic,Slitpic,description ':$model;
        $sql = 'select '.$search.' from {{article}} '.$where;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
   
    public static function banner($aid=1,$Darr=array(),$model='m'){

        $cacheName = isset($Darr['cache'])?$Darr['cache']:'';
        $lock = self::_checkCache($cacheName);
        if(!$lock){
            $order = isset($Darr['orderby'])?'order by '.$Darr['orderby'].' ':'order by ordering ';
            $order = isset($Darr['order'])?$order.$Darr['order']:$order.' asc';
            $row =  isset($Darr['row'])?' limit ' .$Darr['row']:' limit 4 ';
            $arr = array(
                'aId' => $aid,
                'isMobile'=>isset($Darr['mobile'])?$Darr['mobile']:0
            );
            $content = self::_getBanner($arr,$order,$row,$model);
            if(CACHEABLE)Yii::app()->cache->set($cacheName,$content);
        }else{
            $content = $lock;
        }
        return $content;
    }
    private function _getBanner($arr,$order,$limit,$model){
        $ismobile = '';
        if($arr['isMobile']){
            $ismobile = ' g.ismobile = 1 ';
        }
        unset($arr['isMobile']);
        $where = self::_whereWork($arr,$order,$limit,' g.');
        $where = !empty($where)?' where '.$where.$order.$limit:$where.$order.$limit;
        $search = ($model=='m')?' g.width,g.height,a.id,a.title,a.url,a.imgpath ':$model;
        $sql = ' select '.$search.' from  {{ad}} as g left join {{ad_info}} as a '.
                'on a.aid=g.aid '.$where;
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        return $data;
    }
    public static function channel($topid = 0,$Darr=array(),$model='m') {

        $cacheName = isset($Darr['cache'])?$Darr['cache']:'';
        $lock = self::_checkCache($cacheName);
        if (!$lock) {
            $order = isset($Darr['orderby'])?'order by '.$Darr['orderby'].' ':'order by ordering ';
            $order = isset($Darr['order'])?$order.$Darr['order']:$order.' asc';
            $row =  isset($Darr['row'])?' limit ' .$Darr['row']:' limit 10 ';
            $arr = array(
                'topId' => $topid,
                'isMobile'=>isset($Darr['mobile'])?$Darr['mobile']:0,
                'display'=>1
            );

            $content = self::_getChannel($arr, $order, $row,$model);
            if(CACHEABLE)Yii::app()->cache->set($cacheName,$content);
        }else{
            $content = $lock;
        }
        return $content;
    }

    private function _getChannel($arr, $order, $limit,$model) {
        
        $where = self::_whereWork($arr,$order,$limit);
        $where = !empty($where)?' where '.$where.$order.$limit:$where.$order.$limit;
        $search = ($model=='m')?' id,typename,typenameEN ':$model;
        $sql = ' select '.$search.' from  {{channel}} '.$where;
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        return $data;

    }
    private function _checkCache($cacheName){
        if (!empty($cacheName) && CACHEABLE) {
            $cache = Yii::app()->cache->get($cacheName);
            if (!empty($cache)) {
                $lock = 0;
                $content = $cache;
            }
            return $content;
        }else{
            return 0;
        }
    }
    private function _whereWork($arr, $order = '', $limt = '', $Tname = '') {

        $and = ' and ';
        $where = '';
        if (is_array($arr)) {
            $display = (!empty($arr['display'])&&!self::_checkSqlStr($arr['display']))?$Tname.'display = "'.$arr['display'].'" ':'';
            $aid = (!empty($arr['aId'])&&!self::_checkSqlStr($arr['aId']))?$Tname.'aid = "'.$arr['aId'].'" ':'';
            $channelId = (!empty($arr['channelId'])&&!self::_checkSqlStr($arr['channelId']))?$Tname.'id = "'.$arr['channelId'].'" ':'';
            $flag = (!empty($arr['artFlag'])&&!self::_checkSqlStr($arr['artFlag']))?$Tname.'flag = "'.$arr['artFlag'].'" ':'';
            $arcrank = (!empty($arr['arcrank'])&&!self::_checkSqlStr($arr['arcrank']))?$Tname.'arcrank = "'.$arr['arcrank'].'" ':'';
            $artTopId = (!empty($arr['artTopId'])&&!self::_checkSqlStr($arr['artTopId']))?' ('.$Tname.'channelId = "'.$arr['artTopId'].'" or find_in_set("'.$arr['artTopId'].'", '.$Tname.'channelIdAll)) ':'';
            $ismobile = !empty($arr['isMobile'])?$Tname.'ismobile = 1 ':'';
            $mid = (!empty($arr['mid'])&&!self::_checkSqlStr($arr['mid']))?$Tname.'mid = "'.$arr['mid'].'" ':'';
            $where = (!empty($arr['topId'])&&!self::_checkSqlStr($arr['topId'])) ? $where . $Tname . 'pid = "' . $arr['topId'] . '" ' : $where;
            $where = ($arr['topId'] == 0&&isset($arr['topId'])&&!self::_checkSqlStr($arr['topId'])) ? $where . $Tname . 'pid = "' . $arr['topId'] . '" ' : $where;
            $where = !empty($ismobile)&&!empty($where)?$where.$and.$ismobile:$where.$ismobile;;
            $where = !empty($aid)&&!empty($where)?$where.$and.$aid:$where.$aid;
            $where = !empty($flag)&&!empty($where)?$where.$and.$flag:$where.$flag;
            $where = !empty($arcrank)&&!empty($where)?$where.$and.$arcrank:$where.$arcrank;
            $where = !empty($artTopId)&&!empty($where)?$where.$and.$artTopId:$where.$artTopId;
            $where = !empty($channelId)&&!empty($where)?$where.$and.$channelId:$where.$channelId;
            $where = !empty($display)&&!empty($where)?$where.$and.$display:$where.$display;
            $where = !empty($mid)&&!empty($where)?$where.$and.$mid:$where.$mid;
            return $where;
        } else {
            return $where;
        }
    }
    public function _checkSqlStr($string) {
        $string = strtolower($string);
        return preg_match('/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|_user/i', $string);
    }
       public static function _getAllId($topId=0){
       if($topId == 0)return $topId;
       $channel = Channel::model()->findByPk($topId);
       if(self::$lock){
       if(self::$allTid == 0)self::$allTid = '';
       if(!empty(self::$allTid))self::$allTid .= ',';
       self::$allTid .= $channel->id;
       }else{
           self::$lock = true;
       }
       if($channel->pid != 0)self::_getAllId ($channel->pid);
       return self::$allTid;
   }
 
}

?>