<script type="text/javascript" src="<?php echo ASSETS;?>js/widget_add.js"></script>
<?php $this->beginContent('//layouts/widget_step',array('step'=>$step));
            echo $content;
            $this->endContent(); ?>

<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">新增挂件  → 录入挂件信息</h3>
        </div>
        
        <div class="content-box-content">
            <form id="step_1_form" onsubmit="return false;">
                
                <p>
              <label>挂件名称</label>
              <input class="text-input small-input" type="text" id="widgetName" name="small-input">
              <span class="input-notification attention png_bg">英文名称</span></p>
                <p>
              <label>挂件名称</label>
              <input class="text-input small-input" type="text" id="widgetNameCn" name="small-input">
              <span class="input-notification attention png_bg">中文名称</span></p>
                <p>
              <label>显示文件名</label>
              <input class="text-input small-input" type="text" id="widgetFile" name="small-input">
              <span class="input-notification attention png_bg">如：填写 product 则对应( product.php:列表页  productView.php：详情页 ) </span></p>
                <p >
              是否支持移动端：
              <input type="radio" name="ismobile" checked="checked" value ="1" >
              是
              <input type="radio" name="ismobile" value ="0" >
              否</p>
                <p >
              是否启用：
              <input type="radio" name="enable" checked="checked" value ="1" >
              是
              <input type="radio" name="enable" value ="0" >
              否</p>
                <p>
            <label>挂件描述：</label>
            <textarea class="text-input textarea" id="description" name="textfield"  rows="5" style="display: block;"></textarea>
        </p>
                <p>
                    <input class="button" type="submit"  onclick="$('#step_1_form')[0].reset();" value="重置">&nbsp;&nbsp;<input class="button" type="submit"  onclick="$.widgetAdd.infoNextStep();" value="下一步">
                </p>
            </form>
        </div>
    </div>
</div>
