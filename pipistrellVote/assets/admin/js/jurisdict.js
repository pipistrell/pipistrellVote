function delete_cfrm(url,json,alertmsg){
            $.confirm({
                'title'		: '信息提醒',
                'message'	: alertmsg,
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                           $.post(url,json,function(data){
                                   if(data == 1){
                                       $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">2</i>秒后返回栏目管理</div></div>');
                                        var i = 1;
                                        window.setInterval(function(){
                                           if(i > 1){
                                                i--;
                                                $('#scerdShow').html(i);
                                                return;
                                           }
                                           window.location.reload();
                                        },1000); 
                                    }else{
                                        $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
                                    }
                           });
                        }                   
                    },
                    'No'	: {
                        'class'	: 'gray',
                        'action': function(){
                            
                        }	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
}
function submite_cfrm(alertmsg){
            $.confirm({
                'title'		: '信息提醒',
                'message'	: alertmsg,
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                            var i=3;
                            window.setInterval(function(){
                                   if(i > 1){
                                        i--;
                                        $('#scerdShow').html(i);
                                        return;
                                    }
                                    window.history.back();
                            },1000);
                        }                   
                    },
                    'No'	: {
                        'class'	: 'gray',
                        'action': function(){
                            
                        }	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
}
function alert_cfrm(alertmsg){
            $.confirm({
                'title'		: '信息提醒',
                'message'	: alertmsg,
                'buttons'	: {
                    '确定'	: {
                        'class'	: 'blue',
                        'action': function(){
                            var i=1;
                            window.setInterval(function(){
                                   if(i > 1){
                                        i--;
                                        $('#scerdShow').html(i);
                                        return;
                                    }
                                    window.history.back();
                            },1000);
                        }                   
                    }
                }
            });
}