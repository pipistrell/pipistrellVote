<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class ChannelController extends CController {
   public $isType = 0;
   public $isSmallType = 0;
   public $layout='application.admin.views.layouts.main';
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter'
            )
        );
    }

    public function actionIndex() {
        $this->isType = 4;
        $this->isSmallType = $_GET['isSmallType'];
        $this->render('index',array(
            'data'=>$this->_getData()
        ));
    }

    private function _getData(){
        $where = adminSys::_whereWork($_POST);
        $sql = 'select id,typename,typenameEN,display,ordering,pid,ismobile from {{channel}} '.$where.' order by ordering asc ';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        if(is_array($data)){
            $bak = array('top'=>array(),'small'=>array());
            foreach ($data as $value){
                if($value['pid'] == 0){
                    $bak['top'][] = $value;
                }else{
                    $bak['small'][$value['pid']][] = $value;
                }
            }
        }
        return $bak;
    }
    
   public function actionOrderingUp(){
      
       if(empty($_POST['strOrder']))return;
           $data = explode(',', $_POST['strOrder']);
            foreach($data as $key=>$value){
                Channel::model()->updateByPk($value, array(
                    'ordering'=>(int)$_POST['ordering'][$key]
                ));
            }
        echo '<script>parent.reback("操作成功！");</script>';
   }
   public function actionChangeAtr(){
      $bak = Channel::model()->updateByPk($_POST['pkid'],array(
           $_POST['atr']=>$_POST['val']
       ));
      echo $bak;
   }
   public function actionEdit(){
       if(empty($_GET['id'])){
           $this->actionIndex ();return;
       }
       $path = Yii::app()->basePath.'/../themes/defalut/views/ins/';
       $commonArr = '';
       if(is_dir($path)){
           $commonArr = scandir($path);
       }
       $this->render('channelEdit',array(
           'data'=>Channel::model()->findByPk($_GET['id']),
           'widget'=>Widget::model()->findAll(array(
               'condition'=>' enable = :_enable ',
               'params'=>array(':_enable'=>1)
           )),
           'topData'=>Channel::model()->findAll(),
           'commonArr'=>$commonArr
       ));
   }
   public function actionDeleteCover(){
       $filetemp = dirname(Yii::app()->getBasePath()).$_POST['path'];
       if(is_file($filetemp))unlink ($filetemp);
   }
   public function actionEditData(){
       if(!IS_POST){$this->redirect (array('site/array','msg'=>'参数错误！'));return;}
       $channel = Channel::model()->findByPk($_POST['channelId']);
       
       if($channel->typename != $_POST['typename'])$channel->typename = $_POST['typename'];
       if($channel->typenameEN != $_POST['typenameEN'])$channel->typenameEN = $_POST['typenameEN'];
       if($channel->wid != $_POST['wid'])$channel->wid = $_POST['wid'];
       if($channel->widget != $_POST['tempName'])$channel->widget = $_POST['tempName'];
       if($channel->ordering != $_POST['ordering'])$channel->ordering = $_POST['ordering'];
       if($channel->keywords != $_POST['keywords'])$channel->keywords = $_POST['keywords'];
       if($channel->description != $_POST['description'])$channel->description = $_POST['description'];
       if($channel->ismobile != $_POST['ismobile'])$channel->ismobile = $_POST['ismobile'];
       if($channel->display != $_POST['display'])$channel->display = $_POST['display'];
       if($channel->pid != $_POST['topId'])$channel->pid = $_POST['topId'];
       if($channel->temp != $_POST['temp'])$channel->temp = $_POST['temp'];
       if($channel->comtemp != $_POST['comtemp'])$channel->comtemp = $_POST['comtemp'];
       if($channel->Cover != $_POST['coverin'])$channel->Cover = $_POST['coverin'];
       if($channel->smallCover != $_POST['smallcoverin'])$channel->smallCover = $_POST['smallcoverin'];
       if($channel->content != $_POST['article_content'])$channel->content = $_POST['article_content'];
       if($channel->Ctemp != $_POST['commonTemp'])$channel->Ctemp = $_POST['commonTemp'];
       if($channel->Submission != $_POST['submission'])$channel->Submission = $_POST['submission'];
       echo $channel->update();
   }
   public function actionAdd(){
       $path = Yii::app()->basePath.'/../themes/defalut/views/ins/';
       $commonArr = '';
       if(is_dir($path)){
           $commonArr = scandir($path);
       }
       $this->render('addChannel',array(
            'filetemp'=>time('Ymd'),
           'pid'=>isset($_GET['pid'])?(int)$_GET['pid']:0,
            'widget'=>Widget::model()->findAll(array(
               'condition'=>' enable = :_enable ',
               'params'=>array(':_enable'=>1)
               
           )),
           'topData'=>Channel::model()->findAll(),
            'commonArr'=>$commonArr
       ));
   }
   public function actionAdddata(){
       $channel = new Channel();
       $channel->wid = $_POST['wid'];
       $channel->ordering = $_POST['ordering'];
       $channel->typename = $_POST['typename'];
       $channel->typenameEN = $_POST['typenameEN'];
       $channel->keywords = $_POST['keywords'];
       $channel->description = $_POST['description'];
       $channel->ismobile = $_POST['ismobile'];
       $channel->display = $_POST['display'];
       $channel->pid = $_POST['topId'];
       $channel->widget = $_POST['tempName'];
       $channel->temp = $_POST['temp'];
       $channel->comtemp = $_POST['comtemp'];
       $channel->Cover = $_POST['coverin'];
       $channel->smallCover = $_POST['smallcoverin'];
       $channel->content = $_POST['article_content'];
       $channel->filetemp = $_POST['filetemp'];
       $channel->Ctemp = $_POST['commonTemp'];
       $channel->Submission = $_POST['submission'];
       echo $channel->save();
   }
   public function actionDeleteOne(){
       if(!IS_POST)$this->redirect (array('site/error','msg'=>'参数错误！'));
       $channel = Channel::model()->findByPk($_POST['pkid']);
       $path = dirname(Yii::app()->getBasePath()).'/upload/'.$channel->filetemp.'/';
       if(is_file($path.$channel->Cover))unlink($path.$channel->Cover);
       if(is_file($path.$channel->smallCover))unlink($path.$channel->smallCover);
       echo $channel->delete();
   }
}

?>