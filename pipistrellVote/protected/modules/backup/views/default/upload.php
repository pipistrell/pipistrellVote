<div id="main-content">
    
<div class="content-box">

      <div class="content-box-header">
        <h3 style="cursor: s-resize;">数据备份上传</h3>
      </div>
<?php
$this->breadcrumbs=array(
	'backup'=>array('backup'),
	'Upload',
);?>


<div class="form">


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'install-form',
	'enableAjaxValidation' => true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
));
?>
		<p class="config">
		<?php echo $form->labelEx($model,'upload_file'); ?>
		<?php echo $form->fileField($model,'upload_file'); ?>
		<?php echo $form->error($model,'upload_file'); ?>
		</p><!-- row -->	
                <p class="config">
                    <input type="submit" class="button" value="保存" />
                </p>
<?php
	
	$this->endWidget();
?>
</div><!-- form -->
</div>
</div>