<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>add/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>add/css/style.css" />
<script type="text/javascript" src="<?php echo ASSETS;?>js/channel.js"></script>
<style>
    p {padding:0;margin: 0;}
    .lineUnit {height:40px;  display: block; overflow: hidden; border-bottom: 1px #50b0d8 dashed;}
    .lineUnit a{float:right; height:40px; width:30px; display: block;}
    .lineUnit span {float:left;}
    .bottomRight { width: auto; display:block; text-align: right;}

</style>
<div id="main-content">
    
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">栏目管理</h3>
        </div>

        <div class="content-box-content">
            <ul class="shortcut-buttons-set">
        <li><a class="shortcut-button" href="<?php echo CHtml::normalizeUrl(array('channel/add')); ?>"><span> <img src="<?php echo ASSETS; ?>resources/images/icons/pencil_48.png" alt="icon"><br>
                    新增顶级栏目</span></a></li>

    </ul>
    <div class="clear"></div>
           <div class="notification information png_bg"> 
      <div>以下栏目正序排列</div>
    </div>
    <div class="notification attention png_bg"> 
      <div>点击栏目名称可进入栏目内容管理页面</div>
    </div>
            <div class="tree">
    <ul>
        <li>
            <form id="orderingForm" action="<?php echo CHtml::normalizeUrl(array('channel/orderingUp')); ?>" method="post" target="iframeOrd">
            <span><i class="icon-folder-open"></i>网站栏目</span>
            <ul id="webrootUl">
                <?php
                   if(is_array($data['top'])&&!empty($data['top'])):
                       foreach($data['top'] as $value):    
                ?>
                <li id="<?php echo $value['id']; ?>">
                    <p class="lineUnit" ><span <?php if(isset($data['small'][$value['id']]))echo 'class="badge badge-success"';?>>
                            <i class="icon-<?php if(isset($data['small'][$value['id']]))echo 'minus-sign';else echo 'leaf';?>" ></i> <b onclick="window.location.href='<?php echo CHtml::normalizeUrl(array('listart/index','artTopId'=>$value['id'],'isSmallType'=>$value['id'])); ?>'"><?php echo $value['typename'] ?>&nbsp;&nbsp;<?php echo $value['typenameEN'] ?></b>
                        
                        </span><input type="number" value="<?php echo $value['ordering']; ?>" name="ordering[]" id="<?php echo $value['id'];?>" style="float:right; height:18px; margin: 0px; padding:0px; width:30px; text-align: center;" />
                    <b style="float:right;">排序</b>
                        <?php if($value['ismobile']):?>
                    <a  href="#" id="ismobile<?php echo $value['id'] ?>" onclick="$.channel.changeAtr('<?php echo $value['id']; ?>','0','ismobile','禁用移动端','<?php echo ASSETS; ?>resources/images/icons/mobileno.png');" ><img src="<?php echo ASSETS; ?>resources/images/icons/mobile.png" alt="启用移动端" title="启用移动端"></a>
                        <?php else: ?>
                        <a  href="#" id="ismobile<?php echo $value['id'] ?>" onclick="$.channel.changeAtr('<?php echo $value['id']; ?>','1','ismobile','启用移动端','<?php echo ASSETS; ?>resources/images/icons/mobile.png');" ><img src="<?php echo ASSETS; ?>resources/images/icons/mobileno.png" alt="禁用移动端" title="禁用移动端"></a>
                        <?php endif; ?>
                        <?php if($value['display'] == 1): ?>
                        <a  href="#" id="display<?php echo $value['id']; ?>" onclick="$.channel.changeAtr('<?php echo $value['id']; ?>','0','display','隐藏','<?php echo ASSETS; ?>resources/images/icons/cross_circle.png');"><img src="<?php echo ASSETS; ?>resources/images/icons/tick_circle.png" alt="显示" title="显示"></a>
                        <?php else: ?>
                        <a  href="#" id="display<?php echo $value['id']; ?>" onclick="$.channel.changeAtr('<?php echo $value['id']; ?>','1','display','显示','<?php echo ASSETS; ?>resources/images/icons/tick_circle.png');" ><img src="<?php echo ASSETS; ?>resources/images/icons/cross_circle.png" alt="隐藏" title="隐藏"></a>
                        <?php endif; ?>
                        
                        <a  href="page<?php echo $value['id']; ?>.html" target="_blank"><img src="<?php echo ASSETS; ?>resources/images/icons/webview.png" alt="预览" title="预览"></a>
                        <a  href="#" onclick="$.channel.deleteOne('<?php echo $value['id']; ?>');" ><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="删除" title="删除"></a>
                        <a  href="<?php echo CHtml::normalizeUrl(array('channel/edit','id'=>$value['id'])) ?>" ><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="修改" title="修改"></a>
                        <a  href="<?php echo CHtml::normalizeUrl(array('channel/add','pid'=>$value['id'])); ?>" ><img src="<?php echo ASSETS; ?>resources/images/icons/add.png" alt="增加子栏目" title="增加子栏目"></a>
                    </p>
                    <?php if(isset($data['small'][$value['id']]))echo adminSys::_smallChannel($value['id'],$data['small']);?>
                </li>
                <?php endforeach;endif; ?>
            </ul>
            <input type="hidden" id="strOrder" name="strOrder" />
            </form>
            <iframe style="display:none;"  name="iframeOrd" ></iframe>
        </li>

    </ul>
                <div class="clear"></div>
                <div class="bottomRight">
                <a class="button" href="javascript:$.channel.updateOrd();">更新排序</a>
                </div>
</div>
            
            
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('.tree li:has(ul)').addClass('parent_li').find(' > p');
    $('.tree li.parent_li > p').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).find(' span > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).find(' span > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });

});

function reback(msg){
    $.showMsg('<div class="notification success png_bg"><div>'+msg+'</div></div>');
}
</script>