<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class JurisdictionController extends CController {
   public $isType = 0;
   public $isSmallType = 0;
   public $row = 10;
   public $page = 1;
   public $totalPage = 0;
   public $total = 0;
   
   public $row1 = 10;
   public $page1 = 1;
   public $totalPage1 = 0;
   public $total1 = 0;
   public $layout='application.admin.views.layouts.mainAD';
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter'
            )
        );
    }
    
    protected function _time($time, $model = null) {
        if ($model == 1) {
            return $time - (8 * 3600); //转格林威治时间
        } else if ($model == 2) {
            return $time + (8 * 3600); //转北京时间
        }
     }
     

     public function actionIndex() {        
        $this->page = isset($_GET['page'])&&(!empty($_GET['page']))?$_GET['page']:1;
        $this->isType = 9;
        $this->isSmallType = $_GET['isSmallType'];
        $DTC_arr=array();        
        $limit = ' limit '.(($this->page-1)*$this->row).','.$this->row;
        $where = adminSys::_whereWork($_POST);
        $sql = 'select * from {{sysadmin}} '.$where.$limit;
        $sql1 = 'select count(*) as count_num from {{sysadmin}} '.$where;
        $DTC_arr = Yii::app()->db->createCommand($sql)->queryAll(); 
        $DTC_arr_count = Yii::app()->db->createCommand($sql1)->queryAll();
        $this->totalPage = ceil($DTC_arr_count[0]['count_num']/$this->row);
        $this->total = $DTC_arr_count[0]['count_num'];
        $this->render('Cmng_Ctrl',array(              //局部渲染
            'Count_arr'=>$DTC_arr,
        ));
    }
     public function actionCaddRender() {      
        $this->render('Cadd_Ctrl',array(              //局部渲染        
        )); 
    }
    
    public function actionCountInsert(){
        if(!empty($_POST)){
            $DTC = new Sysadmin();
            $DTC->groupid=(int)$_POST['groupid'];
            $DTC->user=$_POST['user'];
            $DTC->password= md5('bjt'.$_POST['password'].'cms');
            $DTC->loginip='::1';
            $DTC->uname=$_POST['uname'];
            $DTC->logintime=(int)$this->_time(time(),1);
            $DTC->email=$_POST['email'];  
            $DTC->staus=(int)$_POST['status'];
            $DTC->save();
            echo $DTC->id; 
        }
    }
    public function actionCountDelete(){
         if(isset($_POST['id'])){
             if($_POST['id']<>1){
                $count = Sysadmin::model()->deleteByPk($_POST['id']);
                echo $count;
             }else{
                 echo -1;
             }
         }  else{
             echo 0;
         }
    }
    public function actionCountRender(){
        if(!empty($_GET)){
            $tar_C = Sysadmin::model()->findByPk($_GET['id']);
            $this->render('Cmodify_Ctrl',array(
                'id'=>$_GET['id'],
                'groupid'=>$_GET['groupid'],
                'user'=>$_GET['user'],
                'uname'=>$_GET['uname'],
                'password'=>$_GET['password'],
                'email'=>$_GET['email'],
                'status'=>$_GET['status']
            )); 
        }            
    }
    public function actionTarCRender(){
        if(!empty($_POST)){
            //$tar_C = Sysadmin::model()->findByPk($_POST['id']);
            $this->render('Cmodify_Ctrl',array(  
                'groupid'=>$_POST['groupid'],
                'user'=>$_POST['user'],
                'uname'=>$_POST['uname'],
                'password'=>$_POST['password'],
                'email'=>$_POST['email'],
                'status'=>$_POST['status']
            )); 
        }            
    }
   
    public function actionCountUpdate(){
        if(!empty($_POST)){
            $tar_C = Sysadmin::model()->findByPk($_POST['id']);            
            if($tar_C->groupid != $_POST['groupid'] && !empty($_POST['groupid']) && $_POST['groupid'] != 'undefined'){$tar_C->groupid = $_POST['groupid'];}
            if($tar_C->user != $_POST['user'] && !empty($_POST['user']) && $_POST['user'] != 'undefined'){$tar_C->user = $_POST['user'];}            
            $tar_C->password = md5('bjt'.$_POST['password'].'cms');           
            if($tar_C->uname != $_POST['uname'] && !empty($_POST['uname']) && $_POST['uname'] != 'undefined'){$tar_C->uname = $_POST['uname'];}            
            if($tar_C->email != $_POST['email'] && !empty($_POST['email']) && $_POST['email'] != 'undefined'){$tar_C->email = $_POST['email'];}            
            if($tar_C->staus != $_POST['status'] && !empty($_POST['status']) && $_POST['status'] != 'undefined'){$tar_C->staus = $_POST['status'];}
            $tar_C->update();  
            $bak=$tar_C->id;            
            echo $bak;
        }        
        else{
            echo 0;
        }
    }
        
    
     public function actionCountSearch() {        
        $this->page = isset($_GET['page'])?$_GET['page']:1;
        $this->isType = 9;
        $this->isSmallType = $_GET['isSmallType'];
        $DTC_arr=array();        
        $limit = ' limit '.(($this->page-1)*$this->row).','.$this->row;
        $where = adminSys::_whereWork($_POST);
        $sql = 'select * from {{sysadmin}} '.$where.$limit;
        $sql1 = 'select count(*) as count_num from {{sysadmin}} '.$where;
        $DTC_arr = Yii::app()->db->createCommand($sql)->queryAll(); 
        $DTC_arr_count = Yii::app()->db->createCommand($sql1)->queryAll();
        $this->totalPage = ceil($DTC_arr_count[0]['count_num']/$this->row);
        $this->total = $DTC_arr_count[0]['count_num'];
        $this->render('Cmng_Ctrl',array(              //局部渲染
            'Count_arr'=>$DTC_arr,
        ));
    }
}

