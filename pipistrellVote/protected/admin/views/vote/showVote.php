<script type="text/javascript" src="<?php echo ASSETS;?>js/mail.js"></script>
<div id="main-content">
    <div class="content-box">
        <div class="content-box-header" >
            <h3 style="cursor: s-resize; float: left;"><a href="/">首页</a>&nbsp;>>&nbsp;<a href="<?php echo CHtml::normalizeUrl(array('channel/index','isSmallType'=>1)); ?>">栏目管理</a>&nbsp;>>&nbsp;<?php echo $title; ?>投票情况</h3>
           
        </div>

        <div class="clear"></div>
        <div class="content-box-content">
        <h3>总票数：<?php echo $a->countVote; ?></h3>
            <link rel="stylesheet" href="<?php echo ASSETS; ?>../vote/css/style.css" media="screen" type="text/css" />
        <?php $colorArr = array(
            array(
                'tc'=>'#d35400','bc'=>'#e67e22'
            ),
            array(
                'tc'=>'#124e8c','bc'=>'#4288d0'
            ),
            array(
                'tc'=>'#2c3e50','bc'=>'#2c3e50'
            ),
            array(
                'tc'=>'#46465e','bc'=>'#5a68a5'
            ),
            array(
                'tc'=>'#333333','bc'=>'#525252'
            ),
            array(
                'tc'=>'#27ae60','bc'=>'#2ecc71'
            ),
            array(
                'tc'=>'#124e8c','bc'=>'#4288d0'
            )
            
        ); ?>
        <?php if(!empty($data)):
            $all = 0;
            foreach($data as $v){
               $bf = floor(($v['count']/$a->countVote)*100);
               $all += $v['count'];
               $num = rand(0,6);
            ?>
        <div class="skillbar clearfix " data-percent="<?php echo $bf; ?>%">
	<div class="skillbar-title" style="background: <?php echo $colorArr[$num]['tc']; ?>;"><span><?php echo $v['votename']; ?></span></div>
	<div class="skillbar-bar" style="background: <?php echo $colorArr[$num]['bc']; ?>;"></div>
	<div class="skill-bar-percent"><?php echo $bf; ?>%</div>
        </div>  
        <?php }endif; ?>
            
        <script type="text/javascript">
            $(function(){
                $('.skillbar').each(function(){
		$(this).find('.skillbar-bar').animate({
			width:$(this).attr('data-percent')
		},6000);
	});  
            });
           
       </script>
            
        </div>

    </div>
    
   
    
</div>
