<script type="text/javascript" src="<?php echo ASSETS; ?>js/listartEdit.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>laydate/laydate.dev.js"></script>
<div id="main-content">

    <div class="content-box">

        <div class="content-box-header">
            <h3 style="cursor: s-resize;">内容修改</h3>
            <input class="button" style="float:right; margin-top: 7px; margin-right: 15px;" type="submit" onclick="$.listEdit.readyData();" value="保存">
        </div>

        <div class="content-box-content">
            <div class="tab-content default-tab" id="tab1" style="display: block;">
                <form id="editForm" action="<?php echo CHtml::normalizeUrl(array('listart/editsave')); ?>" method="post" target="ifHidden">
                    <input type="hidden" name="modelName" value="<?php echo $modelName; ?>" />
                    <input type="hidden" name="pkid" value="<?php echo $data['id']; ?>" />
                    <fieldset>
                        <table style="width:700px;">
                            <tr>
                                <td >
                                    <label>标题</label><input value="<?php echo $data['title']; ?>" class="text-input larger-input " type="text" name="title" >
                                </td>
                                <td>
                                    <label>排序</label><input value="<?php echo $data['ordering']; ?>"  class="text-input small-input" type="text" name="ordering" onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');if(this.value.split('.').length>2){this.value=this.value.split('.')[0]+'.'+this.value.split('.')[1]}" >
                                </td>
                            </tr>
                            <tr>
                                <td ><label>关键词</label><input value="<?php echo $data['keywords']; ?>"  class="text-input larger-input" type="text" name="keywords" ></td>
                                <td ><label>描述</label><input value="<?php echo $data['description']; ?>" class="text-input larger-input " type="text" name="description" ></td>
                            </tr>
                            <tr>
                                <td>
                                    <label>来源</label>
                                    <input  class="text-input larger-input" value="<?php echo $data['source']; ?>" type="text" name="source">
                                </td>
                                <td>
                                    <label>来源链接</label>
                                    <input class="text-input larger-input" value="<?php echo $data['sourceURL']; ?>" type="text" name="sourceURL" >  
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>作者</label>
                                    <input class="text-input larger-input" value="<?php echo $data['Awrite']; ?>" type="text" name="awrite" >
                                </td>
                                <td></td>
                            </tr>
                        </table>


                        <p class="config">
                            移动端显示：
                            <input type="radio" name="ismobile" value ="1" <?php if ($data['ismobile']):echo 'checked="checked" ';
endif;
?>>
                            是
                            <input type="radio" name="ismobile" value ="0" <?php if (!$data['ismobile']):echo 'checked="checked" ';
                                   endif;
?>>
                            否
                        </p>
                        <p class="config">
                            属性：
                            <input type="radio" name="flag" value ="0" <?php if ($data['flag'] == '0'):echo 'checked="checked" ';
                                   endif;
?>>
                            无
                            <input type="radio" name="flag" value ="s" <?php if ($data['flag'] == 's'):echo 'checked="checked" ';
                                   endif;
?>>
                            滚动
                            <input type="radio" name="flag" value ="h" <?php if ($data['flag'] == 'h'):echo 'checked="checked" ';
                                   endif;
?>>
                            头条
                            <input type="radio" name="flag" value ="f" <?php if ($data['flag'] == 'f'):echo 'checked="checked" ';
                                   endif;
?>>
                            幻灯
                            <input type="radio" name="flag" value ="c" <?php if ($data['flag'] == 'c'):echo 'checked="checked" ';
                                   endif;
?>>
                            推荐
                        </p>
                        <div class="content-box column-left">
                            <div class="content-box-header">
                                <h3 style="cursor: s-resize;">内容封面</h3>
                            </div>
                            <div class="content-box-content">
                                <div class="tab-content default-tab" style="display: block;">
                                    <h4>内容封面</h4>
                                    <p>

                                        <input type="hidden" id="coverin" name="litpic"  value="<?php echo $data['litpic']; ?>" />
                                        <img id="cover" class="coverImg" src="<?php echo empty($data['litpic']) ? ASSETS . 'base/NOIMG.jpg' : '/upload/' . $data['filetemp'] . '/' . $data['litpic']; ?>" />
                                        <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index', 'fname' => '/upload/' . $data['filetemp'], 'model' => 2)) ?>" ></iframe>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="content-box column-right">
                            <div class="content-box-header">
                                <h3 style="cursor: s-resize;">内容封面（移动版）</h3>
                            </div>
                            <div class="content-box-content">
                                <div class="tab-content default-tab" style="display: block;">
                                    <h4>内容封面（移动版）</h4>
                                    <p>
                                        <input type="hidden" name="Slitpic" id="smallcoverin"  value="<?php echo $data['Slitpic']; ?>" />
                                        <img id="smallcover" class="coverImg" src="<?php echo empty($data['Slitpic']) ? ASSETS . 'base/NOIMG.jpg' : '/upload/' . $data['filetemp'] . '/' . $data['Slitpic']; ?>" />
                                        <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index', 'fname' => '/upload/' . $data['filetemp'], 'model' => 3)) ?>" ></iframe>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <?php
                        if (is_array($attribute)):
                            $arr = array();
                            $str = '';
                            foreach ($attribute as $key => $value):
                                if ($addfileds[$value] == 'selectAdr') {
                                    
                                } else if ($addfileds[$value] != 'upload'):
                                    ?>
                                    <p class="config">

                                    <?php else: ?>
                                    <div class="config" style="margin-bottom:10px;">

                                    <?php
                                    endif;
                                    if (!empty($addCN[$value])):
                                        ?>
                                        <label><?php echo $addCN[$value]; ?></label>  
                                        <?php
                                    endif;
                                    if ($addfileds[$value] == 'htmltext') {
                                        $arr[] = $value;
                                        ?>
                                        <div id="<?php echo $value ?>"><?php echo $data[$value] ?></div>
                                        <?php
                                        $this->widget('ext.KEditor.KEditor', array(
                                            'name' => $value, //设置name
                                            'id' => $value,
                                            'textareaOptions' => array(
                                                'style' => 'width:98%;height:400px;',
                                            )
                                        ));
                                        ?>
                                    <?php
                                    } else if ($addfileds[$value] == 'select') {
                                        if ($value == 'industry') {
                                            ?>
                                            <script type="text/javascript" src="<?php echo ASSETS; ?>../member/js/Industry.js"></script>
                                            <?php
                                            echo adminSys::_getIndustry('1', $value, $data[$value], $data['Small']);
                                        } else {
                                            echo adminSys::_getAddSelect($value, $data[$value], $modelName);
                                        }
                                    } else if ($addfileds[$value] == 'selectAdr') {
                                        if ($value == 'Province') {
                                            ?>
                                            <script type="text/javascript" src="<?php echo ASSETS; ?>../member/js/resume.js"></script>
            <?php
            }

            echo adminSys::_showPlace('activiAdr', $data[$value], $value);
        } else if ($addfileds[$value] == 'upload') {
            ?>


                                                <?php $uploadInput = 'name="' . $value . '" id="' . $value . $key . 'in"'; ?>

                                        <script>
                                            eval("imgList.<?php echo $value . $key; ?>in = new Object();");
                                        </script>

                                        <input type="hidden" <?php echo $uploadInput; ?> value="" />
                                        <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index', 'fname' => '/upload/' . $data['filetemp'], 'id' => $value . $key, 'model' => 4)) ?>" ></iframe>
                                        <div class="uploadShow" >
                                            <ul id="<?php echo $value . $key; ?>show">
            <?php
            if (!empty($data[$value])):
                $imgArr = explode(',', $data[$value]);
                foreach ($imgArr as $k => $valuea):
                    if ($value == 'infolist' || $value == 'infolistMobile'):
                        $arrExpl = explode('|', $valuea);
                        $valuea = $arrExpl[0];
                        $desArr = explode(',', $data['descriptionAll']);
                    endif;
                    ?>
                                                        <li>

                                                            <script>
                                                                eval('imgList.<?php echo $value . $key; ?>in.<?php echo $value . $key . $k; ?> = "<?php echo $valuea; ?>";');   
                                                                imgkey++;
                                                            </script>

                                                            <img src='/upload/<?php echo $data['filetemp'] . '/' . $valuea; ?>' />
                                                <?php if ($value == 'infolist' || $value == 'infolistMobile'): ?><span>标题：<input type="text" style="width:65%;" value="<?php echo $arrExpl[1]; ?>" name="<?php echo $value; ?>title[]" /></span><?php endif; ?>
                                                <?php if ($value == 'infolist' || $value == 'infolistMobile'): ?><span>描述：<textarea type="text" row="30"  name="<?php echo $value; ?>des[]"><?php echo $desArr[$k]; ?></textarea></span><?php endif; ?>
                                                            <a href="javascript:void(0);" onclick="$.listEdit.deleteImg('/upload/<?php echo $data['filetemp'] . '/' . $valuea; ?>','<?php echo $value . $key . $k; ?>','<?php echo $value . $key; ?>',this);">删除</a>
                                                        </li>
                                            <?php endforeach;
                                        endif; ?>
                                            </ul>
                                        </div>
                                        <?php
                                    }else if ($addfileds[$value] == 'radio') {
                                        switch ($value) {
                                            case 'isAble':
                                                ?>
                                                禁用<input type="radio" name="allow" value="-1" <?php if ($data[$value] == -1) echo ' checked="checked" '; ?>  />启用<input type="radio" name="allow" value="1" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?> checked="checked" />
                                                <?php
                                                break;
                                            case 'allow':
                                                ?>
                                                否<input type="radio" name="allow" value="-1" <?php if ($data[$value] == -1) echo ' checked="checked" '; ?>  />是<input type="radio" name="allow" value="1" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?> checked="checked" />
                    <?php
                    break;
                case 'type':
                    ?>
                                                线上活动<input type="radio" name="type" value="1" checked="checked" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?>  />线下活动<input type="radio" name="type" <?php if ($data[$value] == 2) echo ' checked="checked" '; ?> value="2"  />
                    <?php
                    break;
                case 'telAble':
                    ?>
                                                公开联系电话:&nbsp;&nbsp;<input type="radio" name="telAble" value="1" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?>  />不公开联系电话:&nbsp;&nbsp;<input type="radio" name="telAble" value="2" <?php if ($data[$value] == 2) echo ' checked="checked" '; ?> />
                    <?php
                    break;
                case 'mapAble':
                    ?>
                                                <script>
                                                    var mapLock = false;
                                                    function addMapPage(thp,u){
                                                        if(mapLock){
                                                    
                                                            $('#mapBox').show();
                                                            return;
                                                        }
                                                        var str = '<br /><iframe name="mapBox" id="mapBox" border="0" width="300" height="300" src="'+u+'" ></iframe>';
                                                        $(thp).append(str);
                                                        mapLock = true;
                                                    }
                                                    function getBackData(x,y){
                                                        $('#pointxmap').val(x);
                                                        $('#pointymap').val(y);
                                                    }
                                                    function clearMapBox(){
                                                        if(mapLock)$('#mapBox').hide();
                                                    }
                                                          
                                                </script>
                                                否<input type="radio" name="mapAble" checked="checked"  onclick="clearMapBox();" value="1" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?>  />是<input type="radio" onclick="addMapPage(this.parentNode,'<?php echo CHtml::normalizeUrl(array('map/index')); ?>');" name="mapAble" value="2" <?php if ($data[$value] == 2) echo ' checked="checked" '; ?> />
                                                <input type="hidden" name="PointX" id="pointxmap" /><input type="hidden" name="PointY" id="pointymap" />
                                                <?php if ($data[$value] == 2) { ?>
                                                    <br /><iframe name="mapBox" id="mapBox" border="0" width="300" height="300" src="<?php echo CHtml::normalizeUrl(array('map/index', 'x' => $data['PointX'], 'y' => $data['PointY'])); ?>" ></iframe>
                                                    <script >mapLock = true;</script>
                                                <?php } ?>
                    <?php
                    break;
                case 'class':
                    ?>
                                                商铺出租&nbsp;&nbsp;<input type="radio" name="class"  value="1" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?>   />&nbsp;&nbsp;生意转让<input type="radio" name="class" <?php if ($data[$value] == 2) echo ' checked="checked" '; ?>    value="2"  />
                                                <?php
                                                break;
                                            case 'sad':
                                                ?>
                                                出租&nbsp;&nbsp;<input type="radio" name="sad" <?php if ($data[$value] == 1) echo ' checked="checked" '; ?>   value="1"  />&nbsp;&nbsp;出售<input type="radio" name="sad" <?php if ($data[$value] == 2) echo ' checked="checked" '; ?>   value="2"  />
                                                &nbsp;&nbsp;求租<input type="radio" name="sad" <?php if ($data[$value] == 3) echo ' checked="checked" '; ?>   value="3"  />&nbsp;&nbsp;求购<input type="radio" name="sad"  <?php if ($data[$value] == 4) echo ' checked="checked" '; ?>  value="4"  />
                    <?php
                    break;
            }
        }else if ($addfileds[$value] == 'date') {
            ?>
                                        <input class="text-input small-input" type="text" id="<?php echo $value; ?>" value="<?php echo adminSys::_time($data[$value]); ?>" name="<?php echo $value; ?>" >
                                        <script type="text/javascript" >
                                            laydate({
                                                elem: document.getElementById('<?php echo $value; ?>')
                                            });    
                                        </script>
                                    <?php
                                    } elseif ($addfileds[$value] != 'imgdescription' && !empty($addfileds[$value])) {
                                        switch ($value) {
                                            case 'rent':
                                                ?>
                                                <input type="text" name="rent" class="text-input small-input" value="<?php echo $data[$value]; ?>" value="面议" />&nbsp;&nbsp;
                                                <select name="rentType" >
                                                    <option value="1" <?php echo ($data['rentType'] == 1) ? ' selected="selected" ' : ''; ?>>元/㎡/天</option>
                                                    <option value="2" <?php echo ($data['rentType'] == 2) ? ' selected="selected" ' : ''; ?>>元/㎡/月</option>
                                                    <option value="3" <?php echo ($data['rentType'] == 3) ? ' selected="selected" ' : ''; ?>>元/㎡/年</option>
                                                    <option value="4" <?php echo ($data['rentType'] == 4) ? ' selected="selected" ' : ''; ?>>其它</option>
                                                </select>
                                            <?php
                                            break;
                                        default:
                                            ?>

                                                <input type="<?php echo $addfileds[$value]; ?>" id="<?php echo $value; ?>" name="<?php echo $value; ?>" value="<?php echo $data[$value] ?>" class="text-input small-input" />
                                            <?php
                                            break;
                                    }
                                }
                                if ($addfileds[$value] == 'selectAdr') {
                                    
                                } else if ($addfileds[$value] != 'upload'):
                                    ?>
                                        </p>
        <?php else: ?>
                                    </div>
                                    <div class="clear"></div>
        <?php endif; ?>
        <?php
        if ($modelName == 'imglist')
            echo '<input type="hidden" name="spicalModel" value="imglist" />';
        if ($modelName == 'submission')
            echo '<input type="hidden" name="spicalModel" value="submission" />';

    endforeach;
endif;
?>
<?php if ($modelName == 'vote'): ?>
     <?php
    $this->beginContent('application.admin.views.layouts.editvote',array('filetemp'=>$data['filetemp'],'aid'=>$data['id']));
    echo $content;
    $this->endContent();
    ?>
<?php endif; ?>
                    </fieldset>
                    <div id="addHtmlText" style="display:none;"></div>
                    <div class="clear"></div>
                </form>
                <iframe name="ifHidden" width="100%" style="display:none;"></iframe>
            </div>
            <div class="tab-content" id="tab2" style="display: none;">

            </div>

        </div>

    </div>
</div>
<script type="text/javascript">
<?php if (!empty($arr)): ?>
        addHtmlText = eval('(<?php echo json_encode($arr); ?>)');
<?php endif; ?>
    function changeCover(name,file){
        var old = $('#'+name+'in').val();
    
        var path = '/upload/<?php echo $data['filetemp']; ?>/';
        $.ajax({
            type:'post',
            data:'path='+path+old,
            url:'<?php echo CHtml::normalizeUrl(array('channel/deleteCover')); ?>'
        });
        path = path+file;
        $('#'+name).attr('src',path);
        $('#'+name+'in').val(file);
    }
    function showBack(staus){
        if(staus == 1){
            $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">2</i>秒后返回内容管理</div></div>');
            var i = 2;
            window.setInterval(function(){
                if(i > 1){
                    i--;
                    $('#scerdShow').html(i);
                    return;
                }
                window.location.href=WEB_IN+'?r=listart/index&artTopId=<?php echo $this->isSmallType; ?>&isSmallType=<?php echo $this->isSmallType; ?>';
            },1000);                           
        }else if(staus == 0){
            $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
        }else{
            $.showMsg('<div class="notification error png_bg"><div>'+staus+'</div></div>');
        }
    }
    function uploadBack(id,file){
        eval('imgList.'+id+'in.'+id+imgkey+' = "'+file+'";');
        var value = id;
        value = value.replace(/\d+$/gi,'');
        var cLi = "<li><img src='/upload/<?php echo $data['filetemp']; ?>/"+file+"' />";
        if(value == 'infolist' || value == 'infolistMobile'){
            cLi += "<span>标题：<input type='text' style='width:65%;'  name='"+value+"title[]' /></span>"+
                "<span>描述：<textarea type='text' row='30'  name='"+value+"des[]'></textarea></span>";  
        }
        cLi += "<a href='javascript:void(0);' onclick=\"$.listEdit.deleteImg('/upload/<?php echo $data['filetemp']; ?>/"+file+"','"+id+imgkey+"','"+id+"',this);\">删除</a></li>";
        imgkey++;
        $('#'+id+'show').append(cLi);
    }
</script>
</body>
</html>
