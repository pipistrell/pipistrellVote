<script type="text/javascript" src="<?php echo ASSETS;?>js/widget.js"></script>
<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;"><?php echo $title; ?></h3>
        </div>
        <div class="content-box-content">
            <form onsubmit="return false;">
            <fieldset>
        <p>
              <label>挂件名称:</label>
                  <input class="text-input small-input" type="text" id="widgetName" value="<?php echo $data->widgetName; ?>" name="small-input" readonly="readonly">
                  <span class="input-notification success png_bg">挂件名称不可修改</span>
              
        </p>
        <p>
              <label>挂件中文名称:</label><input class="text-input small-input" type="text" id="widgetNameCn" value="<?php echo $data->widgetNameCn; ?>" name="small-input">
              
        </p>
        <p>
              <label>挂件文件名:</label>
              <input class="text-input small-input" type="text" id="widgetFile" value="<?php echo $data->widgetFile; ?>" name="small-input"><br />
              <span class="input-notification attention png_bg">如有挂件内页则依据此文件名查找，如：product.php(产品列表) productViews.php(产品详情)</span>
        </p>
        <p>
            <label>是否支持移动端：</label>
              <input type="radio" name="ismobile" value ="1" <?php if($data->ismobile):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="ismobile" value ="0" <?php if(!$data->ismobile):echo 'checked="checked" ';endif; ?>>
              否
        </p>
        <p>
            <label>是否启用：</label>
              <input type="radio" name="enable" value ="1" <?php if($data->enable):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="enable" value ="0" <?php if(!$data->enable):echo 'checked="checked" ';endif; ?>>
              否
        </p>
        <p>
            <label>挂件描述：</label>
            <textarea class="text-input textarea" id="description" name="textfield"  rows="5" style="display: block;"><?php echo $data->description; ?></textarea>
        </p>
        <p >
              <input class="button" type="submit" onclick="$.widget.widgetEdit('<?php echo CHtml::normalizeUrl(array("widget/putedit")); ?>','<?php echo $data->wid;?>');" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;
              <input class="button" type="submit" onclick="$.uploadOpen('<?php echo CHtml::normalizeUrl(array("upload/index",'fname'=>'widget/views/'.$data->widgetName)); ?>');" value="上传模板文件"> <br ><br >
              <span class="input-notification attention png_bg">上传文件必须为.php文件  *命名必须为英文</span><br >
              <span class="input-notification attention png_bg">上传时需注意文件命名如：product.php(产品列表) productViews.php(产品详情)</span>
            </p>
            </fieldset>
            </form>
        </div>
    </div>
</div>
