var lock = true;
$.extend({
   mail:{
       checkData:function(u){
           var title = $('#title').val();
           var mail = $('#mail').val();
           var advise = $('#advise').val();
           if(mail == ''){
               $.showMsg('<div class="notification information png_bg"><div>请填写邮箱</div></div>');
               return;
           }
           if(lock){
           $.post(u,{'title':title,'mail':mail,'advise':advise},function(data){
               if(data == 2){
                   $.showMsg('<div class="notification error png_bg"><div>发送失败！</div></div>');
               }else{
                   $.showMsg('<div class="notification success png_bg"><div>发送成功！感谢您的意见反馈</div></div>');
               }
               lock = true;
           });
           }else{
               $.showMsg('<div class="notification error png_bg"><div>已发送！</div></div>');
           }
       }
   } 
});