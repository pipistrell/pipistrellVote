<script type="text/javascript" src="<?php echo ASSETS;?>js/jurisdict.js"></script>
<div id="main-content">    
<div class="content-box">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">添加账户</h3>
      </div>

      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
            <form action="#" method="post" onsubmit="return false;">
            <fieldset id="w_form">
            <p>
              <label>账户名称（英文）</label>
              <input class="text-input small-input" type="text" id="w_Cuser" name="small-input" style="width: 15%;" onkeyup="this.value=this.value.replace(/[\W]/g,'')" />
              <br />
            </p>
            <p>
              <label>密码</label>
              <input class="text-input small-input" type="password" id="w_Cpass1" name="small-input" />
              <label>请重复输入密码</label>
              <input class="text-input small-input" type="password" id="w_Cpass2" name="small-input"  />
              <a onclick="checkpass()" id="w_checkemail">检测</a>
              <small id="w_is_right" style="color:#C00">通过</small>              
            </p> 
            <p>
              <label>账户名称（中文）</label>
              <input class="text-input small-input" type="text" id="w_Cuname" name="small-input" style="width: 15%;" onkeyup ="this.value=this.value.replace(/[u4E00-u9FA5]/g,'')" />
              <br />
            </p>
            <p>
              <label>电子邮箱</label>
              <input class="text-input small-input" type="text" id="w_Cemail" name="small-input" style="width: 15%;" />
              <a onclick="checkemail()" id="w_checkemail">测试</a>
              <small id="w_isemail" >通过</small>
              <br />
            </p>
            <p>  
              <input type="checkbox" name="checkbox2" id="w_Cstatus" />
              是否审核
            </p> 
            <p>
                账户权限：<select id="w_Cgroupid">
                      <option value="2">栏目管理员</option>
                      <option value="3">内容管理员</option>
                  </select>
            </p>
            <p>
                <input id="w_save" class="button" type="submit" value="提交"  onclick="CountSubmite()"/>
            </p>
            <div class="clear"></div>
            </fieldset>
            <div class="clear"></div>
          </form>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    var PASS=0;
    var EMAIL=0;
    $(document).ready(function(){ 
        $('#w_is_right').css('display','none');
        $('#w_isemail').css('display','none');
    });
    var insert_signal=0;
    function iscommon(){
        $('#w_is_right').css('display','table');
    }
    function checkpass(){
        if($('#w_Cpass1').val()!=$('#w_Cpass2').val()){
            PASS=0;
           $.showMsg('<div class="notification success png_bg"><div>'+'两次密码不一致'+'</div></div>');
           $('#w_is_right').css('display','none');           
        }else{
            PASS=1;
            $('#w_is_right').css('display','table');
        }
        
    }
    function checkemail(){
        var mail = $('#w_Cemail').val();
        if (mail != '') {//判断
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        if (reg.test(mail)) {
            EMAIL=1;
           $('#w_isemail').css('display','table');
           return true;
        }else{
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写用户名'+'</div></div>');
           $('#w_isemail').css('display','none');
           EMAIL=0;
           return false;
        }        
        }
        return false;
    }
     function CountSubmite(){   
         if(insert_signal==0){
         var groupid=$('#w_Cgroupid').val();
         var user=$('#w_Cuser').val();         
         var password=$('#w_Cpass1').val();
         var uname=$('#w_Cuname').val();
         var email=$('#w_Cemail').val();
         if($('#w_Cstatus').is(':checked')){
            var status=1;
         }else{
            var status=0;
         }
        if(user == '' || user == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写用户名'+'</div></div>');
           insert_signal=0; 
           return; } 
        if($('#w_Cpass1').val()!=$('#w_Cpass2').val()){
           $.showMsg('<div class="notification success png_bg"><div>'+'两次密码不一致'+'</div></div>');
           insert_signal=0; 
           return; } 
       if(password == '' || password == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请输入密码'+'</div></div>');
           insert_signal=0; 
           return; } 
       if(uname == '' || uname == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写中文名称'+'</div></div>');
           insert_signal=0; 
           return; } 
       if(!checkemail()){
           $.showMsg('<div class="notification success png_bg"><div>'+'email不合法，请重新填写Email'+'</div></div>');
           insert_signal=0; 
           return; }        
         var I_json= 'groupid='+groupid+'&user='+user+'&password='+password+'&uname='+uname+'&email='+email+'&status='+status;
         var url='<?php echo CHtml::normalizeUrl(array("Jurisdiction/CountInsert")); ?>';
         
           $.ajax({
           type:'post',
           url:url,
           data:I_json,
           success:function(data){
               if(data>0){ 
                   var alertmsg='成功添加一个账户，点击确定返回。';
                   alert_cfrm(alertmsg);                   
               }               
           }
           });
           insert_signal=0;
           }else{
                 $.showMsg('<div class="notification success png_bg"><div>'+'提交中请稍后'+'</div></div>');
                 return;
           }
    }
   
    
    
</script>



