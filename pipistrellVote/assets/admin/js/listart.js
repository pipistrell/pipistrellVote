$.extend({
    listart:{
        updateOrd:function(){
            var str = '';
            var li = $('#widgetContent').find('tr');
            $.each(li,function(n,i){
                if(str != '')str += ',';
                str += $(i).attr('id');
            });
            $('#strOrder').val(str);
            $('#orderingForm').submit();
        },
        deleteOne:function(id,wid){
            if(!id)return;
            $.confirm({
                'title'		: '信息提醒',
                'message'	: '您确定要删除此条记录吗？',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                            $.ajax({
                                type:'post',
                                data:'id='+id+'&wid='+wid,
                                url:WEB_IN+'?r=listart/deleteOne',
                                success:function(data){
                                    if(data == 1){
                                        $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">2</i>秒后刷新</div></div>');
                                        var i = 2;
                                        window.setInterval(function(){
                                            if(i > 1){
                                                i--;
                                                $('#scerdShow').html(i);
                                                return;
                                            }
                                            window.location.reload();
                                        },1000);                           
                                    }
                                }
                            });
                        }
                    },
                    'No'	: {
                        'class'	: 'gray',
                        'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
        },
        checkAll:function(){
            var allObj = $(':checkbox[name="checkAll"]:checked');
            if(allObj.length > 0){
                var string = '{';
                var cstr = '';
                $.each(allObj,function(i,n){
                    if(cstr!='')cstr += ',';
                    cstr += '"'+i+'":"'+$(n).val()+'"';
                });
                string += cstr+'}';
                $.post(WEB_IN+'?r=listart/checkAll',{
                    'numberArr':string
                },function(data){
                    if(data==2){
                        $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
                    }
                    $.showMsg('<div class="notification success png_bg"><div>操作成功！页面将在<i id="scerdShow">2</i>秒后刷新</div></div>');
                    var i = 2;
                    window.setInterval(function(){
                        if(i > 1){
                            i--;
                            $('#scerdShow').html(i);
                            return;
                        }
                        window.location.reload();
                    },1000);                           
                });
            }else{
                $.showMsg('<div class="notification attention png_bg"><div>您未选中任何内容！</div></div>');
            }
        },
        deleteAll:function(){
            var allObj = $(':checkbox[name="checkAll"]:checked');
            if(allObj.length > 0){
            var string = '{';
            var cstr = '';
            $.each(allObj,function(i,n){
                if(cstr!='')cstr += ',';
                cstr += '"'+i+'":"'+$(n).val()+'"';
            });
            string += cstr+'}';
            $.post(WEB_IN+'?r=listart/deleteall',{'numberArr':string},function(data){
                if(data==2){
                    $.showMsg('<div class="notification error png_bg"><div>删除失败！</div></div>');
                }
                $.showMsg('<div class="notification success png_bg"><div>操作成功！页面将在<i id="scerdShow">2</i>秒后刷新</div></div>');
                                        var i = 2;
                                        window.setInterval(function(){
                                            if(i > 1){
                                                i--;
                                                $('#scerdShow').html(i);
                                                return;
                                            }
                                            window.location.reload();
                                        },1000);                           
            });
            }else{
                $.showMsg('<div class="notification attention png_bg"><div>您未选中任何内容！</div></div>');
            }
        },
        checkOne:function(id,u){
             $.confirm({
                'title'		: '信息提醒',
                'message'	: '是否审核该条信息？',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                            $.post(u,{
                                'id':id
                            },function(data){
                                console.log(data);
                                if(data == 1){
                                    $.showMsg('<div class="notification success png_bg"><div>操作成功！页面将在<i id="scerdShow">2</i>秒后刷新</div></div>');
                                    var i = 2;
                                    window.setInterval(function(){
                                        if(i > 1){
                                            i--;
                                            $('#scerdShow').html(i);
                                            return;
                                        }
                                        window.location.reload();
                                    },1000);            
                                }else{
                                    $.showMsg('<div class="notification error png_bg"><div>'+data+'</div></div>');
                                }
                            });
                        }
                    },
                    'No'	: {
                        'class'	: 'gray',
                        'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
            
        },
        SearchInfo:function(u){
            var seChannel = $('#seChannel').val();
            var seArctype = $('#seArctype').val();
            window.location.href = u+'&channelid='+seChannel+'&arcrank='+seArctype;
        }
        
    }
});