<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class BjtIns{
    public static function Paging($id,$page,$count,$row=10){
        $all = ceil($count/$row);
        $pageBottom = $page.'/'.$all.'页 共'.$count.'条';
        $pageBottom .= '<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$id,'row'=>$row,'count'=>$count,'page'=>$page)).'">首页</a>';
        if($page > 1 && $all > 1) $pageBottom .= '<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$id,'row'=>$row,'count'=>$count,'page'=>$page-1)).'">上一页</a>';
        if($all > 1) $pageBottom .= '<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$id,'row'=>$row,'count'=>$count,'page'=>$page+1)).'">下一页</a>';
        $pageBottom .= '<a href="'.CHtml::normalizeUrl(array('ins/page','id'=>$id,'row'=>$row,'count'=>$count,'page'=>$all)).'">尾页</a>';
        $pageBottom .= '跳转至<select name="pagerMenu" onchange="location=\'/index.php?r=ins/page&page=\'+this.options[this.selectedIndex].value+\'&row='.$row.'&count='.$count.'&id='.$id.'\'" ;="">';
        for($i=1;$i<=$all;$i++){
            $select = ($page == $i)?'selected="selected"':'';
            $pageBottom .= '<option value="'.$i.'" '.$select.'>'.$i.'</option>';
        }
        $pageBottom .= '</select>页';
        return $pageBottom;
    }

 
}
?>