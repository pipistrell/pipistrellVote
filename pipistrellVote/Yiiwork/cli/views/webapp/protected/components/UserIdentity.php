<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $user_model = Member::model()->find('name=:name',array(':name'=>$this->username));
            $return = 2;
            $back = array();
            $non = true;
            if($user_model === null || !isset($user_model->mem_id)){
                $this -> errorCode = self::ERROR_USERNAME_INVALID;
                $return = 1;
                $non = false;
                $back = array('return'=>$return);
            } else if ($user_model->password !== md5($this -> password)){
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
                $return = 1;
                $back = array('return'=>$return);
            } else {
                $this->errorCode=self::ERROR_NONE;
                $return = 0;
         
                $back = array('group'=>$user_model->group_id,'jurisdition'=>$user_model->jurisdition_id,'mem_id'=>$user_model->mem_id,'return'=>$return);
            }
            if($non){
             if($user_model->ready_num == -2 && $return!=1 && $return != 2){
                $return = 100;
                $back = array('return'=>$return);
            }}
            return $back;
	}
}