<?php

class adminSys {
    public static $str = '';
    public static $allTid= 0;
    public static $lock = false;

    public static function _whereWork($arr, $Tname = '') {
        if (empty($arr))
            return '';
        $and = ' and ';
        $where = '';
        if (is_array($arr)) {
            $aid = (!empty($arr['aId'])&&!self::_checkSqlStr($arr['aId']))? $Tname . 'aid = "' . $arr['aId'] . '" ' : '';
            $channelId = (!empty($arr['channelId'])&&!self::_checkSqlStr($arr['channelId'])) ? $Tname . 'id = "' . $arr['channelId'] . '" ' : '';
            $flag = (!empty($arr['artFlag'])&&!self::_checkSqlStr($arr['artFlag'])) ? $Tname . 'flag = "' . $arr['artFlag'] . '" ' : '';
            $arcrank = (!empty($arr['arcrank'])&&!self::_checkSqlStr($arr['arcrank'])) ? $Tname . 'arcrank = "' . $arr['arcrank'] . '" ' : '';
            $artTopId = (!empty($arr['artTopId'])&&!self::_checkSqlStr($arr['artTopId'])) ? ' (' . $Tname . 'channelId = "' . $arr['artTopId'] . '" or find_in_set("' . $arr['artTopId'] . '", ' . $Tname . 'channelIdAll)) ' : '';
            $title = (empty($_GET['title'])&&!self::_checkSqlStr($arr['title'])) ? '' : ' ' . $Tname . 'title LIKE "%' . $arr['title'] . '%" ';
            $width = (empty($_GET['width'])&&!self::_checkSqlStr($arr['width'])) ? '' : ' ' . $Tname . 'width = "' . $arr['width'] . '" ';
            $height = (empty($_GET['height'])&&!self::_checkSqlStr($arr['height'])) ? '' : ' ' . $Tname . 'height = "' . $arr['height'] . '" ';
            $wid = (!empty($arr['wId'])&&!self::_checkSqlStr($arr['wid']))?$Tname.'wid = "'.$arr['wId'].'" ':'';
            $enable = (!empty($arr['enable'])&&!self::_checkSqlStr($arr['enable']))?$Tname.'enable = "'.$arr['enable'].'" ':'';
            $arctype = (!empty($arr['arctype'])&&!self::_checkSqlStr($arr['arctype'])) ? $Tname . 'arctype = "' . $arr['arctype'] . '" ' : '';
            $refid = (isset($arr['refid'])&&!self::_checkSqlStr($arr['refid']))?$Tname.'refid = "'.$arr['refid'].'" ':'';
            $uid = (isset($arr['uid'])&&!self::_checkSqlStr($arr['uid']))?$Tname.'uid = "'.$arr['uid'].'" ':'';
            $midn = (!empty($arr['midn'])&&!self::_checkSqlStr($arr['midn']))?$Tname.'mid <> 0 ':'';
            $channelid = (!empty($arr['channelid'])&&!self::_checkSqlStr($arr['channelid'])) ? $Tname . 'channelId = "' . $arr['channelid'] . '" ' : '';
            $leval = (!empty($arr['leval'])&&!self::_checkSqlStr($arr['leval']))?$Tname.'leval ="'.$arr['leval'].'" ':'';
            $aname = (!empty($arr['aname'])&&!self::_checkSqlStr($arr['aname']))?$Tname.'aname like "%'.$arr['aname'].'%" ':'';
            $jobName = (!empty($arr['jobName'])&&!self::_checkSqlStr($arr['jobName']))?$Tname.'jobName like "%'.$arr['jobName'].'%" ':'';
            
            $where = (!empty($arr['topId'])&&!self::_checkSqlStr($arr['topId'])) ? $where . $Tname . 'pid = "' . $arr['topId'] . '" ' : $where;
            $where = ($arr['topId'] == 0 && isset($arr['topId'])&&!self::_checkSqlStr($arr['topId'])) ? $where . $Tname . 'pid = "' . $arr['topId'] . '" ' : $where;
            $where = $arr['isMobile'] ? $where . $and . $Tname . 'ismobile = 1 ' : $where;
            
            
            $where = !empty($aid) && !empty($where) ? $where . $and . $aid : $where . $aid;
            $where = !empty($flag) && !empty($where) ? $where . $and . $flag : $where . $flag;
            $where = !empty($arcrank) && !empty($where) ? $where . $and . $arcrank : $where . $arcrank;
            $where = !empty($artTopId) && !empty($where) ? $where . $and . $artTopId : $where . $artTopId;
            $where = !empty($channelId) && !empty($where) ? $where . $and . $channelId : $where . $channelId;
            $where = !empty($width) && !empty($where) ? $where . $and . $width : $where . $width;
            $where = !empty($title) && !empty($where) ? $where . $and . $title : $where . $title;
            $where = !empty($height) && !empty($where) ? $where . $and . $height : $where . $height;
            $where = !empty($wid)&&!empty($where)?$where.$and.$wid:$where.$wid;
            $where = !empty($enable)&&!empty($where)?$where.$and.$enable:$where.$enable;
            $where = !empty($arctype)&&!empty($where)?$where.$and.$arctype:$where.$arctype;
            $where = !empty($refid)&&!empty($where)?$where.$and.$refid:$where.$refid;
            $where = !empty($uid)&&!empty($where)?$where.$and.$uid:$where.$uid;
            $where = !empty($midn)&&!empty($where)?$where.$and.$midn:$where.$midn;
            $where = !empty($channelid)&&!empty($where)?$where.$and.$channelid:$where.$channelid;
            $where = !empty($leval)&&!empty($where)?$where.$and.$leval:$where.$leval;
            $where = !empty($aname)&&!empty($where)?$where.$and.$aname:$where.$aname;
            $where = !empty($jobName)&&!empty($where)?$where.$and.$jobName:$where.$jobName;
            if (!empty($where))
                $where = ' where ' . $where;
            return $where;
        } else {
            return $where;
        }
    }
     public function _checkSqlStr($string) {
        $string = strtolower($string);
        return preg_match('/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|_user/i', $string);
    }
    public static function _smallChannel($id,$small,$model=0){
        if(!$model)self::$str = '';
        if(isset($small[$id]) && !empty($small[$id])){
            self::$str .= '<ul>';
            foreach($small[$id] as $value){
                $cspan = isset($small[$value['id']])? 'class="badge badge-success"':'';
                $ci = isset($small[$value['id']])? 'class="icon-minus-sign"':'class="icon-leaf"';
                $mobile = $value['ismobile']?'<a id="ismobile'.$value['id'].'"  href="#" onclick="$.channel.changeAtr(\''.$value['id'].'\',\'0\',\'ismobile\',\'禁用移动端\',\''.ASSETS.'resources/images/icons/mobileno.png\');" ><img src="'.ASSETS.'resources/images/icons/mobile.png" alt="启用移动端" title="启用移动端"></a>':'<a  id="ismobile'.$value['id'].'"  href="#" onclick="$.channel.changeAtr(\''.$value['id'].'\',\'1\',\'ismobile\',\'启用移动端\',\''.ASSETS.'resources/images/icons/mobile.png\');" ><img src="'.ASSETS.'resources/images/icons/mobileno.png" alt="禁用移动端" title="禁用移动端"></a>';
                $display = ($value['display']==1)?'<a  href="#" id="display'.$value['id'].'" onclick="$.channel.changeAtr(\''.$value['id'].'\',\'0\',\'display\',\'隐藏\',\''.ASSETS.'resources/images/icons/cross_circle.png\');" ><img src="'.ASSETS.'resources/images/icons/tick_circle.png" alt="显示" title="显示"></a>':'<a  href="#" id="display'.$value['id'].'" onclick="$.channel.changeAtr(\''.$value['id'].'\',\'1\',\'display\',\'显示\',\''.ASSETS.'resources/images/icons/tick_circle.png\');"><img src="'.ASSETS.'resources/images/icons/cross_circle.png" alt="隐藏" title="隐藏"></a>';
                self::$str .= '<li id="'.$value['id'].'">';
                self::$str .= '<p class="lineUnit" ><span '.$cspan.'><i '.$ci.'></i><b onclick="window.location.href=\''.CHtml::normalizeUrl(array('listart/index','artTopId'=>$value['id'],'isSmallType'=>$value['id'])).'\'">'.$value['typename'].'&nbsp;&nbsp;'.$value['typenameEN'].'</b></span>';
                self::$str .= '<input type="number" value="'.$value['ordering'].'" name="ordering[]" style="float:right; height:18px; margin: 0px; padding:0px; width:30px; text-align: center;" /><b style="float:right;">排序</b>';
                self::$str .= $mobile.$display;
                self::$str .= '<a  href="page'.$value['id'].'.html" target="_blank" ><img src="'.ASSETS.'resources/images/icons/webview.png" alt="预览" title="预览"></a>';
                self::$str .= '<a  href="#" onclick="$.channel.deleteOne(\''.$value['id'].'\');" ><img src="'.ASSETS.'resources/images/icons/cross.png" alt="删除" title="删除"></a>';
                self::$str .= '<a  href="'.CHtml::normalizeUrl(array('channel/edit','id'=>$value['id'])).'" ><img src="'.ASSETS.'resources/images/icons/pencil.png" alt="修改" title="修改"></a>';
                self::$str .= '<a  href="'.CHtml::normalizeUrl(array('channel/add','pid'=>$value['id'])).'" ><img src="'.ASSETS.'resources/images/icons/add.png" alt="增加子栏目" title="增加子栏目"></a></p>';
                if(isset($small[$value['id']]))self::_smallChannel ($value['id'], $small,1);
                self::$str .= '</li>';  
            }
            self::$str .= '</ul>';
        }
        return self::$str;
        
    }
    public static function _arcrankCN($arcrank){
        switch($arcrank){
            case 1:
                return '展示';
                break;
            case 0:
                return '回收站';
                break;
            case -1:
                return '未审核';
                break;
        }
    }
    public static function _levalCN($leval){
        switch($leval){
            case 1:
                return '省';
                break;
            case 2:
                return '市';
                break;
            case 3:
                return '区';
                break;
        }
    }
    public static function _cutStr($str,$num=5){
        $lenth = strlen($str);
        if($lenth > $num)
        {$bakStr = mb_substr($str,0,$num,'utf-8').'..';        
        }else{
            $bakStr = $str;
        }
        return $bakStr;
    }
    public static function _flagCN($flag){
        switch($flag){
            case 's':
                return '滚动S';
                break;
            case 'h':
                return '头条H';
                break;
            case 'f':
                return '幻灯F';
                break;
            case 'c':
                return '推荐C';
                break;
            default :
                return '无';
                break;
        }
    }
    public static function _time($time,$model=1){
        if($model == 1){
            return date('Y-m-d',$time+(8*3600));
        }else{
            return strtotime($time)-(8*3600);
        }
    }
   public static function _getAddSelect($modelName,$val,$bfModelName){
       if(empty($modelName))return '';
       eval("\$obj = Add".$bfModelName."_".$modelName."::model()->findAll();");
       $str = '<select class="small-input" name="'.$modelName.'" >';
      
       if($obj){
           foreach($obj as $value){
               $selected = ($value->id==$val)?' selected="selected" ':'';
               $str .= '<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
           }
       }
       $str .= '</select>';
       return $str;
   }
   public static function _getQuickChannel($id=0){
       return Channel::model()->findAll(' pid = :_pid ',array(':_pid'=>$id));
   }
   public static function _getAllId($topId=0){
       if($topId == 0)return $topId;
       $channel = Channel::model()->findByPk($topId);
       if(self::$lock){
       if(self::$allTid == 0)self::$allTid = '';
       if(!empty(self::$allTid))self::$allTid .= ',';
       self::$allTid .= $channel->id;
       }else{
           self::$lock = true;
       }
       if($channel->pid != 0)self::_getAllId ($channel->pid);
       return self::$allTid;
   }
   public static function _jobLeval($leval){
       switch($leval){
           case 1:
               return '行业';
               break;
           case 2:
               return '行业分类';
               break;
           case 3:
               return '职位';
               break;
       }
   }
   public static function _getProvince($id,$name,$u){
       $obj = Address::model()->findAll(' leval = :_leval ',array(':_leval'=>'1'));
       $str = '<p id="'.$id.'" class="config">'.
           '<select name="'.$name.'" onchange="$.resume.addPlace(this.value,this.parentNode,\'1\',\''.$u.'\');">'.
               '<option value="">请选择省份</option>';
       if(!empty($obj)){
           foreach($obj as $v){
               $str .= '<option value="'.$v->aid.'">'.$v->aname.'</option>';
           }
       }
       $str .= '</select>&nbsp;&nbsp;</p>';
       return $str;
   }
   public static function _showPlace($idin,$val,$en){
       if(!$val)return;
       $str = '';
       switch($en){
           case 'Province':
               $leval = '1';
               $id = '';
               $str = '<p id="'.$idin.'" class="config">';
               break;
           case 'City':
               $leval = '2';
               $id = ' id="'.$idin.'City"; ';
               break;
           case 'Area':
               $leval = '3';
               $id = ' id="'.$idin.'Area"; ';
               break;
       }
       $data = Address::model()->findAll(' leval = :_leval ',array(':_leval'=>$leval));
       
       if(!empty($data)){
           $str .= '<select '.$id.' name="'.$en.'" onchange="$.resume.addPlace(this.value,this.parentNode,\''.$leval.'\',\''.CHtml::normalizeUrl(array('request/getPlace')).'\');">';
           foreach($data as $v){
               $sel = ($val==$v->aid)?' selected="selected" ':'';
               $str .= '<option value="'.$v->aid.'" '.$sel.'>'.$v->aname.'</option>';
           }
           $str .= '</option></select>';
       }
       if($leval==3){
           $str .= '</p>';
       }
       return $str;
   }
   public static function _getIndustry($leval,$name,$val='',$sVal=''){//获取行业选项卡

       $data = JobChannel::model()->findAll('leval=:_leval',array(':_leval'=>$leval));
       $str = '<select name="'.$name.'" id="'.$name.'" onchange="$.indystry.getInfo(this.value,this.parentNode,\''.CHtml::normalizeUrl(array('request/getIndustry')).'\');" >'.
               '<option value="" >选择行业</option>';
       
       if(!empty($data)){
           foreach($data as $v){
               $select = ($v->id == $val)?' selected="selected" ':'';
               $str .= '<option value="'.$v->id.'" '.$select.'>'.$v->jobName.'</option>';
           }
       }
       $str .= '</select>';
       if(!empty($sVal)){
           $str .= '&nbsp;&nbsp;<select name="Small" >';
           $Sdata = JobChannel::model()->findAll(' pid=:_pid ',array(':_pid'=>$val));
           if(!empty($Sdata)){
               foreach($Sdata as $v){
                   $select = ($v->id == $sVal)?' selected="selected" ':'';
                   $str .= '<option value="'.$v->id.'" '.$select.'>'.$v->jobName.'</option>';
               }
           }
           $str .= '</select>';
       }
       return $str;
   }
}

?>