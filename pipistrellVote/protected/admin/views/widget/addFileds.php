<script type="text/javascript" src="<?php echo ASSETS;?>js/addFileds.js"></script>
<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">添加字段</h3>
            <input class="button" style="float:right; margin-top: 7px; margin-right: 15px;" type="submit" onclick="$.addfileds.putData('<?php echo CHtml::normalizeUrl(array('widget/addSqlFiled')); ?>');" value="保存">
            <div class="clear"></div>
        </div>

        <div class="content-box-content">
            <div class="tab-content default-tab" style="display: block;">
                <form method="post" onsubmit ="return false;">
                    <input type="hidden" id="wid" value="<?php echo $wid; ?>" />
            <fieldset>
                <p>
                    <label>字段名</label>
                    <input  class="text-input small-input" type="text" id="filedName"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[\u4e00-\u9fa5]/g,''))" onkeyup="this.value=this.value.replace(/[\u4e00-\u9fa5]/g,'')"  /><span class="input-notification attention png_bg">不能为中文！</span>
                </p>
                <p>
                    <label>提示文字</label>
                    <input  class="text-input small-input" type="text" id="filedNameCN"/>
                </p>
                <div style="display:block;overflow: hidden;">
                    <label>属性</label>
                    <div  onclick="$.addfileds.changeAttr(this,'radio');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>radio</h5><br /><small class="small">单选框<small></div>
                 </div>
                    <div  onclick="$.addfileds.changeAttr(this,'date');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>date</h5><br /><small class="small">日期选择<small></div>
                 </div>
                    <div  onclick="$.addfileds.changeAttr(this,'htmltext');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>htmltext</h5><br /><small class="small">详情录入(可编辑文本框)<small></div>
                 </div>
                    <div  onclick="$.addfileds.changeAttr(this,'upload');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>upload</h5><br /><small class="small">上传控件<small></div>
                 </div>
                 <div  onclick="$.addfileds.changeAttr(this,'number');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>number</h5><br /><small class="small">数字输入<small></div>
                 </div>
                    <div  onclick="$.addfileds.changeAttr(this,'text');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>text</h5><br /><small class="small">文本输入框<small></div>
                 </div>

                </div>
            </fieldset>
                </form>

            </div>
        </div>
    </div>
</div>
