<script type="text/javascript" src="<?php echo ASSETS;?>js/widget.js"></script>
<div id="main-content">
    <ul class="shortcut-buttons-set">
        <li><a class="shortcut-button" href="<?php echo CHtml::normalizeUrl(array('widget/add','step'=>1)); ?>"><span> <img src="<?php echo ASSETS; ?>resources/images/icons/pencil_48.png" alt="icon"><br>
                    新增挂件</span></a></li>
    </ul>
    <div class="clear"></div>
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">挂件管理</h3>
        </div>

        <div class="content-box-content">
            <div class="tab-content default-tab" style="display: block;">
                <table>
                    <thead>
                        <tr>
                            
                            <th>序号</th>
                            <th>名称</th>
                            <th>中文名称</th>
                            <th>默认文件</th>
                            <th>是否启用</th>
                            <th>是否支持移动</th>
                            <th>系统挂件</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="bulk-actions align-left">

                                    <div class="pagination"><?php if($this->totalPage > 1): ?> <a href="<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType, 'page' => 1)); ?>" title="首页">« 首页</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page - 1))); ?>" title="上一页">« 上一页</a> 
                                        <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <a href="<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType, 'page' => $i)); ?>" class="number <?php if($i==$this->page)echo 'current'; ?>" title="第<?php echo $i; ?>页"><?php echo $i; ?></a> 
                                        <?php endfor; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page + 1))); ?>" title="下一页">下一页 »</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType, 'page' => $this->totalPage)); ?>" title="尾页">尾页 »</a> 
                                        跳转至 <select onchange="window.location.href='<?php echo CHtml::normalizeUrl(array('widget/index', 'isSmallType' => $this->isSmallType)); ?>&page='+this.value;">
                                            <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <option <?php if($this->page == $i) echo 'selected="selected"'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        共有 <?php echo $this->total; ?> 条记录
                                    <?php else: echo '共有 '.$this->total.' 条记录';endif; ?></div>
                                    
                                    <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody id="widgetContent">
                        <?php if (is_array($data) && !empty($data)):
                            foreach ($data as $key => $value):
                                ?>
                                <tr <?php if ($key % 2 == 0) echo 'class="alt-row"'; ?>>
                                   
                                    <td><?php echo $value['wid']; ?></td>
                                    <td><?php echo $value['widgetName']; ?></td>
                                    <td><a href="<?php echo CHtml::normalizeUrl(array('widget/sqldetail','wid'=>$value['wid'])); ?>"><?php echo $value['widgetNameCn'];?></a></td>
                                    <td><?php echo $value['widgetFile']; ?> / <?php echo $value['widgetFile'].'View'; ?></td>
                                    <td><?php if ($value['enable']):echo '是';
                        else: echo '否';
                        endif; ?></td>
                                    <td><?php if ($value['ismobile']):echo '是';
                        else: echo '否';
                        endif; ?></td>
                                    <td><?php if ($value['isSys']):echo '是';
                        else: echo '否';
                        endif; ?></td>
                                    <td>

                                        <a href="<?php echo CHtml::normalizeUrl(array('widget/edit','wid'=>$value['wid'])); ?>" title="Edit"><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="修改"></a> 
                                        <a href="#" onclick="$.widget.deleteSome('<?php echo $value['wid']; ?>','1');" title="Delete"><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="删除"></a> 
                                    </td>
                                </tr>
    <?php endforeach;
endif; ?>
                    </tbody>
                </table>  
            </div>

        </div>
    </div>
</div>
