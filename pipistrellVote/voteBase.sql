/*
Navicat MySQL Data Transfer

Source Server         : xdjy
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : voteBase

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2015-04-29 13:40:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `pipi_ad`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_ad`;
CREATE TABLE `pipi_ad` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '1',
  `width` int(6) NOT NULL,
  `height` int(6) NOT NULL,
  `ismobile` tinyint(4) NOT NULL,
  `Stime` int(11) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_ad
-- ----------------------------
INSERT INTO `pipi_ad` VALUES ('4', '广告1', '1', '1', '0', '1430253751');

-- ----------------------------
-- Table structure for `pipi_addarticle`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_addarticle`;
CREATE TABLE `pipi_addarticle` (
  `id` int(11) NOT NULL,
  `channelId` int(11) NOT NULL,
  `body` mediumtext,
  `userip` char(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_addarticle
-- ----------------------------

-- ----------------------------
-- Table structure for `pipi_addvote`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_addvote`;
CREATE TABLE `pipi_addvote` (
  `id` int(11) NOT NULL,
  `channelId` int(11) NOT NULL,
  `startime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `countVote` int(11) DEFAULT '0',
  `isAble` tinyint(4) DEFAULT NULL,
  `userip` char(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_addvote
-- ----------------------------
INSERT INTO `pipi_addvote` VALUES ('98', '19', '1429603200', '1429776000', '1', null, '::1');
INSERT INTO `pipi_addvote` VALUES ('99', '19', '-28800', '-28800', '0', null, '::1');

-- ----------------------------
-- Table structure for `pipi_addvote_infos`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_addvote_infos`;
CREATE TABLE `pipi_addvote_infos` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `votename` char(120) NOT NULL,
  `litpic` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_addvote_infos
-- ----------------------------
INSERT INTO `pipi_addvote_infos` VALUES ('5', '98', '票1', '20150429124029_60163.png', '1');
INSERT INTO `pipi_addvote_infos` VALUES ('6', '98', '票2', '20150429124036_77986.png', '0');
INSERT INTO `pipi_addvote_infos` VALUES ('7', '98', '票3', '20150429124041_61740.png', '0');
INSERT INTO `pipi_addvote_infos` VALUES ('8', '98', '票4', '20150429124047_50141.png', '0');
INSERT INTO `pipi_addvote_infos` VALUES ('9', '99', '444', '20150429131917_97478.png', '0');
INSERT INTO `pipi_addvote_infos` VALUES ('10', '99', '555', '20150429131919_92515.png', '0');
INSERT INTO `pipi_addvote_infos` VALUES ('11', '99', '666', '20150429131920_78884.png', '0');

-- ----------------------------
-- Table structure for `pipi_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_admin_group`;
CREATE TABLE `pipi_admin_group` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) NOT NULL,
  `pid` tinyint(4) NOT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_admin_group
-- ----------------------------
INSERT INTO `pipi_admin_group` VALUES ('1', '系统管理', '0');

-- ----------------------------
-- Table structure for `pipi_ad_info`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_ad_info`;
CREATE TABLE `pipi_ad_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `imgpath` varchar(255) DEFAULT NULL,
  `ordering` tinyint(4) NOT NULL,
  `Stime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_ad_info
-- ----------------------------
INSERT INTO `pipi_ad_info` VALUES ('29', '4', '20150429124300_17932', '/', 'ad/4/20150429124300_17932.jpg', '0', '1430253784');
INSERT INTO `pipi_ad_info` VALUES ('30', '4', '20150429124302_87258', '/', 'ad/4/20150429124302_87258.jpg', '0', '1430253784');

-- ----------------------------
-- Table structure for `pipi_article`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_article`;
CREATE TABLE `pipi_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL DEFAULT '0',
  `wid` smallint(6) NOT NULL,
  `channelId` smallint(6) NOT NULL,
  `channelIdAll` varchar(90) NOT NULL,
  `arcrank` tinyint(4) NOT NULL DEFAULT '1',
  `title` char(60) NOT NULL,
  `keywords` char(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `click` mediumint(9) NOT NULL DEFAULT '0',
  `filetemp` char(24) NOT NULL,
  `Slitpic` char(100) DEFAULT NULL,
  `litpic` char(100) DEFAULT NULL,
  `flag` set('s','h','f','c') DEFAULT NULL,
  `sourceURL` varchar(60) DEFAULT NULL,
  `source` char(30) DEFAULT NULL,
  `Awrite` char(20) NOT NULL,
  `senddate` int(11) NOT NULL,
  `Aupdate` int(11) NOT NULL,
  `ismobile` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `msgCount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_article
-- ----------------------------
INSERT INTO `pipi_article` VALUES ('98', '0', '26', '19', '0', '1', '投票信息1', null, null, '0', '20150429', null, '20150429124017_24224.png', null, null, null, '', '1430253653', '0', '1', '0', '0');
INSERT INTO `pipi_article` VALUES ('99', '0', '26', '19', '0', '1', '投票2', null, null, '0', '20150429', null, '20150429131855_17264.jpg', null, null, null, '', '1430255962', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for `pipi_channel`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_channel`;
CREATE TABLE `pipi_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(50) NOT NULL,
  `typenameEN` varchar(24) DEFAULT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `wid` int(11) NOT NULL,
  `widget` varchar(14) NOT NULL,
  `temp` varchar(80) NOT NULL,
  `comtemp` varchar(80) DEFAULT NULL,
  `Cover` varchar(255) DEFAULT NULL,
  `smallCover` varchar(255) DEFAULT NULL,
  `filetemp` char(24) DEFAULT NULL,
  `display` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `ismobile` tinyint(4) NOT NULL DEFAULT '0',
  `content` text,
  `Ctemp` int(11) NOT NULL DEFAULT '0',
  `Submission` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_channel
-- ----------------------------
INSERT INTO `pipi_channel` VALUES ('19', '投票栏目', 'tplm', '', '', '26', 'vote', 'vote', 'voteView', '', '', null, '1', '0', '0', '1', '', '0', '1');
INSERT INTO `pipi_channel` VALUES ('20', '投票小类', '13', '', '', '26', 'vote', 'vote', 'voteView', '', '', null, '1', '0', '19', '1', '', '0', '1');

-- ----------------------------
-- Table structure for `pipi_sysadmin`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_sysadmin`;
CREATE TABLE `pipi_sysadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` tinyint(4) NOT NULL,
  `user` char(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `loginip` char(255) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `logintime` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `staus` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_sysadmin
-- ----------------------------
INSERT INTO `pipi_sysadmin` VALUES ('1', '1', 'admin', '081088295e334d923cee84e8e8f64997', '::1', '系统管理员', '1430256581', '', '1');

-- ----------------------------
-- Table structure for `pipi_sysconfig`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_sysconfig`;
CREATE TABLE `pipi_sysconfig` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `configName` varchar(20) NOT NULL,
  `group` tinyint(4) NOT NULL DEFAULT '0',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_sysconfig
-- ----------------------------
INSERT INTO `pipi_sysconfig` VALUES ('1', 'MEMBER', '1', '0');
INSERT INTO `pipi_sysconfig` VALUES ('2', 'CACHEABLE', '1', '0');
INSERT INTO `pipi_sysconfig` VALUES ('3', 'COPYRIGHT', '2', '<p>版权所有 版权所有 版权所有 版权所有 版权所有 版权所有</p><p>版权所有 版权所有 版权所有 版权所有 版权所有 版权所有 版权所有 版权所有 版权所有 版权所有 版权所有</p>');

-- ----------------------------
-- Table structure for `pipi_widget`
-- ----------------------------
DROP TABLE IF EXISTS `pipi_widget`;
CREATE TABLE `pipi_widget` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `widgetName` varchar(24) NOT NULL,
  `widgetFile` varchar(24) NOT NULL,
  `ismobile` tinyint(4) NOT NULL DEFAULT '1',
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `widgetNameCn` varchar(24) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `tableName` char(20) NOT NULL,
  `Attribute` varchar(200) DEFAULT NULL,
  `isSys` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wid`),
  KEY `Attribute` (`Attribute`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pipi_widget
-- ----------------------------
INSERT INTO `pipi_widget` VALUES ('2', 'listnews', 'listnews', '1', '1', '新闻栏目', '文章列表及详情', 'article', 'body', '0');
INSERT INTO `pipi_widget` VALUES ('26', 'vote', 'vote', '1', '1', '投票系统', '提供投票信息管理', 'vote', 'startime,endtime,countVote,isAble', '0');
