<?php
Class VoteWidget extends CWidget {
    public $filetemp = 'vote';
    public $id = 0;
    public $page = 1;
    public $row = 10;
    public $isMobile = 0;
    public $count = 0;
    public $orderby = ' a.ordering ';
    public $order = ' asc ';
    public $data = array();
    public function init(){//独立先执行    beginWidget();
           if(!empty($this->data['row']))$this->row = $this->data['row'];
           if(!empty($this->data['page']))$this->page = $this->data['page'];
           if(!empty($this->data['count']))$this->count = $this->data['count'];
    }
    public function run(){//独立后执行     endWidget();

        if($this->id == 0)return;
        $has = '';
        if(CACHEABLE) $has = Yii::app()->cache->get('votelist'.$this->id.$this->page);
        if($has){
            $content = $has;
        }else{
            $limit = ($this->page-1) * $this->row;
            $limit = ' limit '.$limit.','.$this->row;
            $order = ' order by '.$this->orderby.$this->order;
            $sql = 'select a.id,a.title,a.keywords,a.description,a.click,a.Slitpic,a.litpic,a.source,a.Awrite,a.senddate,a.filetemp,v.startime,v.endtime,v.countVote from {{article}} as a '.
                    'left join {{addvote}} as v on a.id=v.id '.
                    ' where a.arcrank = 1 and (a.channelId = "'.$this->id.'" or find_in_set("'.$this->id.'",a.channelIdAll)) '.$order.$limit;
            $arr = Yii::app()->db->createCommand($sql)->queryAll();
            if(!$this->count){
            $sql1 = 'select count(*) as count_number from {{article}} where arcrank = 1 and (channelId = "'.$this->id.'" or find_in_set("'.$this->id.'",channelIdAll)) ';
            $count = Yii::app()->db->createCommand($sql1)->queryAll();
            $this->count = $count[0]['count_number'];
            }
            $content = array(
                'data'=>$arr
            );
            if(CACHEABLE)Yii::app()->cache->set('votelist'.$this->id.$this->page,$content);
        }

        $this->render('vote/'.$this->filetemp,$content);
    }
}
?>