<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
define('SITE_URL','/');
define('ASSETS', SITE_URL."assets/admin/");
define('UPFILE', SITE_URL."upload/");
define('EXT_URL', SITE_URL."protected/extensions/");