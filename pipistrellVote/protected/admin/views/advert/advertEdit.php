<script type="text/javascript" src="<?php echo ASSETS;?>js/advert.js"></script>

              
<div id="main-content">    
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;"><?php echo $title; ?></h3>
            <input class="button" type="submit" onclick="self.location=document.referrer;" value="返回上一页">&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div class="content-box-content">
            <form onsubmit="return false;">
            <fieldset>
        <p>
              <label>图片名称:</label>
                  <input  class="text-input small-input" type="text" id="AD_infotitle" value="<?php echo $data->title; ?>" name="small-input" >
<!--                  <span class="input-notification success png_bg">挂件名称不可修改</span>-->
        </p> 
        <p>
              <label>图片文件地址:</label>
              <input  class="text-input small-input" type="text" id="AD_infoimgpath" value="<?php echo $data->imgpath; ?>" name="small-input">
        </p>
        <p id="w_selectimg">            
              <input class="button" type="submit" onclick="$.uploadOpen('<?php echo CHtml::normalizeUrl(array("upload/index",'fname'=>'upload/ad/'.$aid,'view'=>'index_AD')); ?>');" value="选择图片"> <br ><br >              
        </p>
        <p id="w_warning">
              <span class="input-notification attention png_bg">上传文件必须为图片类型文件文件  *命名必须为英文</span><br >
              <span class="input-notification attention png_bg">当您选择图片时，原有图片会自动删除。</span><br >
              <span class="input-notification attention png_bg">该状态下不支持批量上传图片，当您点击完成时系统只会存储您最后选择的图片。</span><br >
              <span class="input-notification attention png_bg">您所上传的图片需要以英文命名如：name1.jpg; name2.png</span>
        </p>
        <p id="w_submite">
              <input class="button" type="submit" onclick="ADinfosubmite()" value="提交">&nbsp;&nbsp;&nbsp;&nbsp;  
        </p>
            </fieldset>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var NAMEARR=[];
    var AID;    
    $(document).ready(function(){   
        NAMEARR.splice(0,NAMEARR.length);
        AID=0;
        AID="<?php echo $aid?>";
//        $('#w_warning').hide();
//        $('#w_submite').hide();
        $('#AD_infoTitle').val("<?php echo $data['title']?>");
        $('#AD_infoStime').val("<?php echo $data['Stime']?>");
        $('#AD_infoimgpath').val("<?php echo $data['imgpath']?>");
    });
  function ADinfosubmite(){      
         var title=$('#AD_infotitle').val();
         var imgpath=$('#AD_infoimgpath').val(); 
         var aid=AID;
         var alert_str='';
           var I_json='';           
           var url='';
           if("<?php echo $show_mod?>"==2){
               var id="<?php echo $id?>";
               I_json= 'title='+title+'&imgpath='+imgpath+'&id='+id;
               url='<?php echo CHtml::normalizeUrl(array("Advert/ADinfoupdate")); ?>';
               alert_str='修改成功';
           }else{  
               I_json= 'title='+title+'&imgpath='+imgpath+'&aid='+aid;
               url='<?php echo CHtml::normalizeUrl(array("Advert/ADinfoinsert")); ?>';
               alert_str='已添加一张图片';
           }                
           $.ajax({
           type:'post',
           url:url,
           data:I_json,
           success:function(data){        
               if(data>0){ 
                   cover_imgs();
                   NAMEARR.splice(0,NAMEARR.length);
                   var alertmsg=alert_str+'，确定返回广告详情页面';
                   alert_cfrm(alertmsg);
               } 
           }
       });
    }
    function setpath(newname){ 
          $('#AD_infotitle').attr("readonly",false);
          $('#AD_infoimgpath').attr("readonly",false);
          NAMEARR.push(newname);
          //console.log('setpath='+AID);
          var path='ad/'+AID+'/'+newname;
          $('#AD_infoimgpath').val(path);
          var arr=newname.split(".");
          $('#AD_infotitle').val(arr[0]);
          $('#AD_infotitle').attr("readonly",true);
          $('#AD_infoimgpath').attr("readonly",true);
    }    
    function cover_imgs(imgpath){
        if("<?php echo $show_mod?>"==1){                        
            NAMEARR.pop();            
        }else{            
            NAMEARR.pop();             
            var imgpath="<?php echo $data->imgpath; ?>";            
            var arr=imgpath.split('/');            
            NAMEARR.push(arr[arr.length-1]); 
        }
//         $.each(NAMEARR,function(i,val){
//             console.log(i+val);
//         });
        var namearr=NAMEARR.join(',');
        var aid=AID;
        //console.log('cover img='+AID);
        $.ajax({
           type:'post',
           url:'<?php echo CHtml::normalizeUrl(array("Advert/DeleteImgs")); ?>',
           data:'aid='+aid+'&namearr='+namearr,
           success:function(data){ 
               if(data>0){
                   //console.log(data+'条数据冗余');
               }
           }
       });
    }
</script>
