<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="<?php echo ASSETS;?>resources/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ASSETS;?>resources/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ASSETS;?>resources/css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ASSETS;?>css/base.css" type="text/css" />
<link rel="stylesheet" href="<?php echo ASSETS;?>confirm/jquery.confirm.css" type="text/css" />
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/simpla.jquery.configuration.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/facebox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/jquery.datePicker.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/jquery.date.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>js/system.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>confirm/jquery.confirm.js"></script>

    </head>
    <body >
        <?php $this->beginContent('//layouts/left');
            echo $content;
            $this->endContent(); ?>
     <?php echo $content; ?>
    </body>
</html>