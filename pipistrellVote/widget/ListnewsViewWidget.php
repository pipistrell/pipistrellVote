<?php
Class listnewsViewWidget extends CWidget {
    public $filetemp = 'listnewsView';
    public $id = 0;//这里指栏目ID
    public $data = array();
    public function init(){//独立先执行    beginWidget();
        $has = '';
        if(CACHEABLE)$has = Yii::app()->cache->get('listnewsView'.$this->id.$this->data['id']);
            if(!$has){
            $sql = 'select a.id,a.title,a.keywords,a.description,a.click,a.Slitpic,a.litpic,a.flag,a.source,a.sourceURL,a.Awrite,a.senddate,a.Aupdate,d.body ' .
                    ' from {{article}} as a left join {{addarticle}} as d on a.id = d.id  ' .
                    'where a.arcrank = 1 and a.id = "'.$this->data['id'].'" ';
            $has = Yii::app()->db->createCommand($sql)->queryRow();
            }
            $this->data = array(
                    'id'=>$has['id'],
                    'body' => $has['body'],
                    'senddate' => $has['senddate'],
                    'update' => $has['Aupdate'],
                    'author' => $has['Awrite'],
                    'source' => $has['source'],
                    'flag' => $has['flag'],
                    'litpic' => $has['litpic'],
                    'Slitpic' => $has['Slitpic'],
                    'click' => $has['click'],
                    'description' => $has['description'],
                    'keywords' => $has['keywords'],
                    'title' => $has['title'],
                    'sourceURL'=>$has['sourceURL']
            );
           
            if(CACHEABLE)Yii::app()->cache->set('listnewsView'.$this->id.$this->data['id'],$this->data);
    }
    public function run(){//独立后执行     endWidget();
        if($this->id == 0 || $this->data['id'] == 0)return;
        $has = array();
        if(CACHEABLE)$has = Yii::app()->cache->get('listnewsViewpaging'.$this->id.$this->data['id']);
        if(!$has){
        $after = Article::model()->find(' (channelId = :_channelId or find_in_set(:_channelId,channelIdAll)) and arcrank = :_arcrank and id > :_id  ',array(
            ':_channelId'=>$this->id,
            ':_arcrank'=>1,
            ':_id'=>$this->data['id']
        ));
        $before = Article::model()->find(' (channelId = :_channelId or find_in_set(:_channelId,channelIdAll)) and arcrank = :_arcrank and id < :_id ',array(
            ':_channelId'=>$this->id,
            ':_arcrank'=>1,
            ':_id'=>$this->data['id']
        ));

        if(!empty($after))$has['after'] = array('title'=>$after->title,'id'=>$after->id,'channelId'=>$after->channelId);
        if(!empty($before))$has['before'] = array('title'=>$before->title,'id'=>$before->id,'channelId'=>$before->channelId);
        if(CACHEABLE)Yii::app()->cache->set('listnewsViewpaging'.$this->id.$this->data['id'],$has);
        }
        $this->data['paging'] = $has;
        $this->render('listnews/'.$this->filetemp,$this->data);
    }
}
?>