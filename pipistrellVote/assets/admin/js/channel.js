
$.extend({
    channel:{
        updateOrd:function(){
            var str = '';
            var li = $('#webrootUl').find('li');
            $.each(li,function(n,i){
                if(str != '')str += ',';
                str += $(i).attr('id');
            });
            $('#strOrder').val(str);
            $('#orderingForm').submit();
        },
        changeAtr:function(pkid,val,atr,msg,img){
            if(pkid == '' || val == '' || atr == ''){
                $.showMsg('<div class="notification attention png_bg"><div>参数错误！</div></div>');
                return;
            }

            $.ajax({
                type:'post',
                data:'pkid='+pkid+'&val='+val+'&atr='+atr,
                url:WEB_IN+'?r=channel/ChangeAtr',
                success:function(data){
                    if(data == 1){
                       var pimg = $('#'+atr+pkid).find('img').attr('src');
                       $('#'+atr+pkid).attr('onclick',"$.channel.changeAtr('"+pkid+"','"+Math.abs(val-1)+"','"+atr+"','"+msg+"','"+pimg+"');").html("<img src='"+img+"' alt='"+msg+"' title='"+msg+"'>");
                    }
                }
            });
        },
        deleteOne:function(id){
            if(!id)return;
            $.confirm({
                'title'		: '信息提醒',
                'message'	: '您确定要删除此条记录吗？',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                           $.post(WEB_IN+'?r=channel/deleteOne',{pkid:id},function(data){
                if(data == 1){
                    $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">3</i>秒后返回栏目管理</div></div>');
                    var i = 3;
                    window.setInterval(function(){
                        if(i > 1){
                            i--;
                            $('#scerdShow').html(i);
                            return;
                        }
                        window.location.reload();
                    },1000); 
                }else{
                    $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
                }
            });
                    }
                   
                }, 'No'	: {
                        'class'	: 'gray',
                        'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
                    }
            }
            });
            
        }
    }
});