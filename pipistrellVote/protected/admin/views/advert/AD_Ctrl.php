<script type="text/javascript" src="<?php echo ASSETS;?>js/advert.js"></script>
<div id="main-content">
<div class="content-box">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">广告管理</h3>
        <a class="shortcut-button" href="<?php echo CHtml::normalizeUrl(array('Advert/ADinfo','mod'=>1)); ?>" style="float: left;" onclick="ADinsert()" >
            <span style="padding:3px;"> 
                <img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="icon" style="margin-bottom: 0px;">
                新增广告
            </span></a>
        <div class="clear"></div>
      </div>
      <!-- End .content-box-header -->
      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
        <table>
            <thead>
              <tr>                
                <th>编号</th>
                <th>广告名称</th>
                <th>尺寸</th>
                <th>是否支持移动</th>
                <th>创建时间</th>
                <th>操作</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td colspan="6">
                  <div class="bulk-actions align-left">
                    <div class="pagination"><?php if($this->totalPage > 1): ?> <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => 1)); ?>" title="首页">« 首页</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page - 1))); ?>" title="上一页">« 上一页</a> 
                                        <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => $i)); ?>" class="number <?php if($i==$this->page)echo 'current'; ?>" title="第<?php echo $i; ?>页"><?php echo $i; ?></a> 
                                        <?php endfor; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page + 1))); ?>" title="下一页">下一页 »</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => $this->totalPage)); ?>" title="尾页">尾页 »</a> 
                                        跳转至 <select onchange="window.location.href='<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType)); ?>&page='+this.value;">
                                            <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <option <?php if($this->page == $i) echo 'selected="selected"'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        共有 <?php echo $this->total; ?> 条记录
                                    <?php else: echo '共有 '.$this->total.' 条记录';endif; ?></div>
                                    
                                    <div class="clear"></div>
                </td>
              </tr>
            </tfoot>
            <tbody>
                <?php foreach($AD_arr as $value): ?>                       
                    <tr>
<!--                      <td><input type="checkbox" /></td>-->
                      <td><?php echo $value['aid']; ?></td>
                      <td><a href="<?php echo CHtml::normalizeUrl(array('Advert/ADinfo','aId'=>$value['aid'],'ADname'=>$value['title'],'ADwidth'=>$value['width'],'ADheight'=>$value['height'],'ADismobile'=>$value['ismobile'],'ADStime'=>$value['Stime'],'mod'=>2)); ?>" title="title"><?php echo $value['title']?></a></td>
                      <td><?php echo $value['width']; ?>X<?php echo $value['height']; ?>(px)</td>
                      <td><?php echo $value['ismobile']; ?></td>
                      <td><?php echo $value['Stime']; ?></td>
                      <td>              
                        <a href="<?php echo CHtml::normalizeUrl(array('Advert/ADinfo','aId'=>$value['aid'],'ADname'=>$value['title'],'ADwidth'=>$value['width'],'ADheight'=>$value['height'],'ADismobile'=>$value['ismobile'],'ADStime'=>$value['Stime'],'mod'=>2)); ?>" title="Edit"><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="Edit" /></a>
                        <a href="#" title="Delete" onclick="ADdeleteajax('<?php echo $value['aid']?>','<?php echo $value['title']?>')"><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="Delete" /></a>
<!--                        <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>-->
                      </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>  
        </div>
<!--          end table 2-->
      </div>
    </div>
</div>
<script type="text/javascript"> 
    $(document).ready(function(){
    });
   function ADdeleteajax(aid,ADname){
       var url='<?php echo CHtml::normalizeUrl(array('Advert/ADdelete')); ?>';
       var D_json='aid='+aid+'&ADname='+ADname;
       var alertmsg='删除一条广告';
       delete_cfrm(url,D_json,alertmsg);
   }

</script>