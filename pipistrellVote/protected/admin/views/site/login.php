<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="<?php echo ASSETS; ?>resources/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ASSETS; ?>resources/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ASSETS; ?>resources/css/invalid.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo ASSETS; ?>resources/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>resources/scripts/simpla.jquery.configuration.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>resources/scripts/facebox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>resources/scripts/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/system.js"></script>

    </head>
    <body id="login">
<div id="login-wrapper" class="png_bg">
  <div id="login-top">
    <!-- Logo (221px width) -->
    <a href="#"><img id="logo" src="<?php echo ASSETS; ?>resources/images/logo.png" alt="Simpla Admin logo" /></a> </div>
  <!-- End #logn-top -->
  <div id="login-content">
      <div class="notification information png_bg">
        <div id="login_msg"><?php echo $msg; ?></div>
      </div>
      <p>
        <label>用户名</label>
        <input class="text-input" type="text" id="user_id" />
      </p>
      <div class="clear"></div>
      <p>
        <label >密码</label>
        <input class="text-input" type="password" id="user_psw" />
      </p>
      <div class="clear"></div>
      <p>
          <p>
              <label>验证码</label><img src="<?php echo CHtml::normalizeUrl(array("Sother/checkimage")); ?>" style="float:right;" onclick="javascript:this.src='<?php echo CHtml::normalizeUrl(array("Sother/checkimage")); ?>';"/><input class="text-input" type="text" id="user_yz" style="width:135px; margin-right:15px;" />
          </p>
        <input class="button" type="submit" value="登陆" onclick="$.loginCheck('<?php echo CHtml::normalizeUrl(array("request/Checklogin")); ?>','<?php echo CHtml::normalizeUrl(array('site/index')); ?>');" />
      </p>
  </div>
</div>
        

</body>
</html>