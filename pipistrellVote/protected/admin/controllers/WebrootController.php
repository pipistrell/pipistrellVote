<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class WebrootController extends CController {
public $isType = 0;
   public $isSmallType = 0;
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter'
            )
        );
    }

    public function actionIndex() {
        $this->layout='application.admin.views.layouts.main';
        $this->isType = 1;
        $this->isSmallType = $_GET['isSmallType'];
        $this->render('webrootconfig');
    }

}

?>