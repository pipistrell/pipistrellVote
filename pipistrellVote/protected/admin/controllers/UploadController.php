<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class UploadController extends CController {

    public $view = 'index';
    public $model = 0;
    public $domId = '';
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter - Uploadfile'
            )
        );
    }

    public function actionIndex() {
        $this->model = isset($_GET['model']) && !empty($_GET['model']) ? $_GET['model'] : 0;
        $this->view = isset($_GET['view']) && !empty($_GET['view']) ? $_GET['view'] : 'index';
        $this->domId = isset($_GET['id']) && !empty($_GET['id'])?$_GET['id']:'';
        $this->renderPartial($this->view, array(
            'upPath' => CHtml::normalizeUrl(array('upload/uploadfile')),
            'fname' => $_GET['fname']
        ));
    }

    public function actionUploadfile() {
        
        if (!empty($_POST)) {
            $file_n = '';
            if (isset($_POST['fileName'])) {
                $file_n = $_POST['fileName'];
            } else if (isset($_GET['fileName'])) {
                $file_n = $_GET['fileName'];
            }
            $POST_MAX_SIZE = ini_get('post_max_size');
            $unit = strtoupper(substr($POST_MAX_SIZE, -1));
            $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

            if ((int) $_SERVER['CONTENT_LENGTH'] > $multiplier * (int) $POST_MAX_SIZE && $POST_MAX_SIZE) {
                header("HTTP/1.1 500 Internal Server Error");
                echo "POST exceeded maximum allowed size.";
                exit(0);
            }

            $save_path = dirname(Yii::app()->getBasePath()) . "/" . $file_n . "/";
            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            $upload_name = "Filedata";
            $max_file_size_in_bytes = 2147483647;    // 2GB 
            $extension_whitelist = array("php", "gif", "jpg", "png");
            $valid_chars_regex = '.A-Z0-9_ !@#$%^&()+={}\[\]\',~`-';

            $MAX_FILENAME_LENGTH = 260;
            $file_name = "";
            $file_extension = "";
            $uploadErrors = array(
                0 => "0",
                1 => "1",
                2 => "2",
                3 => "3",
                4 => "4",
                6 => "5"
            );
          
            if (!isset($_FILES[$upload_name])) {
                $this->HandleError("No upload found in \$_FILES for " . $upload_name);
                exit(0);
            } else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0) {
                $this->HandleError($uploadErrors[$_FILES[$upload_name]["error"]]);
                exit(0);
            } else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])) {
                $this->HandleError("Upload failed is_uploaded_file test.");
                exit(0);
            } else if (!isset($_FILES[$upload_name]['name'])) {
                $this->HandleError("File has no name.");
                exit(0);
            }

            $file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
            if (!$file_size || $file_size > $max_file_size_in_bytes) {
                $this->HandleError("File exceeds the maximum allowed size");
                exit(0);
            }

            if ($file_size <= 0) {
                $this->HandleError("File size outside allowed lower bound");
                exit(0);
            }

            $file_name = preg_replace('/[^' . $valid_chars_regex . ']|\.+$/i', "", basename($_FILES[$upload_name]['name']));
            if (strlen($file_name) == 0 || strlen($file_name) > $MAX_FILENAME_LENGTH) {
                $this->HandleError("Invalid file name");
                exit(0);
            }
            
            $path_info = pathinfo($_FILES[$upload_name]['name']);
            $file_extension = $path_info["extension"];
            $is_valid_extension = false;
            foreach ($extension_whitelist as $extension) {
                if (strcasecmp($file_extension, $extension) == 0) {
                    $is_valid_extension = true;
                    break;
                }
            }

            if (!$is_valid_extension) {
                $this->HandleError("1444");
                exit(0);
            }

            $file_name = date("YmdHis").'_'.rand(10000, 99999).'.'.$path_info["extension"];
            if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $save_path . $file_name)) {
                $this->HandleError("文件无法保存");
                exit(0);
            }

            echo $file_name;
            return;
        }
    }

 

}

?>