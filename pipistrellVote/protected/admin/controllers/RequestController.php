<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class RequestController extends CController {

    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter - Checklogin'
            )
        );
    }

    public function actionChecklogin() {
        if (IS_POST) {
            if (strcasecmp(Yii::app()->session['yztpcheck'],$_POST['yzm'])) {
                echo '验证码错误!';
                return;
            }
            $identity = new UserIdentity($_POST['user'], $_POST['psw']);

            $choose = $identity->authenticate();
            switch ($choose['return']) {
                case '':
                    echo '用户名或密码错误！';
                    break;
                case 100:
                    echo '该账户暂未通过审核！';
                    break;
                case 1:
                    echo '帐户名错误！';
                    break;
                case 2:
                    echo '密码错误！';
                    break;
            }
            if ($choose['return'] !== 9)
                return;
            Yii::app()->user->setReturnUrl(SITE_URL);
            Yii::app()->user->login($identity, 0, $choose);
            Sysadmin::model()->updateByPk($choose['id'], array('loginip' => $_SERVER['REMOTE_ADDR'], 'logintime' => time() - (8 * 3600)));
            echo 9;
        }
    }

    public function actionSysConfigWrite() {
        if (IS_POST) {

            $str = '<?php ';
            $str .= 'define("WEBNAME","' . (empty($_POST['webname']) ? '' : $_POST['webname']) . '");';
            $str .= 'define("WEBURL","' . (empty($_POST['weburl']) ? '' : $_POST['weburl']) . '");';
            $str .= 'define("SITE_URL","' . (empty($_POST['siteurl']) ? '' : $_POST['siteurl']) . '");';
            $str .= 'define("ASSETS",SITE_URL."assets/");';
            $str .= 'define("UPLOADTEMP","' . (empty($_POST['uploadTemp']) ? 'upload/' : $_POST['uploadTemp']) . '");';
            $str .= 'define("UPFILE",SITE_URL.UPLOADTEMP);';
            $str .= 'define("MEMBER","' . $_POST['member'] . '");';
            $str .= 'define("BANMSG","' . $_POST['banmsg'] . '");';
            $str .= 'define("MSGABLE","' . $_POST['msg'] . '");';
            $str .= 'define("CACHEABLE","' . $_POST['cache'] . '");';
            $str .= 'define("COPYRIGHT","' . $_POST['copyright'] . '");';
            $str .= 'define("MOBICOPYRIGHT","' . $_POST['mobicopyright'] . '");';

            file_put_contents(dirname(Yii::app()->BasePath) . '/protected/config/sysconfig.php', $str);
            echo 1;
        }
    }

    public function actionDeleteOne() {
        if (!IS_POST)$this->redirect(array('site/error', 'msg' => '数据无效！'));
        switch ($_POST['modelId']) {
            case 1:
                $bak = $this->_widgetDelete();
                break;
        }
        echo $bak;
    }

    private function _widgetDelete() {
        if (!isset($_POST['id']) || empty($_POST['id']))
            $this->redirect(array('site/error', 'msg' => '数据无效！'));
        $widget = Widget::model()->findByPk($_POST['id']);
        $cfile = dirname(Yii::app()->getBasePath()).'/widget/'.$widget->widgetName.'Widget.php';
        $cvfile = dirname(Yii::app()->getBasePath()).'/widget/'.$widget->widgetName.'ViewWidget.php';
        $vfile = dirname(Yii::app()->getBasePath()).'/widget/views/'.$widget->widgetName.'/';
        if(is_file($cfile))unlink($cfile);
        if(is_file($cvfile))unlink($cvfile);
        if(is_dir($vfile)) $this->deldir ($vfile);
        Widget::model()->deleteByPk($_POST['id']);
        return 1;
    }

    private function deldir($dir) {//删除文件夹
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != "." && $file != "..") {
                $fullpath = $dir . "/" . $file;
                if (!is_dir($fullpath)) {
                    unlink($fullpath);
                } else {
                    $this->deldir($fullpath);
                }
            }
        }
        closedir($dh);
        if (rmdir($dir)) {
            return true;
        } else {
            return false;
        }
    }
    public function actionDeleteCahce(){
        $fileAll = dirname(Yii::app()->getBasePath()).'/protected/runtime/cache';
        $this->deldir($fileAll);

        echo 1;
    }
    public function actionGetPlace(){
        if(empty($_POST['id']))return;
        $data = Address::model()->findAll(' pid=:_pid ',array(':_pid'=>$_POST['id']));
        $bakArr = array();
        if(is_array($data)){
            foreach($data as $v){
                $bakArr[] = array(
                    'id'=>$v->aid,
                    'name'=>$v->aname
                );
            }
        }
        echo json_encode($bakArr);
    }
    public function actionGetIndustry(){
        if(empty($_POST['id']))return;
        $data = JobChannel::model()->findAll(' pid=:_pid ',array(':_pid'=>$_POST['id']));
        $bakArr = array();
        if(!empty($data)){
            foreach($data as $v){
                $bakArr[] = array(
                    'id'=>$v->id,
                    'name'=>$v->jobName
                );
            }
        }
        echo json_encode($bakArr);
    }
}

?>