<script type="text/javascript" src="<?php echo ASSETS;?>js/jurisdict.js"></script>
<div id="main-content">    
<div class="content-box">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">修改账户</h3>
      </div>

      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
            <form action="#" method="post" onsubmit="return false;">
            <fieldset id="w_form">
            <p>
              <label>账户名称（英文）</label>
              <input class="text-input small-input" type="text" id="w_Cuser" name="small-input" style="width: 15%;" onkeyup="this.value=this.value.replace(/[\W]/g,'')" />
              <br />
            </p>
              <label>账户名称（中文）</label>
              <input class="text-input small-input" type="text" id="w_Cuname" name="small-input" style="width: 15%;" onkeyup ="this.value=this.value.replace(/[u4E00-u9FA5]/g,'')"  />
              <br />
            </p>
            <p>
              <label>密码</label>
              <input class="text-input small-input" type="text" id="w_Cpassword" name="small-input" style="width: 15%;"  />
              <br />
            </p>
            <p>
              <label>电子邮箱</label>
              <input class="text-input small-input" type="text" id="w_Cemail" name="small-input" style="width: 15%;"  />
              <br />
            </p>
            <p>  
              <input type="checkbox" name="checkbox2" id="w_Cstatus" />
              是否审核
            </p> 
            <p>
                账户权限：<select id="w_Cgroupid">
                      <option value="1">系统管理员</option>
                      <option value="2">栏目管理员</option>
                      <option value="3">内容管理员</option>
                      
                  </select>
            </p>
            <p>
                <input id="w_save" class="button" type="submit" value="保存"  onclick="CountSubmite()"/>
            </p>
            <div class="clear"></div>
            </fieldset>
            <div class="clear"></div>
          </form>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">  
    $(document).ready(function(){ 
        "<?php echo $email?>"
        $('#w_Cgroupid').val("<?php echo $groupid?>");
        $('#w_Cuser').val("<?php echo $user?>");         
        $('#w_Cpassword').val();
        $('#w_Cuname').val("<?php echo $uname?>");
        $('#w_Cemail').val("<?php echo $email?>");
        if("<?php echo $status?>"==1){
           $('#w_Cstatus').attr("checked",true);    
        }else{
            $('#w_Cstatus').attr("checked",false);
        }
        
    });
    function checkemail(){
        var mail = $('#w_Cemail').val();
        if (mail != '') {//判断
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        if (reg.test(mail)) {
            EMAIL=1;
           $('#w_isemail').css('display','table');
           return true;
        }else{
           $.showMsg('<div class="notification success png_bg"><div>'+'email不合法，请重新填写Email'+'</div></div>');
           $('#w_isemail').css('display','none');
           EMAIL=0;
           return false;
        }        
        }
        return false;
    }
    var submite_signal=0;
     function CountSubmite(){   
         if(submite_signal==0){
             var id="<?php echo $id?>";
         var groupid=$('#w_Cgroupid').val();
         var user=$('#w_Cuser').val();     
         var uname=$('#w_Cuname').val();
         var password=$('#w_Cpassword').val();
         var email=$('#w_Cemail').val();
         if($('#w_Cstatus').is(':checked')){
            var status=1;
         }else{
            var status=0;
         }
        if(user == '' || user == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写用户名'+'</div></div>');
           submite_signal=0; 
           return; } 
       if(uname == '' || uname == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写中文名称'+'</div></div>');
           submite_signal=0; 
           return; } 
       if(!checkemail()){
           submite_signal=0; 
           return; }        
         var I_json= 'id='+id+'&groupid='+groupid+'&user='+user+'&uname='+uname+'&password='+password+'&email='+email+'&status='+status;
         var url='<?php echo CHtml::normalizeUrl(array("Jurisdiction/CountUpdate")); ?>';
           $.ajax({
           type:'post',
           url:url,
           data:I_json,
           success:function(data){
               if(data>0){ 
                   var alertmsg='修改成功。';
                   alert_cfrm(alertmsg);                   
               }               
           }
           });
           submite_signal=0;
           }else{
                 $.showMsg('<div class="notification success png_bg"><div>'+'提交中请稍后'+'</div></div>');
                 return;
           }
    }
   
</script>
