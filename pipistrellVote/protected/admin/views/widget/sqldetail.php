<script type="text/javascript" src="<?php echo ASSETS;?>js/sqldetail.js"></script>
<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">字段管理</h3>
        </div>

        <div class="content-box-content">
            <div class="tab-content default-tab" style="display: block;">
                <form target="hideIf" id="deleteF" method="post" action="<?php echo CHtml::normalizeUrl(array('widget/deletField')); ?>">
            <fieldset>
            <p >
              <label>挂件名称</label>
              <?php echo $widgetData->widgetName; ?>&nbsp;/&nbsp;<?php echo $widgetData->widgetNameCn; ?></p>
            <p>
                <label>附加字段</label>
                <input class="text-input small-input" type="text" value="<?php echo $widgetData->Attribute; ?>"  readonly="readonly" />
            </p>
           
                <input type="hidden" name="wid" value="<?php echo $widgetData->wid; ?>" />
            <p>
                <?php $arr = explode(',',$widgetData->Attribute);
                       foreach($arr as $v):
                ?>
                <input type="checkbox" value="<?php echo $v; ?>" checked="checked" name="sqlcheck[]" /><?php echo $v; ?>
                <?php endforeach; ?>
                <br>
              <span class="input-notification attention png_bg">未勾选字段将被删除！</span>
            </p>
           
            <p><a class="button" onclick="$('#deleteF').submit();" >删除字段</a>
            <a class="button" href="<?php echo CHtml::normalizeUrl(array('widget/addFildes','wid'=>$widgetData->wid)); ?>" >添加字段</a></p>
            <p>
                <label>字段类型</label>
                <textarea name="addfileds" readonly="readonly" rows=2 >
                  <?php foreach($addfileds as $key => $value):
                      echo '"'.$key.'"=>"'.$value.'",'; 
                    endforeach; ?>
                </textarea>
            </p>
            <p>
                <label>字段名称</label>
                <textarea name="addCN"  rows=2 >
                  <?php foreach($addCN as $key => $value):
                      echo '"'.$key.'"=>"'.$value.'",'; 
                    endforeach; ?>
                </textarea>
            </p>
            </fieldset>
                </form>
                 <iframe style="display:none;" name="hideIf"></iframe>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function backMsg(b){
    if(b){
        $.confirm({
                'title'		: '信息提醒',
                'message'	: '操作成功！',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                         window.location.reload();
                    }
                   
                }
            }
            });
    }else{
        $.showMsg('<div class="notification error png_bg"><div>操作失败！</div></div>');
    }
}
</script>