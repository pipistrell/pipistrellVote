$.extend({
    loginCheck:function(u,open){
        var userId = $('#user_id').val();
        var userPsw = $('#user_psw').val();
        var yz = $('#user_yz').val();
        if(userId == ''){
            $('#login_msg').html('用户名不能为空！');
            return;
        }
        if(userPsw == ''){
            $('#login_msg').html('密码不能为空！');
            return;
        }
        if(yz == ''){
            $('#login_msg').html('验证码不能为空！');
            return;
        }
        $.ajax({
            type:'POST',
            data:'user='+userId+'&psw='+userPsw+'&yzm='+yz,
            url:u,
            success:function(data){
                if(data == '9'){
                    window.location.href= open;
                    return;
                }
                $('#login_msg').html(data);
            }
        });
    },
    sysWrite:function(u){
        var webname = $('#webname').val();
        var weburl = $('#weburl').val();
        var siteurl = $('#siteurl').val();
        var uploadTemp = $('#uploadTemp').val();
        var copyright = $('#copyright').val();
        var mobicopyright = $('#mobicopyright').val();
        var member = $(":radio[name='member']:checked").val();
        var cache = $(":radio[name='cache']:checked").val();
        var msg = $(":radio[name='msg']:checked").val();
        var banmsg = $('#banmsg').val();
        if(member == '' || member == undefined)member = 0;
        if(cache == '' || cache == undefined)cache = 0;
        if(msg == '' || msg == undefined)msg = 0;
        $.ajax({
            type:'post',
            data:'webname='+webname+'&weburl='+weburl+'&siteurl='+siteurl+'&uploadTemp='+uploadTemp+'&copyright='+copyright+'&mobicopyright='+mobicopyright+'&member='+member+'&cache='+cache+'&msg='+msg+'&banmsg='+banmsg,
            url:u,
            success:function(data){
                if(data==1){
                    $('#attentionMsg').html('<div>修改成功！</div>').fadeIn(200);
                    setTimeout(function(){
$('#attentionMsg').fadeOut(200);
},3000);
                }
            }
        });
    },
    uploadOpen:function(u){
        $.facebox('<iframe src="'+u+'" width="100%" height="100%" frameborder="no" border="0"></iframe>');
    },
    showMsg:function(msg){
        $.facebox('<br >'+msg);
    },
    showLoading:function(msg){
        $.facebox('<img src="/assets/admin/resources/images/loading.gif"/>'+msg);
    },
    deleteCache:function(){
        $.confirm({
                'title'		: '信息提醒',
                'message'	: '您确定要清除所有缓存吗？',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                         $.post(WEB_IN+'?r=request/DeleteCahce',function(data){
            $.showMsg('<div class="notification success png_bg"><div>操作成功！</div></div>');
                                         });
                }
                    }, 'No'	: {
                        'class'	: 'gray',
                        'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
                    }
            }

            });
        
    }

});