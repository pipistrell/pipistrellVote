<script type="text/javascript" src="<?php echo ASSETS;?>js/channeledit.js"></script>

<div id="main-content">
    <div class="content-box">
        <div class="content-box-header" >
            <h3 style="cursor: s-resize; float: left;">栏目修改</h3>
           <input class="button" style="float:right; margin-top: 7px; margin-right: 15px;" type="submit" onclick="$.channeledit.putDataDb();" value="保存">
        </div>

        <div class="clear"></div>
        <div class="content-box-content">
            <div class="tab-content default-tab" id="tab1" >
                <form onsubmit="return false;">
                    <fieldset>
                    <input type="hidden" id="channelId" value="<?php echo $data->id; ?>" />
                    
                    <div id="widgetBox" style="display:block;overflow: hidden;">
                  <label>选择挂件</label>
                  <?php if(!empty($widget)): 
                      $widgetArr = array();
                      foreach($widget as $key=>$value):
                          $widgetArr[$key] = array(
                              'widgetFile'=>$value['widgetFile'],
                              'widgetName'=>$value['widgetName']
                          );
                          ?>
              <div id="<?php echo $key; ?>" onclick="$.channeledit.changeWidget(this,'<?php echo $value->wid; ?>');" class="notification <?php if($data->wid == $value->wid):echo 'success';else:echo 'information';endif; ?> png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
      <div><h5><?php echo $value->widgetNameCn; ?></h5><br /><a href="#" title="<?php echo $value->description; ?>"><small class="small"><?php echo $value->description; ?></small></a></div>
    </div>
              <?php endforeach;endif; ?>
                  <div id="0" onclick="$.channeledit.changeWidget(this,'0');" abs="advise" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>意见反馈</h5><br /><small class="small">提供访问者提交留言意见<small></div>
                 </div>
                  <div id="0" onclick="$.channeledit.changeWidget(this,'0');" abs="release" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
                  <div><h5>简历列表</h5><br /><small class="small">提供访问者查看简历<small></div>
                 </div>
              <input type="hidden" id="wid" value="<?php echo $data->wid; ?>" />
              </div>
              <div class="clear"></div>
              <p>
                  <label>栏目排序：</label>
                  <input class="text-input small-input"  id="ordering" type="number" value="<?php echo $data->ordering; ?>" />
                  <span class="input-notification information png_bg">默认正序排列</span>
              </p>
                <p>
              <label>栏目标题</label>
              <input class="text-input small-input" type="text" id="typename" value="<?php echo $data->typename; ?>" name="small-input">
              </p>
              <p>
              <label>英文标题</label>
              <input class="text-input small-input" type="text" id="typenameEN" value="<?php echo $data->typenameEN; ?>" name="small-input">
              </p>
              <p>
              <label>关键词</label>
              <input class="text-input small-input" type="text" id="keywords" value="<?php echo $data->keywords; ?>" name="small-input">
              <span class="input-notification information png_bg">文件头部关键词</span>
              </p>
              <p>
              <label>描述</label>
              <input class="text-input small-input" type="text" id="description" value="<?php echo $data->description; ?>" name="small-input">
              <span class="input-notification information png_bg">文件头部描述</span>
              </p>
              <p>
              开启移动端浏览：
              <input type="radio" name="ismobile" value ="1" <?php if($data->ismobile == 1):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="ismobile" value ="0" <?php if($data->ismobile == 0):echo 'checked="checked" ';endif; ?>>
              否</p>
              <p>
                  栏目是否启用：
                  <input type="radio" name="display" value ="1" <?php if($data->display == 1):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="display" value ="0" <?php if($data->display == 0):echo 'checked="checked" ';endif; ?>>
              否
              </p>
              <p>
                  是否支持会员信息发布：
                  <input type="radio" name="submission" value ="1" <?php if($data->Submission == 1):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="submission" value ="-1" <?php if($data->Submission == -1):echo 'checked="checked" ';endif; ?>>
              否
              </p>
              <p>
                  上级栏目：<select id="topId">
                      <option value="0">顶级栏目</option>
                      <?php foreach($topData as $value):
                          if($data->id != $value->id): ?>
                      <option value="<?php echo $value->id; ?>" <?php if($value->id == $data->pid)echo 'selected="selected"'; ?>><?php if($value->pid != 0):echo '&nbsp;&nbsp;&nbsp;';endif; echo '&nbsp;'.$value->typename; ?></option>
                      <?php endif;endforeach; ?>
                  </select>
              </p>
              <p>
                  内页模板：<select id="commonTemp">
                     <?php if(!empty($commonArr)):
                         foreach($commonArr as $key=>$value):
                         if($value != '.' && $value != '..'): 
                            preg_match('/\d+/',$value,$arr);
                            if(empty($arr[0]))$arr[0] = 0;
?>
                      <option value="<?php echo $arr[0]; ?>" <?php if($arr[0] == $data->Ctemp)echo 'selected = "selected" '; ?>><?php echo $value; ?></option>
                      <?php $i++;endif;endforeach;endif; ?>
                  </select>
                  <span class="input-notification attention png_bg">关联布局文件，如：common1.php 关联 main1.php</span>
              </p>
              <input class="text-input small-input"  id="tempName" type="hidden" value="<?php echo $data->widget; ?>" />
              <p>
                  <label>挂件首页：</label>
                  <input class="text-input small-input"  id="temp" type="text" value="<?php echo $data->temp; ?>" />
                  <span class="input-notification attention png_bg">挂件默认首页文件</span>
              </p>
              <p>
                  <label>挂件内页：</label>
                  <input class="text-input small-input"  id="comtemp" type="text" value="<?php echo empty($data->comtemp)?'无':$data->comtemp; ?>" />
                  <span class="input-notification attention png_bg">挂件默认内页文件</span>
              </p>
              <div class="content-box column-left">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">栏目封面</h3>
      </div>
      <div class="content-box-content">
        <div class="tab-content default-tab" style="display: block;">
          <h4>栏目封面</h4>
          <p>
                 
                  <input type="hidden" id="coverin" name="cover" value="<?php echo $data->Cover; ?>" />
                  <img id="cover" class="coverImg" src="<?php echo empty($data->Cover)?ASSETS.'base/NOIMG.jpg':'/upload/channel/'.$data->filetemp.'/'.$data->Cover; ?>" />
                  <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index','fname'=>'/upload/channel/'.$data->filetemp,'model'=>2)) ?>" ></iframe>
                  
              </p>
        </div>
      </div>
    </div>
              <div class="content-box column-right">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">栏目封面（移动版）</h3>
      </div>
      <div class="content-box-content">
        <div class="tab-content default-tab" style="display: block;">
          <h4>栏目封面（移动版）</h4>
          <p>
                  <input type="hidden" id="smallcoverin" name="smallcover" value="<?php echo $data->smallCover; ?>" />
                  <img id="smallcover" class="coverImg" src="<?php echo empty($data->smallCover)?ASSETS.'base/NOIMG.jpg':'/upload/channel/'.$data->filetemp.'/'.$data->smallCover; ?>" />
                  <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index','fname'=>'/upload/channel/'.$data->filetemp,'model'=>3)) ?>" ></iframe>
              </p>
        </div>
      </div>
    </div>
              <div class="clear"></div>
              
             <p>
                 <label>栏目介绍</label>
                 <div id="article_content"><?php echo $data->content ?></div>
           <?php $this->widget('ext.KEditor.KEditor',array(
			'name'=>'content132', //设置name
                        'id'=>'article_content',
			'textareaOptions'=>array(
				'style'=>'width:98%;height:400px;',
			),
                       
		)); ?>


             </p>
              </fieldset>
                    </form>
        </div>
            
            
        </div>

    </div>
    
   
    
</div>
<script>
    var widget = eval('('+'<?php echo json_encode($widgetArr); ?>'+')');
function changeCover(name,file){
    var old = $('#'+name+'in').val();
    
    var path = '/upload/channel/<?php echo $data->filetemp; ?>/';
    $.ajax({
        type:'post',
        data:'path='+path+old,
        url:'<?php echo CHtml::normalizeUrl(array('channel/deleteCover')); ?>'
    });
    path = path+file;
    $('#'+name).attr('src',path);
    $('#'+name+'in').val(file);
}

</script>