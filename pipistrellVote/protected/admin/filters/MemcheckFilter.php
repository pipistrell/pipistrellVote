<?php
class MemcheckFilter extends CFilter
{
    protected function preFilter($filterChain)
    {
        if (Yii::app()->user->isGuest)
        {
            Yii::app()->runController('site/login');  
        } else {
            $filterChain->run();
        }
    }
 

}

?>