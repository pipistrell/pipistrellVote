<script type="text/javascript" src="<?php echo ASSETS;?>js/advert.js"></script>

<script type="text/javascript" src="<?php echo ASSETS;?>js/widget_add.js"></script>
<?php $this->beginContent('//layouts/Advert_step',array('step'=>$step));
            echo $content;
            $this->endContent(); ?>

<div id="main-content">    
<div class="content-box">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">图片管理</h3>
            <a id="w_lt_AD" class="shortcut-button"  href="<?php echo CHtml::normalizeUrl(array('Advert/index')); ?>" style="float: left;" >
            <span style="padding:3px;"> 
                <img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="icon" style="margin-bottom: 0px;">
                返回广告列表页
            </span></a>
        <ul class="content-box-tabs">
          <li><a href="#tab1" class="default-tab">广告详情</a></li>
          
          <li><a href="#tab2">广告列表</a></li>
        </ul>
        <div class="clear"></div>
      </div>

      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
            <form action="#" method="post" onsubmit="return false;">
            <fieldset id="w_form">
            <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
            <div id="w_step1" style="width:100%; height: auto;  overflow: hidden;">
            <p>
              <label>焦点图名称</label>
              <input class="text-input small-input" type="text" id="ADtitle" name="small-input" style="width: 15%;" value="<?php echo $ADname?>" />

              <br />

            </p>
            <p>
              <label>焦点图尺寸（像素）</label>
              <input class="text-input small-input" type="text" id="ADwidth" name="small-input" style="width:30px"/>X<input class="text-input small-input" type="text" id="ADheight" name="small-input" style="width:30px;"/>（px）

              <br /> </p>            
            <p>              
              <input type="checkbox" name="checkbox2" id="ADismobile" />
              应用于手机网站
            </p>           
            <p>
                <input id="w_save" class="button" type="submit" value="下一步"  onclick="ADsubmite()"/>
            </p>
            <div class="clear"></div>
            </div>

            <div id="w_step2" style="width:100%; height: auto; overflow: hidden;">
            <ul class="shortcut-buttons-set" style="overflow:hidden; width:45%; margin-right: 0px;"> 
                <li><a class="shortcut-button" href="#" ><span><img src="" alt="icon"><br>插入单张图片</span></a></li>
            </ul>
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">上传图片列表</h3>
        </div>
        <div class="content-box-content">
<!--            缩略图样式表-->
            <style>
                #w_imglist{
                    width:100%;
                    height:auto;
                    overflow:hidden;
                    list-style-type: none;
                }
                #w_imglist li{
                    list-style-type: none;                    
                    width:160px;
                    height:160px;
                    overflow:hidden;
                    float:left;
                    margin-left:3px;
                    margin-top:3px;
                    padding: 0px;
                }
                #w_imglist img{
                    width:100%;
                    height:105px;
                }
            </style>
<!--            样式表结束-->
            <div class="tab-content default-tab"  style="display: block; overflow: hidden;">
                <ul id="w_imglist" style="overflow:hidden;"> 
                     
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div> 
            <div class="w_step2_btns">
               <input class="button" type="submit" value="返回上一步"  onclick="prestep()"/>
               <input class="button" type="submit" value="完成"  onclick="submitebyqueue()"/>
                <div class="clear"></div>
            </div>
            </div>
<!--       w_step2 end!!-->
            </fieldset>
            <div class="clear"></div>
            <!-- End .clear -->
          </form>
        </div>
        <div class="tab-content" id="tab2"  style="display: block;">
                    <table>
            <thead>
              <tr>                
                <th>编号</th>
                <th>图片名称</th>
                <th><input class="button" type="submit" value="保存排序" onclick="saveorder()"/></th>
                <th>上传时间</th>
                <th>操作</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td colspan="6">
                  <div class="bulk-actions align-left">
                   
                                    <div class="pagination"><?php if($this->totalPage1 > 1): ?> <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => 1)); ?>" title="首页">« 首页</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page1 - 1))); ?>" title="上一页">« 上一页</a> 
                                        <?php for ($i = 1; $i <= $this->totalPage1; $i++): ?>
                                            <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => $i)); ?>" class="number <?php if($i==$this->page1)echo 'current'; ?>" title="第<?php echo $i; ?>页"><?php echo $i; ?></a> 
                                        <?php endfor; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page + 1))); ?>" title="下一页">下一页 »</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType, 'page' => $this->totalPage)); ?>" title="尾页">尾页 »</a> 
                                        跳转至 <select onchange="window.location.href='<?php echo CHtml::normalizeUrl(array('advert/index', 'isSmallType' => $this->isSmallType)); ?>&page='+this.value;">
                                            <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <option <?php if($this->page1 == $i) echo 'selected="selected"'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        共有 <?php echo $this->total; ?> 条记录
                                    <?php else: echo '共有 '.$this->total1.' 条记录';endif; ?></div>
                                    
                                    <div class="clear"></div>
                                    
                  </div>                
                </td>
              </tr>
            </tfoot>
            <tbody id="w_ADinfoGride">
               <?php if(!empty($AD_info_arr)){?>
                <?php foreach($AD_info_arr as $value): ?>                       
                    <tr>
<!--                      <td><input type="checkbox" /></td>-->
                        <td id="ADinfoID"><?php echo $value['id']; ?></td>
                      <td><a href="<?php echo CHtml::normalizeUrl(array('Advert/AdvertEdit','id'=>$value['id'],'ADname'=>$ADname,'aid'=>$aid,'mod'=>'2')); ?>" title="title"><?php echo $value['title']?></a></td>
                      <td id="ADinfoOR"><input type="text" value="<?php echo $value['ordering']; ?>" name="ordering" style="width:30px"/></td>                      
                      <td><?php echo $value['Stime']; ?></td>
                      <td>              
                        <a href="<?php echo CHtml::normalizeUrl(array('Advert/AdvertEdit','id'=>$value['id'],'ADname'=>$ADname,'aid'=>$aid,'mod'=>'2')); ?>" title="Edit"><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="Edit" /></a>
                        <a href="#" title="Delete" onclick="ADinfodeleteajax('<?php echo $value['id']?>','<?php echo $value['imgpath']?>')"><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="Delete"  /></a>
<!--                        <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>-->
                      </td>
                    </tr>
                <?php endforeach; }?>
            </tbody>
          </table> 
        </div>
<!--          end table 2-->
      </div>
    </div>
</div>
<script type="text/javascript">  
    var AID;
    $(document).ready(function(){ 
        AID=0;  
        if("<?php echo $show_mod?>"==1){
            $('#w_save').val('下一步'); 
            $('#w_IOBO').hide();
            $('.stepInfo').show();
            $('#w_brg1').css('background','#45a0f3');
            $('#w_brg2').css('background','#45a0f3');
            $('#w_s1').css('background','#45a0f3');
            $('#w_s2').css('background','#45a0f3');
            $('#w_s3').css('background','#45a0f3');
        }else{
            $('#w_save').val('保存设置');
            $('#w_IOBO').show();
            $('.stepInfo').hide();
            $('#ADname').val("<?php echo $ADname?>");
            $('#ADwidth').val("<?php echo $ADwidth?>");
            $('#ADheight').val("<?php echo $ADheight?>");            
            AID="<?php echo $aid?>";
            var getstr="<?php echo CHtml::normalizeUrl(array('Advert/AdvertEdit','ADname'=>$ADname,'mod'=>'1')); ?>&aid="+AID;
            $('#w_IOBO').attr("href",getstr);
            if("<?php echo $ADismobile?>"==1){
                $("#ADismobile").attr("checked",true);
            } else{
                $("#ADismobile").attr("checked",false);
            }
        }        
        $('#w_step2').hide();
    });
    function prestep(){
         $('#w_step2').hide();
         $('#w_step1').show();
         //$('#w_s2').css('background','#25A006');
         $('#w_brg1').css('background','#45a0f3');
    }
    function nextstep(){
         $('#w_step1').hide();
         $('#w_step2').show();
    }
    function fillhtml(filename){         
        var imgsrc="<?php echo $src='upload/ad/';?>"+AID+'/'+filename;  
        //console.log(imgsrc);
        var aid=AID;        
        var fn_arr=filename.split(".");        
        var simghtm='<li style=\"list-style-type:none;\" id="'+fn_arr[0]+'">'+
//            '<input class="text-input small-input" type="text" id="ADwidth" name="small-input" style="width:30px"/>'+
            '<img id="smallcover" class="coverImg" src="'+imgsrc+'" />'+
            '<a id="w_del" onclick="delete_simg(\''+aid+'\',\''+fn_arr[0]+'\',\''+filename+'\')">删除</a>'+
            '<input id="w_ADinfoname" type="text" value=\"'+fn_arr[0]+'\"/>'+
            '</li>';
        $('#w_imglist').append(simghtm);
        
        $('#w_s2').css('background','#25A006');
        $('#w_brg1').css('background','#25A006');        
        return;
    }
    
    function submitebyqueue(){
        var arr=$('#w_imglist').children().map(function() {                                       
                    var i=$(this).children()[0].src;                    
                    var a=i.split('/');             
                    var p=$(this).children('#w_ADinfoname').val();
                    var val=a[a.length-1];
                    if(p!=""){
                        //var varr=val.split('.');
                        val=val+'-'+p;
                    }                    
                    return val;
         }).get();
         arr=$.map(arr, function(index,value) { 
             //console.log(index+'='+value);
              value=value+'='+index;
              return value;
          });          
         //console.log(AID);
          var aid=AID;
          var str=arr.join('&');
          var iq_json='aid='+aid+'&'+str;
          var alert_str='';
          $.ajax({
           type:'post',
           url:'<?php echo CHtml::normalizeUrl(array("Advert/InsertbyQueue")); ?>',
           data:iq_json,
           success:function(data){
               if(data>0){
                   alert_str='共有'+data+'张图片上传';
                   $('#w_s3').css('background','#25A006');
                   $('#w_brg2').css('background','#25A006'); 
                    $.showMsg('<div class="notification success png_bg"><div>操作成功！'+alert_str+'系统将在<i id="scerdShow">3</i>秒后返回挂件管理</div></div>');
            var i = 3;
            window.setInterval(function(){
                if(i > 1){
                    i--;
                    $('#scerdShow').html(i);return;
                }
                window.location.href = '<?php echo CHtml::normalizeUrl(array('Advert/index')); ?>';
            },1000);           
            return;
        }else{
                   alert_str='您没有选择任何图片，不过您可以补充图片';                 
               }
               $.showMsg('<div class="notification success png_bg"><div>'+alert_str+'</div></div>');
           }
       });
    }
    
    function saveorder(){
//         design by ZhaoBo!!!!
//         var arr = $(':input[name="ordering"]');
//         $.each(arr,function(i,n){
//             console.log($(n).val());
//         });
        var arr=$('#w_ADinfoGride').children().map(function() { 
                var id=$(this).children('#ADinfoID').text();
                var ord=$(this).children('#ADinfoOR').children().val();                 
                var val=id+'='+ord;                   
                //console.log(val);
                return val;
         }).get();
          var res='保存成功';
          var U_json=arr.join('&');
          $.ajax({
              type:'post',
              url:'<?php echo CHtml::normalizeUrl(array("Advert/ADinfoSaveOrd")) ?>',
              data:U_json,
              success:function(data){
                  if(data>0){
                      location.reload();
                      //$.showMsg('<div class="notification success png_bg"><div>'+res+'</div></div>');                      
                  }                  
              }                    
          });
          
    }
    function delete_simg(aid,fname,fname_ext){ 
        $.ajax({
           type:'post',
           url:'<?php echo CHtml::normalizeUrl(array("Advert/DeleteImg")); ?>',
           data:'aid='+aid+'&oldname='+fname_ext,
           success:function(data){ 
               if(data>0){
                  $('#'+fname).remove();
                  alert('删除');
               }
           }
       });        
    }
    
    function msg(){
         $.each(imgs, function(index,value) { 
             console.log(index+value);             
         });
    }    
    function ADinfodeleteajax(id,imgpath){
       //----------------------
        var url='<?php echo CHtml::normalizeUrl(array('Advert/ADinfodelete')); ?>';
        var D_json='id='+id+'&imgpath='+imgpath;
        var alertmsg='您确认删除该条记录吗？';
        delete_cfrm(url,D_json,alertmsg);
        return;
       //----------------------

    }
     var insert_signal=0;
    function ADsubmite(){
        if(insert_signal==0){
           insert_signal=1;
         var title=$('#ADtitle').val();
         var width=$('#ADwidth').val();
         var height=$('#ADheight').val(); 
         var aid=AID;
         //console.log(AID);       
         var ismobile=0;
         var res='';
         var time='';
         if($('#ADismobile').is(':checked')){
             ismobile=1;
         }else{
             ismobile=0;
         }
         if(title == '' || title == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写广告标题'+'</div></div>');
           insert_signal=0; 
           return; } 
          if(width == '' || width == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写广告的宽度'+'</div></div>');
           insert_signal=0; 
           return; }
          if(height == '' || height == undefined){
           $.showMsg('<div class="notification success png_bg"><div>'+'请填写广告的高度'+'</div></div>');
           insert_signal=0; 
           return; }
           var url='';
           if("<?php echo $show_mod?>"==2){
               url='<?php echo CHtml::normalizeUrl(array("Advert/ADupdate")); ?>';
               res='修改成功！' ;
           }else{
               time = (new Date()).valueOf();
               url='<?php echo CHtml::normalizeUrl(array("Advert/ADinsert")); ?>';
               res='添加成功';
           }   
           var I_json='title='+title+'&width='+width+'&height='+height+'&ismobile='+ismobile+'&aid='+aid+'&time='+time;
           $.ajax({
           type:'post',
           url:url,
           data:I_json,
           success:function(data){ 
               if(data>0){               
                    AID=data;                       
                    $('#w_step1').hide();
                    $('#w_step2').show();                   
                   load_upload(AID); 
                   $.showMsg('<div class="notification success png_bg"><div>'+res+'</div></div>');
                   if("<?php echo $show_mod?>"==1){
                          var getstr="<?php echo CHtml::normalizeUrl(array('Advert/AdvertEdit','ADname'=>$ADname,'mod'=>'1')); ?>&aid="+AID;
                          $('#w_IOBO').attr("href",getstr);
                   }
                   $('#w_IOBO').show();
                   //$('#w_s1').css('backgroundcolor','#25A006');
               } 
               else if(data==-1){
                   $.showMsg('<div class="notification success png_bg"><div>'+res+'</div></div>');
                   $('#w_step1').hide();
                   $('#w_step2').show();                   
               }
               $('#w_s1').css('background','#25A006');
           }
       });   insert_signal=0;
       }else{
             $.showMsg('<div class="notification success png_bg"><div>'+'提交中请稍后'+'</div></div>');
             return;
       }
    }
    function load_upload(aid){        
        var param='<?php echo CHtml::normalizeUrl(array("upload/index",'view'=>'index_ADimgQueue')); ?>&fname=upload/ad/'+aid;
        var src_p='<?php echo ASSETS; ?>'+'resources/images/icons/pencil_48.png';
        var src_d='<?php echo ASSETS; ?>'+'resources/images/icons/delete.png';
        
        var htm='<li><a class="shortcut-button" href="#" onclick="$.uploadOpen(\''+param+'\');"><span><img src="'+src_p+'" alt="icon"><br>批量上传图片</span></a></li>'+
            '<li><a id="w_IOBO" class="shortcut-button" href="<?php echo CHtml::normalizeUrl(array('Advert/AdvertEdit','aid'=>$aid,'ADname'=>$ADname,'mod'=>'1')); ?>";"><span><img src="'+src_p+'" alt="icon"><br>插入单张图片</span></a></li>';
                             
        $('.shortcut-buttons-set').html(htm);
        //var btnhtm='<p><input class="button" type="submit" value="保存图片"  onclick="submitebyqueue()"/> </p>';
        //$('#w_form').append(btnhtm);
    }
    
    
</script>



