<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
Class InsController extends Controller {
    public $typename = '';
    public $typenameEN = '';
    public $keywords = '';
    public $description = '';
    public function init(){
       
    }
    public function actionPage() {
        if (empty($_GET['id'])) {
            $this->redirect(array('bjt/index'));
            return;
        }
        $has = '';
        $paging = array(
            'page' => $_GET['page'],
            'row' => $_GET['row'],
            'count' => $_GET['count']
        );
        if (CACHEABLE)
            $has = Yii::app()->cache->get('InsCommon' . $_GET['id']);
        if ($has) {
            $content = $has;
        } else {
             $channel = Channel::model()->findByPk($_GET['id']);
            
                $widget = 'webroot.widget.' . $channel->widget . 'Widget';
           
            $content = array(
                'id' => $channel->id,
                'widget' => $widget,
                'fileTemp' => $channel->temp,
                'typename' => $channel->typename,
                'typenameEN' => $channel->typenameEN,
                'pid' => $channel->pid,
                'data' => $paging,
                 'Ctemp'=>$channel->Ctemp,
                'keywords'=>$channel->keywords,
                'description'=>$channel->description
            );
            if (CACHEABLE)
                Yii::app()->cache->set('InsCommon' . $_GET['id'], $contet);
        }
        $this->keywords = $content['keywords'];
        $this->description = $content['description'];
        $this->typename = $content['typename'];
        if(empty($content['Ctemp'])){
            $this->layout = 'main';
            $this->render('common', $content);
        }else{
            $this->layout = 'main'.$content['Ctemp'];
            $this->render('common'.$content['Ctemp'], $content);
        }
    }

    public function actionInfo() {
        if (empty($_GET['id']) || empty($_GET['pid'])) {
            $this->redirect(array('bjt/index'));
            return;
        }
        $has = '';
        if (CACHEABLE)
            $has = Yii::app()->cache->get('InsCommonInfo' . $_GET['pid'] . $_GET['id']);
        if ($has) {
            $content = $has;
        } else {
            
            $channel = Channel::model()->findByPk($_GET['pid']);
            
            $widget = 'webroot.widget.' . $channel->widget . 'viewWidget';
            $content = array(
                'id' => $channel->id,
                'widget' => $widget,
                'fileTemp' => $channel->comtemp,
                'typename' => $channel->typename,
                'typenameEN' => $channel->typenameEN,
                'pid' => $channel->pid,
                'data' => array(
                    'id' => $_GET['id'],
                ),
                'Ctemp'=>$channel->Ctemp,
                'keywords'=>$channel->keywords,
                'description'=>$channel->description
            );
            if (CACHEABLE)
                Yii::app()->cache->set('InsCommonInfo' . $_GET['pid'] . $_GET['id'], $contet);
        }
        $this->keywords = $content['keywords'];
        $this->description = $content['description'];
        $this->typename = $content['typename'];
       if(empty($content['Ctemp'])){
            $this->layout = 'main';
            $this->render('common', $content);
        }else{
            $this->layout = 'main'.$content['Ctemp'];
            $this->render('common'.$content['Ctemp'], $content);
        }
    }

}

?>