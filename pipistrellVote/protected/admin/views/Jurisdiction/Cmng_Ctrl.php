<script type="text/javascript" src="<?php echo ASSETS;?>js/jurisdict.js"></script>
<div id="main-content">
    <ul class="shortcut-buttons-set">
        <li><a class="shortcut-button" href="<?php echo CHtml::normalizeUrl(array('jurisdiction/CaddRender')); ?>"><span> <img src="<?php echo ASSETS; ?>resources/images/icons/memadd.png" alt="icon"><br>
                    新增账户</span></a></li>
    </ul>
    <div class="clear"></div>
<div class="content-box">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">账户管理</h3>
        <div class="clear"></div>
      </div>
      <!-- End .content-box-header -->
      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
        <table>
            <thead>
              <tr>                
                <th>账户编号</th>
                <th>账户名称</th>
                <th>账户中文名</th>
                <th>电子邮箱</th>
                <th>审核状态</th>
                <th>操作</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td colspan="6">
                  <div class="bulk-actions align-left">
                    <div class="pagination"><?php if($this->totalPage > 1): ?> <a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType, 'page' => 1)); ?>" title="首页">« 首页</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page - 1))); ?>" title="上一页">« 上一页</a> 
                                        <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType, 'page' => $i)); ?>" class="number <?php if($i==$this->page)echo 'current'; ?>" title="第<?php echo $i; ?>页"><?php echo $i; ?></a> 
                                        <?php endfor; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page + 1))); ?>" title="下一页">下一页 »</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType, 'page' => $this->totalPage)); ?>" title="尾页">尾页 »</a> 
                                        跳转至 <select onchange="window.location.href='<?php echo CHtml::normalizeUrl(array('Jurisdiction/index', 'isSmallType' => $this->isSmallType)); ?>&page='+this.value;">
                                            <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <option <?php if($this->page == $i) echo 'selected="selected"'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        共有 <?php echo $this->total; ?> 条记录
                                    <?php else: echo '共有 '.$this->total.' 条记录';endif; ?></div>
                                    
                                    <div class="clear"></div>
                </td>
              </tr>
            </tfoot>
            <tbody>
                <?php foreach($Count_arr as $value): ?>    
                    <tr>
                      <td id="w_id"><?php echo $value['id']; ?></td>
                      <td><a href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/CountRender',
                          'id'=>$value['id'],
                          'groupid'=>$value['groupid'],
                          'user'=>$value['user'],
                          'uname'=>$value['uname'],
                          'email'=>$value['email'],
                          'status'=>$value['staus'],
                          'logintime'=>$value['logintime'],
                          'password'=>$value['password'])); ?>"  title="title"><?php echo $value['user']?></a></td>
                      <td><?php echo $value['uname']; ?></td>
                      <td><?php echo $value['email']; ?></td>
                      <td><?php echo $value['staus']; ?></td>
                      <td>              
                        <a id="w_edit" href="<?php echo CHtml::normalizeUrl(array('Jurisdiction/CountRender',
                          'id'=>$value['id'],
                          'groupid'=>$value['groupid'],
                          'user'=>$value['user'],
                          'uname'=>$value['uname'],
                          'email'=>$value['email'],
                          'status'=>$value['staus'],
                          'logintime'=>$value['logintime'],
                          'password'=>$value['password'])); ?>"  title="Edit"><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="Edit" /></a>
                          <a href="#" onclick="Countdeleteajax('<?php echo $value['id']?>')" title="Delete" onclick=""><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="Delete" /></a>
                      </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>  
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"> 
    $(document).ready(function(){
    });
   function ADdeleteajax(aid,ADname){
       var url='<?php echo CHtml::normalizeUrl(array('Advert/ADdelete')); ?>';
       var D_json='aid='+aid+'&ADname='+ADname;
       var alertmsg='删除该账户';
       delete_cfrm(url,D_json,alertmsg);
   }      
  function Countdeleteajax(id){
       //----------------------
        var url='<?php echo CHtml::normalizeUrl(array('Jurisdiction/CountDelete'));?>';
        var D_json='id='+id;
        var alertmsg='您确认删除该账户吗？';
        delete_cfrm(url,D_json,alertmsg);
        return;
       //----------------------

    }
   
</script>