<div id="main-content">
    <div class="notification success png_bg" style="display:none;" id="attentionMsg">
      <div>  </div>
    </div>
    <div class="notification attention png_bg" style="display:none;" id="attentionMsga">
      <div>  </div>
    </div>
<div class="content-box">

      <div class="content-box-header">
        <h3 style="cursor: s-resize;">站点设置</h3>
        <ul class="content-box-tabs">
          <li><a href="#tab1" class="default-tab current">基本设置</a></li>

          <li><a href="#tab2" class="">高级设置</a></li>
        </ul>
        <div class="clear"></div>
      </div>

      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block;">
          <form onsubmit="return false;" action="#" method="post">
            <fieldset>
            <p class="config">
              <label>网站标题</label>
              <input value="<?php echo WEBNAME; ?>" class="text-input small-input" type="text" id="webname" name="small-input"></p>
            <p class="config">
              <label>网站网址</label>
              <input value="<?php echo WEBURL; ?>" class="text-input small-input" type="text" id="weburl" name="small-input"></p>
            <p class="config">
              <label>根目录</label>
              <input value="<?php echo SITE_URL; ?>" class="text-input small-input" type="text" id="siteurl" name="small-input"></p>
            <p class="config">
              <label>上传目录</label>
              <input value="<?php echo UPLOADTEMP; ?>" class="text-input small-input" type="text" id="uploadTemp" name="small-input"></p>
            <p class="config">
              <label>PC版权声明</label>
              <input value="<?php echo COPYRIGHT; ?>" class="text-input large-input" type="text" id="copyright" name="small-input">
              <br>
              <small>可输入标签</small> </p>
            
            <p class="config">
              <label>移动版权声明</label>
              <input value="<?php echo MOBICOPYRIGHT; ?>" class="text-input large-input" type="text" id="mobicopyright" name="small-input">
              <br>
              <small>可输入标签</small> </p>
<!--            <p class="configP" >
              开启会员功能：
              <input type="radio" name="member" value ="1" <?php if(MEMBER):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="member" value ="0" <?php if(!MEMBER):echo 'checked="checked" ';endif; ?>>
              否</p>
            <p class="configP">
              开启缓存功能：
              <input type="radio" name="cache" value ="1" <?php if(CACHEABLE):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="cache" value ="0" <?php if(!CACHEABLE):echo 'checked="checked" ';endif; ?>>
              否</p>
            <p class="configP">
              开启文章评论功能：
              <input type="radio" name="msg" value ="1" <?php if(MSGABLE):echo 'checked="checked" ';endif; ?>>
              是
              <input type="radio" name="msg" value ="0" <?php if(!MSGABLE):echo 'checked="checked" ';endif; ?>>
              否</p>
            <p class="configP">
               <label>敏感词屏蔽：</label>
              <input value="<?php echo BANMSG; ?>" class="text-input large-input" type="text" id="banmsg" name="small-input">
              <br>
              <small>多个词语请用 | 隔开（键盘：shift + \）</small>
            </p>-->
            </fieldset>
            <div class="clear"></div>
          </form>
        </div>
        <div class="tab-content" id="tab2" style="display: none;">
          
        </div>
          
      </div>
      <p class="config">
              <input class="button" type="submit" onclick="$.sysWrite('<?php echo CHtml::normalizeUrl(array("request/sysConfigWrite")); ?>');" value="修改">
            </p>
    </div>
</div>
    </body>
</html>