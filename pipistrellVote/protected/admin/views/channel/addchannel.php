<script type="text/javascript" src="<?php echo ASSETS;?>js/channeledit.js"></script>

<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">栏目修改</h3>
           
        </div>

        <div class="clear"></div>
        <div class="content-box-content">
            <div class="tab-content default-tab" id="tab1" >
                <form onsubmit="return false;">
                    <fieldset>
                    <input type="hidden" id="channelId" value="0" />
                 
                     <div id="widgetBox" style="display:block;overflow: hidden;">
                  <label>选择挂件</label>
                  <?php if(!empty($widget)): 
                      $widgetArr = array();
                      foreach($widget as $key=>$value):
                          $widgetArr[$key] = array(
                              'widgetFile'=>$value['widgetFile'],
                              'widgetName'=>$value['widgetName']
                          );
                          ?>
              <div id="<?php echo $key; ?>" onclick="$.channeledit.changeWidget(this,'<?php echo $value->wid; ?>');" class="notification information png_bg" style="width:13%; height:100px; overflow: hidden; float: left; margin-left: 15px;">
      <div><h5><?php echo $value->widgetNameCn; ?></h5><br /><a href="#" title="<?php echo $value->description; ?>"><small class="small"><?php echo $value->description; ?></small></a></div>
    </div>
              <?php endforeach;endif; ?>
                  
              <input type="hidden" id="wid" value="<?php echo $data->wid; ?>" />
              </div>
                    
              <div class="clear"></div>
              <p>
                  <label>栏目排序：</label>
                  <input class="text-input small-input"  id="ordering" type="number" value="0" />
                  <span class="input-notification information png_bg">默认正序排列</span>
              </p>
                <p>
              <label>栏目标题</label>
              <input class="text-input small-input" type="text" id="typename" value="" name="small-input">
              </p>
              <p>
              <label>英文标题</label>
              <input class="text-input small-input" type="text" id="typenameEN" value="" name="small-input">
              </p>
              <p>
              <label>关键词</label>
              <input class="text-input small-input" type="text" id="keywords" value="" name="small-input">
              <span class="input-notification information png_bg">文件头部关键词</span>
              </p>
              <p>
              <label>描述</label>
              <input class="text-input small-input" type="text" id="description" value="" name="small-input">
              <span class="input-notification information png_bg">文件头部描述</span>
              </p>
              <p>
              开启移动端浏览：
              <input type="radio" name="ismobile" value ="1" checked="checked">
              是
              <input type="radio" name="ismobile" value ="0"  >
              否</p>
              <p>
                  栏目是否启用：
                  <input type="radio" name="display" value ="1" checked="checked">
              是
              <input type="radio" name="display" value ="0"  >
              否
              </p>
              <p>
                  是否支持会员信息发布：
                  <input type="radio" name="submission" value ="1" checked="checked" >
              是
              <input type="radio" name="submission" value ="-1" >
              否
              </p>
              <p>
                  上级栏目：<select id="topId">
                      <option value="0">顶级栏目</option>
                      <?php foreach($topData as $value):?>
                      <option value="<?php echo $value->id; ?>" <?php if($value->id == $pid)echo 'selected="selected"'; ?>><?php if($value->pid != 0):echo '&nbsp;&nbsp;&nbsp;';endif; echo '&nbsp;'.$value->typename; ?></option>
                      <?php endforeach; ?>
                  </select>
              </p>
              <p>
                  内页模板：<select id="commonTemp">
                     <?php if(!empty($commonArr)):
                         $i = 0;
                         foreach($commonArr as $key=>$value):
                         if($value != '.' && $value != '..'): ?>
                      <option value="<?php echo $i; ?>"><?php echo $value; ?></option>
                      <?php $i++;endif;endforeach;endif; ?>
                  </select>
              </p>
              <input class="text-input small-input"  id="tempName" type="hidden" value="" />
              <p>
                  <label>挂件首页：</label>
                  <input class="text-input small-input"  id="temp" type="text" value="" />
                  <span class="input-notification attention png_bg">挂件默认首页文件</span>
              </p>
              <p>
                  <label>挂件内页：</label>
                  <input class="text-input small-input"  id="comtemp" type="text" value="" />
                  <span class="input-notification attention png_bg">挂件默认内页文件</span>
              </p>
              <div class="content-box column-left">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">栏目封面</h3>
      </div>
      <div class="content-box-content">
        <div class="tab-content default-tab" style="display: block;">
          <h4>栏目封面</h4>
          <p>
                 
                  <input type="hidden" id="coverin" name="cover" value="" />
                  <img id="cover" class="coverImg" src="<?php echo ASSETS; ?>base/NOIMG.jpg" />
                  <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index','fname'=>'/upload/channel/'.$filetemp,'model'=>2)) ?>" ></iframe>
                  
              </p>
        </div>
      </div>
    </div>
              <div class="content-box column-right">
      <div class="content-box-header">
        <h3 style="cursor: s-resize;">栏目封面（移动版）</h3>
      </div>
      <div class="content-box-content">
        <div class="tab-content default-tab" style="display: block;">
          <h4>栏目封面（移动版）</h4>
          <p>
                  <input type="hidden" id="smallcoverin" name="smallcover" value="<?php echo $data->smallCover; ?>" />
                  <img id="smallcover" class="coverImg" src="<?php echo ASSETS; ?>base/NOIMG.jpg" />
                  <iframe border="no" width="100%" height="50px" src="<?php echo CHtml::normalizeUrl(array('upload/index','fname'=>'/upload/channel/'.$filetemp,'model'=>3)) ?>" ></iframe>
              </p>
        </div>
      </div>
    </div>
              <div class="clear"></div>
              
             <p>
                 <label>栏目介绍</label>
                 <div id="article_content"></div>
           <?php $this->widget('ext.KEditor.KEditor',array(
			'name'=>'content132', //设置name
                        'id'=>'article_content',
			'textareaOptions'=>array(
				'style'=>'width:98%;height:400px;',
			)
		)); ?>


             </p>
                    </fieldset>
                    </form>
        </div>
            
            
        </div>
        <p class="config">
              <input class="button" type="submit" onclick="$.channeledit.putDataDb();" value="保存">
            </p>
    </div>
    
   
    
</div>
<script>
    var widget = eval('('+'<?php echo json_encode($widgetArr); ?>'+')');
function changeCover(name,file){
    var old = $('#'+name+'in').val();
    
    var path = '/upload/channel/<?php echo $filetemp; ?>/';
    $.ajax({
        type:'post',
        data:'path='+path+old,
        url:'<?php echo CHtml::normalizeUrl(array('channel/deleteCover')); ?>'
    });
    path = path+file;
    $('#'+name).attr('src',path);
    $('#'+name+'in').val(file);
}

</script>