<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
<script type="text/javascript" src="<?php echo ASSETS;?>resources/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>swfupload/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>swfupload/js/swfupload.queue.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>swfupload/js/fileprogress.js"></script>
<script type="text/javascript" src="<?php echo ASSETS;?>swfupload/js/handlers.js"></script>
    </head>
    <body >
<style>
    .progressWrapper {
	width: 357px;
	overflow: hidden;
}

.progressContainer {
	margin: 5px;
	padding: 4px;
	border: solid 1px #E8E8E8;
	background-color: #F7F7F7;
	overflow: hidden;
}
/* Message */
.message {
	margin: 1em 0;
	padding: 10px 20px;
	border: solid 1px #FFDD99;
	background-color: #FFFFCC;
	overflow: hidden;
}
/* Error */
.red {
	border: solid 1px #B50000;
	background-color: #FFEBEB;
}

/* Current */
.green {
	border: solid 1px #DDF0DD;
	background-color: #EBFFEB;
}

/* Complete */
.blue {
	border: solid 1px #CEE2F2;
	background-color: #F0F5FF;
}

.progressName {
	font-size: 8pt;
	font-weight: 700;
	color: #555;
	width: 323px;
	height: 14px;
	text-align: left;
	white-space: nowrap;
	overflow: hidden;
}

.progressBarInProgress,
.progressBarComplete,
.progressBarError {
	font-size: 0;
	width: 0%;
	height: 2px;
	background-color: blue;
	margin-top: 2px;
}

.progressBarComplete {
	width: 100%;
	background-color: green;
	visibility: hidden;
}

.progressBarError {
	width: 100%;
	background-color: red;
	visibility: hidden;
}

.progressBarStatus {
    display: none;
	margin-top: 2px;
	width: 337px;
	font-size: 7pt;
	text-align: left;
	white-space: nowrap;
}


#divStatus {
    display: none;
}

/* -- SWFUpload Object Styles ------------------------------- */
.swfupload {
	vertical-align: top;
}

</style>

<script type="text/javascript">
		var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "<?php echo ASSETS; ?>swfupload/swfupload/swfupload.swf",
				upload_url: "<?php echo $upPath; ?>",	
				post_params: {
                                    "fileName":"<?php echo $fname; ?>"
                                },
				file_size_limit : "1 MB",
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "<?php echo ASSETS; ?>swfupload/images/TestImageNoText_65x29.png",	// Relative to the Flash file
				button_width: "65",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">上传</span>',
				button_text_style: ".theFont { font-size: 16; color:#ffffff;font-weight:bold; }",
				button_text_left_padding: 12,
				button_text_top_padding: 3,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
function uploadSuccess(file, serverData) {
    <?php
        switch($this->model){
            case 1:
                echo 'parent.'.Yii::app()->session['uploadModel'].';';break;
            case 2:
                echo 'parent.changeCover("cover",serverData);';break;
            case 3:
                echo 'parent.changeCover("smallcover",serverData);';break;
            case 4:
                echo 'parent.uploadBack("'.$this->domId.'",serverData);';break;
            case 5:
                echo 'parent.batchShow(serverData);';break;
            case 6:
                echo 'parent.voteLitpic("'.$this->domId.'",serverData);';break;
        }
    ?>
     return;

}

	</script>
<div id="content">
<div>
				<span id="spanButtonPlaceHolder"></span>
				<input id="btnCancel" type="button" value="" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
			</div>

		<div class="fieldset flash" id="fsUploadProgress" style="display:none;">
			<span class="legend"></span>
	  </div>
		<div id="divStatus" style="display:none;"></div>
			


</div>
        <div id="show_img"></div>