<script type="text/javascript" src="<?php echo ASSETS;?>js/listart.js"></script>
<div id="main-content">

    <div class="clear"></div>
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;"><a href="/">首页</a>&nbsp;>>&nbsp;<a href="<?php echo CHtml::normalizeUrl(array('channel/index','isSmallType'=>1)); ?>">栏目管理</a>&nbsp;>>&nbsp;会员发布内容管理</h3>
            <input class="button" style="float:right; margin-top: 7px; margin-right: 15px;" type="submit" onclick="$.listart.updateOrd();" value="更新排序">
        </div>
    
        <div class="content-box-content">
            <ul class="shortcut-buttons-set">
                <li><a class="shortcut-button" href="javascript:void(0);" onclick="$.listart.deleteAll();"><span> <img src="<?php echo ASSETS; ?>resources/images/icons/paper_content_pencil_48.png" alt="icon"><br>
                            批量删除内容</span></a></li>
                <li><a class="shortcut-button" href="javascript:void(0);" onclick="$.listart.checkAll();"><span> <img src="<?php echo ASSETS; ?>resources/images/icons/pencil_48.png" alt="icon"><br>
                            批量审核内容</span></a></li>
    </ul>
    <div class="clear"></div>
            <div class="tab-content default-tab" style="display: block;">
                <fieldset>
                    <p>选择栏目：<select id="seChannel" >
                            <option value="">全部</option>
                            <?php foreach($channel as $v): ?>
                            <option value="<?php echo $v['id']; ?>" <?php if($channelId == $v['id'])echo ' selected = "selected" '; ?> ><?php echo $v['typename']; ?></option>
                            <?php endforeach; ?>
                        </select>&nbsp;&nbsp;<select id="seArctype">
                            <option value="" >全部</option>
                            <option value="-1">未审</option>
                            <option value="1">已审</option>
                        </select> <input class="button"  type="button" onclick="$.listart.SearchInfo('<?php echo CHtml::normalizeUrl(array('listart/MemberSet','isSmallType'=>2)); ?>');" value="搜索"></p>
                </fieldset>
                <table>
                    <thead>
                        <tr>
                            <th><input class="check-all" type="checkbox"></th>
                            <th>序号</th>
                            <th>名称</th>
                            <th>状态</th>
                            <th>属性</th>
                            <th>作者</th>
                            <th>是否支持移动</th>
                            <th>提交时间</th>
                            <th>操作</th>
                            <th>排序</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="bulk-actions align-left">

                                    <div class="pagination">
                                        <?php if($this->totalPage > 1): ?><a href="<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType, 'page' => 1)); ?>" title="首页">« 首页</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page - 1))); ?>" title="上一页">« 上一页</a> 
                                        <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <a href="<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType, 'page' => $i)); ?>" class="number <?php if($i==$this->page)echo 'current'; ?>" title="第<?php echo $i; ?>页"><?php echo $i; ?></a> 
                                        <?php endfor; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType, 'page' => ($this->page + 1))); ?>" title="下一页">下一页 »</a>
                                        <a href="<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType, 'page' => $this->totalPage)); ?>" title="尾页">尾页 »</a> 
                                        跳转至 <select onchange="window.location.href='<?php echo CHtml::normalizeUrl(array('listart/index', 'isSmallType' => $this->isSmallType)); ?>&page='+this.value;">
                                            <?php for ($i = 1; $i <= $this->totalPage; $i++): ?>
                                            <option <?php if($this->page == $i) echo 'selected="selected"'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        共有 <?php echo $this->total; ?> 条记录
                                    <?php else: echo '共有 '.$this->total.' 条记录';endif; ?></div>
                                    <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody id="widgetContent">
                    <form  action="<?php echo CHtml::normalizeUrl(array('listart/orderingUp')); ?>" method="post"  id="orderingForm" target="hiddenFr"> 
                        <?php if (is_array($data) && !empty($data)):
                            foreach ($data as $key => $value):
                                ?>
                                <tr <?php if ($key % 2 == 0) echo 'class="alt-row"'; ?> id="<?php echo $value['id']; ?>">
                                   <td><input type="checkbox" value="<?php echo $value['id'].','.$value['wid']; ?>" name="checkAll"></td>
                                    <td><?php echo $value['id']; ?></td>
                                    <td><a href="<?php echo CHtml::normalizeUrl(array('listart/edit','pkid'=>$value['id'],'wid'=>$value['wid'],'channelId'=>$value['channelId'])); ?>"><?php echo adminSys::_cutStr($value['title'],6); ?></a></td>
                                    <td><?php echo adminSys::_arcrankCN($value['arcrank']); ?></td>
                                    <td><?php echo adminSys::_flagCN($value['flag']); ?></td>
                                    <td><?php echo $value['Awrite'];?></td>
                                    <td>
                                        <?php if ($value['enable']):echo '是';
                        else: echo '否';
                        endif; ?></td>
                                    <td><?php echo adminSys::_time($value['senddate'],1); ?>
                                    </td>
                                    <td>
                                        <?php if($value['arcrank'] == -1): ?>
                                        <a href="#" onclick="$.listart.checkOne('<?php echo $value['id']; ?>','<?php echo CHtml::normalizeUrl(array('listart/checkOne')); ?>');" title="审核"><img src="<?php echo ASSETS; ?>resources/images/icons/hammer_screwdriver.png" alt="审核"></a>       
                                        <?php endif; ?>
                                        <a href="<?php echo CHtml::normalizeUrl(array('listart/edit','pkid'=>$value['id'],'wid'=>$value['wid'],'channelId'=>$value['channelId'])); ?>" title="修改"><img src="<?php echo ASSETS; ?>resources/images/icons/pencil.png" alt="修改"></a> 
                                        <a href="#" onclick="$.listart.deleteOne('<?php echo $value['id']; ?>','<?php echo $value['wid']; ?>');" title="删除"><img src="<?php echo ASSETS; ?>resources/images/icons/cross.png" alt="删除"></a> 
                                        <a href="info<?php echo $value['id'].'.'.$value['channelId']; ?>.html" target="_BLANK"  title="预览PC"><img src="<?php echo ASSETS; ?>resources/images/icons/webview.png" alt="预览PC"></a> 
                                    </td>
                                    <td>
                                        <input type="number" value="<?php echo $value['ordering']; ?>" name="ordering[]" style="height:18px; margin: 0px; padding:0px; width:30px; text-align: center;" />
                                    </td>
                                </tr>
    <?php endforeach;
endif; ?>
                                <input type="hidden" id="strOrder" name="strOrder" />
                    </form>
                    </tbody>
                </table>  
                <iframe name="hiddenFr" style="display: none;" border="no" ></iframe>
            </div>

        </div>
    </div>
</div>
    <script type="text/javascript">
    function reback(msg){
        if(msg == 1){
            $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">2</i>秒后返回栏目管理</div></div>');
                    var i = 2;
                    window.setInterval(function(){
                        if(i > 1){
                            i--;
                            $('#scerdShow').html(i);
                            return;
                        }
                        window.location.reload();
                    },1000); 
        }
    }
    </script>