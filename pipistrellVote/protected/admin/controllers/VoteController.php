<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
class VoteController extends CController {
public $isType = 0;
   public $isSmallType = 0;
    public function filters() {
        return array(
            array(
                'application.admin.filters.MemcheckFilter'
            )
        );
    }

    public function actionShow() {
        $this->layout='application.admin.views.layouts.main';
        $this->isType = 1;
        $this->isSmallType = $_GET['isSmallType'];
        $a = Addvote::model()->findByPk($_GET['pkid']);
        $data = Addvote_infos::model()->findAll(' aid=:aid ',array(':aid'=>$_GET['pkid']));
        $this->render('showVote',array(
            'a'=>$a,
            'data'=>$data,
            'title'=>$_GET['title']
        ));
    }

}

?>