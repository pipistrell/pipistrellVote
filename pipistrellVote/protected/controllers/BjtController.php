<?php
/*
 * author:pipistrll
 * email:appyouxiang@163.com
 */
header("Content-Type: text/html; charset=UTF-8");

class BjtController extends Controller {

  public function actionIndex() {
       $installFile = dirname(Yii::app()->getBasePath()).'/install/lock.txt';
        if(!is_file($installFile) && Yii::app()->session['install'] != md5('installBjtCms001') && is_dir(dirname(Yii::app()->getBasePath()).'/install/')){
            Header("Location: /install/index.php");
            return;
        }else if(!is_file($installFile) && Yii::app()->session['install'] == md5('installBjtCms001') && is_dir(dirname(Yii::app()->getBasePath()).'/install/')){
            $sql = dirname(Yii::app()->getBasePath()).'/install/bjtcms.sql';
            $string = file_get_contents($sql);
            $bak = Yii::app()->db->createCommand($string)->execute();
            file_put_contents ($installFile, 'ok');
            unset(Yii::app()->session['install']);
        }
        
        $this->render('index');
    }
    public function actionError(){
        $msg = empty($_GET['msg'])?'您要的页面未找到！':$_GET['msg'];
        $this->renderPartial('error',array(
            'msg'=>$msg
        ));
    }

}