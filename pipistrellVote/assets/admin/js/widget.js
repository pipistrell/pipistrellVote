$.extend({
    widget:{
        deleteSome:function(id,modelId){
            if(!id)return;
            $.confirm({
                'title'		: '信息提醒',
                'message'	: '您确定要删除此条记录吗？',
                'buttons'	: {
                    'Yes'	: {
                        'class'	: 'blue',
                        'action': function(){
                            $.ajax({
                                type:'post',
                                data:'id='+id+'&modelId='+modelId,
                                url:WEB_IN+'?r=request/deleteOne',
                                success:function(data){
                                    if(data == 1){
                                        $.showMsg('<div class="notification success png_bg"><div>操作成功！系统将在<i id="scerdShow">2</i>秒后返回挂件管理</div></div>');
                                        var i = 2;
                                        window.setInterval(function(){
                                            if(i > 1){
                                                i--;
                                                $('#scerdShow').html(i);
                                                return;
                                            }
                                            window.location.reload();
                                        },1000);                           
                                    }
                                }
                            });
                        }
                    },
                    'No'	: {
                        'class'	: 'gray',
                        'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
        },
        widgetEdit:function(u,wid){
            var widgetNameCn = $('#widgetNameCn').val();
            var widgetFile = $('#widgetFile').val();
            var ismobile = $(":radio[name='ismobile']:checked").val();
            var enable = $(":radio[name='enable']:checked").val();
            var description = $('#description').val();
            if(ismobile == '' || ismobile == null)ismobile = 1;
            if(enable == '' || enable == null)enable = 1;
            $.ajax({
                type:'post',
                data:'widgetNameCn='+widgetNameCn+'&widgetFile='+widgetFile+'&ismobile='+ismobile+'&enable='+enable+'&description='+description+'&wid='+wid,
                url:u,
                success:function(data){

                    if(data != ''){
                        $.showMsg('<div class="notification success png_bg"><div>修改成功</div></div>');
                    }
                }
            });
        },
        add:function(u){
        
        }
    }//widget对象结束
});