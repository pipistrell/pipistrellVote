<script type="text/javascript" src="<?php echo ASSETS;?>js/widget_add.js"></script>
<?php $this->beginContent('//layouts/widget_step',array('step'=>$step));
            echo $content;
            $this->endContent(); ?>

<div id="main-content">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">新增挂件 → 上传控制器文件</h3>
        </div>
        
        <div class="content-box-content">
            <div class="notification information png_bg">
      <div>注意上传文件格式必须为*.php</div>
    </div>
            <form id="step_1_form" onsubmit="return false;">
                 <input type="hidden" id="isUpload" value="0" />
                 <p>
                     <iframe border="no" width="100%" height="100%" src="<?php echo CHtml::normalizeUrl(array('upload/index','fname'=>'widget','model'=>1)); ?>" ></iframe>
                 </p>
                <p>
                    <input class="button" type="submit"  onclick="$('#step_1_form')[0].reset();" value="重置">&nbsp;&nbsp;<input class="button" type="submit"  onclick="$.widgetAdd.nextStep();" value="下一步">
                </p>
            </form>
        </div>
    </div>
</div>
