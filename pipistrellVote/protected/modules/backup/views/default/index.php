<div id="main-content">

    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;"><a href="/">首页</a>&nbsp;>>&nbsp;数据库备份</h3>
        </div>
    
        <div class="content-box-content">
           
    <div class="clear"></div>
            <div class="tab-content default-tab" style="display: block;">
                <?php
$this->breadcrumbs=array(
	'Manage'=>array('index'),
);?>
<h1></h1>

<?php $this->renderPartial('_list', array(
		'dataProvider'=>$dataProvider,
));
?>
            </div>
        </div>
    </div>
</div>
<script >
$(function(){
    var msg = '<?php echo $msg; ?>';
    if(msg != ''){
        $.showMsg(msg);
    }
});
</script>
